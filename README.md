[about-diy-homeautomation]: ./Doc/About-diy-homeautomation.adoc
[getting started]: ./Doc/getting-Started.adoc
[building a sensor node]: ./Doc/building-a-sensor-node.adoc
[building the gateway]: ./Doc/building-the-gateway.adoc
[**contributing**]: ./Doc/CONTRIBUTING.adoc

# Einführung

Um einen **Überblick** über das Projekt zu bekommen sollte zunächst [About-diy-homeautomation] durchgelesen werden. Dort wird die **Architektur** dieser Homeautomationlösung erklärt.

In [Building a sensor node] wird erklärt, wie man die **Sensorknoten**
aufbaut, die zur Datenaufnahme benötigt werden.

Der Aufbau des **Gateways**, an das die Sensorknoten die Daten schicken wird in [building the gateway] ausführlich beschrieben.

Anschließend kann in [Getting Started] nachgelesen werden, wie
man mit der zuvor aufgebauten Hardware seine eigene Homeautomation aufbauen kann.

Wer zu diesem Projekt beitragen möchte kann in [**Contributing**] nachlesen, welche
Schritte dazu erforderlich sind.

## Viel Erfolg!
