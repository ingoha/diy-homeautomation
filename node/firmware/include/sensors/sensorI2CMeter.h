#pragma once

#include "device.h"
#include "config_board.h"
#include "Wire.h"

class SensorI2CMeter : public Device {
 public:
    explicit SensorI2CMeter(int aDeviceID);
    ~SensorI2CMeter();
    void handle(Payload *message = NULL);
};
