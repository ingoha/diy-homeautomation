#pragma once

#include <HCSR04.h>
#include "device.h"

class SensorDistance : public Device {
 private:
    HCSR04* sensor;

 public:
    explicit SensorDistance(int aDeviceID);
    ~SensorDistance();
    void handle(Payload *message = NULL);
};
