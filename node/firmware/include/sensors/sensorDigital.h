#pragma once

#include "device.h"
#include "radio.h"

class SensorDigital : public Device {
 public:
    explicit SensorDigital(int aDeviceID);
    ~SensorDigital();
    static void handleInterrupt();
    void handle(Payload *message = NULL);
};
