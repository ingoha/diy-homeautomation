#pragma once

#include "device.h"
#include "config_board.h"
#include "DHT.h"

class SensorTempHum : public Device {
 private:
    DHT* dht = new DHT(DHTPIN, nodeConfig->dhtType);
 public:
    explicit SensorTempHum(int aDeviceID);
    ~SensorTempHum();
    void handle(Payload *message = NULL);
};
