#include "config.h"
#pragma once

#include "radio.h"
#include "device.h"

class SensorMBus : public Device {
 private:
    void readSerial(byte message[]);
    bool waitACKSlave();
    void extractZaehlerDaten(byte message[], RFMRadio &radio);  // We accept pass by reference instead of a pointer here
    void initialCommunication(byte address);
    void sendRequest(byte address);
    void wakeUp(int num, unsigned int delayMs);  // not used in current version

 public:
    explicit SensorMBus(int aDeviceID);
    ~SensorMBus();
    void handle(Payload *message = NULL);
};
