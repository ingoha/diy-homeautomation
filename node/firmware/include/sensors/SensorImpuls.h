#pragma once

#include "radio.h"
#include "device.h"

class SensorImpuls : public Device {
 private:
    static volatile uint64_t last_interrupt_time;
    static volatile uint64_t interrupt_time;
    byte serial_data;


 public:
    explicit SensorImpuls(int aDeviceID);
    ~SensorImpuls();
    static void impulsCounterInterrupt();
    void handle(Payload *message = NULL);
    static float impulsMeterReading;           // variable for the last meter reading
    static float impulsesPerValue;             // variable for rotations per kWh/ m^3 o.ae.
    static volatile float impulses;                     // number of impulses since last sending of meter reading
    static volatile unsigned int debounceTime;          // current set debounce time of interrupt
};
