#pragma once

#include "radio.h"
#include "device.h"


class SensorD0 : public Device {
 private:
    byte  serial_data;
    String inString;
    float   D0_meter_Reading;
    float   D0_current_Power;
    uint64_t time_1;
    uint64_t time_2;

 public:
    explicit SensorD0(int aDeviceID);
    ~SensorD0();
    void handle(Payload *message = NULL);
};
