#pragma once

// This file contains all the enums for sensors, actors, interfaces, RFM-module etc.

const bool debug = true;
const int debugBaudRate = 9600;
const int setupDeviceId = 0;

enum others{
  eNetworkID = 22,
  eGatewayID = 1,
  eMBusAdress = 5,
  eSleeptime8s = 8,
  eStartAdressEEPROM = 0
};

enum operationModes{
  eSetupMode = 0,
  eIntervallMode = 1,
  eSlaveMode = 2
};

enum sensorIDs{
  eSensorDigitalID = 4,
  eSensorDistanceID = 5,
  eSensorTempHumID = 6,
  eSensorHumidityID = 7,
  eSensorImpulsID = 8,
  eSensorImpulsNumImpulsesID = 81,
  eSensorNotUsed = 0
};

enum actorIDs{
  eActorDekoID = 30,
  eActorNotUsed = 0
};

enum interfaceIDs{
  eInterfaceD0Smartmeter = 9,
  eInterfaceI2C = 20,
  eInterfaceMBusID = 20,
  eInterfaceMBusEnergyID = 21,
  eInterfaceMBusVolumeID = 22,
  eInterfaceMBusFlowID = 23,
  eInterfaceMBusPowerID = 24,
  eInterfaceNotUsed = 0
};
