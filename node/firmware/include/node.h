#pragma once

#include "radio.h"
#include "nodeConfigStruct.h"


#define DEVICE_ID_NODEMANAGMENT     ((uint8_t) 0)
#define SENSOR_VAL_RESET_NODE       ((float) 1.0)
#define WAIT_TIME_BEFORE_RESET      ((int) 5000)
#define TIMEOUT_RESET_REQUEST       ((int) 500)
#define REQ_ID_INTERVALL            ((int) 100)
#define LED_MSG_SIZE                ((int) 4)

class Node {
 private:
    NodeConfiguration nodeConfig;
    RFMRadio radio;
    byte rfmMessageBuffer[RF69_MAX_DATA_LEN] = {0};   // define from RFM lib, created as attribute to only declare once / safe runtime
    bool skip = true;
    int analogTimeElapsed = 0;
    bool getConfig();
    bool setConfig();
    void requestNodeId();
    void waitForEncryptKey();
    void waitForConfig();
    void setupSensors();
    void checkBackToSetupMode();
    void resetNode();
    void printConfig();

 public:
    Node();
    ~Node();
    byte mode();
    void handleUnknownMode();
    bool setup();
    void masterSlaveMode();
    void intervallMode();
    void setupMode();
};
