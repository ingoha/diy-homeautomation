#pragma once

#include <RFM69.h>
#include <config.h>
#include <DHT.h>

typedef struct {
    bool    eepromNotInitialised   = true;                        // Flag shows if standardconfig was already written to EEPROM
    uint8_t operationMode          = eSetupMode;                  // Mode of the node
    uint8_t sensorDigital          = eSensorNotUsed;              // Device ID of the digital sensor, by default not built in
    uint8_t sensorDistance         = eSensorNotUsed;              // Device ID of the distance sensor, by default not built in
    uint8_t sensorTempHum          = eSensorTempHumID;            // Device ID of the temperature sensor (DHT11), by default built in
    uint8_t sensorHumidity         = eSensorHumidityID;           // Device ID of the humidity sensor (DHT11), by default built in
    uint8_t sensorImpuls           = eSensorNotUsed;              // Device ID of the impuls sensor, by default not built in
    uint8_t sensorImpulsNumPulses  = eSensorImpulsNumImpulsesID;  // Device ID of sum of impulses the impuls sensor counted, gets configured with sensorImpuls
    float   startMeterReading      = 0;                           // Startvalue of the impuls sensor
    float   impulsesPerValue       = 0;                           // Impulses necesarry for one unit to be counted fully (i.e. m^3, kWh)
    int     debounceTime           = 0;                           // Debounce time so impulses are only counted once
    uint8_t d0Smartmeter           = eInterfaceNotUsed;           // Device ID of the D0 smartmeter, by default not connected
    uint8_t mBus                   = eInterfaceNotUsed;           // Device ID of the MBus, by default not connected
    uint8_t mBusEnergy             = eInterfaceMBusEnergyID;      // Device ID of the energycounter (MBus), gets configured with MBus
    uint8_t mBusVolume             = eInterfaceMBusVolumeID;      // Device ID of the volumecounter (MBus), gets configured with MBus
    uint8_t mBusFlow               = eInterfaceMBusFlowID;        // Device ID of the flowcounter (MBus), gets configured with MBus
    uint8_t mBusPower              = eInterfaceMBusPowerID;       // Device ID of the powercounter (MBus), gets configured with MBus
    uint8_t mBusAdress             = eMBusAdress;                 // Adress of the node within the MBus
    uint8_t dekoElement            = eActorNotUsed;               // Device ID of the LED-Element, by default not built in
    uint8_t i2cMeter               = eInterfaceNotUsed;           // Device ID of the I2C interface, by default not connected
    uint8_t i2cMeterDevices        = 0;                           // Number of devices on the I2C bus
    int     analogDelaySeconds     = 8;                           // Number of seconds between sending of sensordata
    int     nodeID                 = 2;                           // ID 2 is reserved for new nodes
    uint8_t gatewayID              = eGatewayID;                  // ID of the gateway in the RFM
    byte    frequency              = RF69_868MHZ;                 // Frequency at which the RFM module is working
    char    encryptKey[16]         = "";                          // Encryption-Key for RFM DO NOT MAKE A STRING unless you want major bugs with the eeprom
    uint8_t dhtType                = DHT11;                       // Type of DHT-Sensor which ist used
} NodeConfiguration;
