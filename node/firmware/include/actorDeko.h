#pragma once

#include "device.h"
#include "radio.h"

class ActorDeko : public Device {
 public:
    // Explicit keyword to avoid unexpected behavior, see: https://stackoverflow.com/questions/121162/what-does-the-explicit-keyword-mean
    explicit ActorDeko(int aDeviceID);
    ~ActorDeko();
    void handle(Payload *message = NULL);
};
