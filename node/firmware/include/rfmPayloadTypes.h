#pragma once

#include <Arduino.h>
#include "nodeConfigStruct.h"

// struct for wireless data transmission
// nodeID and deviceID could be reduced to 16 and 8 bit. Problem is that memory in esp32 (receiver) ist 32 byte aligned.
// To avoid bit shifting we accept to transmit more bytes than necessary
typedef struct {
    int32_t   nodeID;           // node ID (1xx, 2xx, 3xx);  1xx = basement, 2xx = main floor, 3xx = outside
    uint32_t  deviceID;         // sensor ID (2, 3, 4, 5)
    uint64_t  uptime_ms;        // uptime in ms
    float     sensordata;       // sensor data?
    float     battery_volts;    // battery condition?
} Payload;


// Struct for Requesting Node Id and EncryptKey from Gateway
typedef struct {
    int32_t nodeID;
    uint32_t deviceID;
    uint32_t randomNumber;
} RequestIdPayload;

// Struct which is sent by the gateway, containing all necessary members
// note: There are 3 buffers used, to deal with alignment and size issues. Do not change their order / seize without testing afterwards
typedef struct {
    uint8_t operationMode          = eSetupMode;                // Mode of the node
    uint8_t sensorDigital          = eSensorNotUsed;            // Device ID of the digital sensor, by default not built in
    uint8_t sensorDistance         = eSensorNotUsed;            // Device ID of the distance sensor, by default not built in
    uint8_t sensorImpuls           = eSensorNotUsed;            // Device ID of the impuls sensor, by default not built in
    uint8_t d0Smartmeter           = eInterfaceNotUsed;         // Device ID of the D0 smartmeter, by default not connected
    uint8_t mBus                   = eInterfaceNotUsed;         // Device ID of the MBus, by default not connected
    uint8_t i2cMeter               = eInterfaceNotUsed;         // Device ID of the I2C interface, by default not connected
    uint8_t i2cMeterDevices        = 0;                         // Number of devices on the I2C bus
    uint8_t dekoElement            = eActorNotUsed;             // Device ID of the LED-Element, by default not built in
    uint8_t dhtType                = DHT11;                     // Type of DHT-Sensor which ist used
    int     buffer                 = 0;                         // Buffer is needed due to alignment issues
    int     analogDelaySeconds     = 600;                       // Number of seconds between sending of sensordata
    int     buffer2                = 0;                         // Buffer is needed due to alignment issues
    int     debounceTime           = 0;                         // Debounce time so impulses are only counted once
    int     buffer3                = 0;                         // Buffer is needed due to alignment issues
    float   startMeterReading      = 0;                         // Startvalue of the impuls sensor
    float   impulsesPerValue       = 0;                         // Impulses necesarry for one unit to be counted fully (i.e. m^3, kWh)
} DetailedNodeConfig;
