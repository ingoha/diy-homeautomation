#pragma once
#include <RFM69.h>
#include "nodeConfigStruct.h"
#include "rfmPayloadTypes.h"


class RFMRadio {
 private:
    RFM69* radio;
    float batteryVoltage();
    const NodeConfiguration* nodeConfig;


 public:
    RFMRadio();
    ~RFMRadio();
    void init();
    void send(int aDeviceID, float aValue);
    void requestIdAndEncryptKey(uint32_t randomNumber);
    void sleep();
    void encrypt();
    bool reinitialize();

    bool receiveDone(byte* msgBuffer, int* payloadSize = nullptr);
    void listenModeStart();
    void listenModeEnd();
    void passNodeConfig(const NodeConfiguration* aNodeConfig);
};


