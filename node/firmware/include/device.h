#pragma once

#include "utils.h"
#include "radio.h"
#include "nodeConfigStruct.h"
#include "Array.h"


const int MAX_NUMBER_OF_DEVICES = 100;

class Device {
 public:
    explicit Device(int aDeviceID);
    ~Device();
    static void handleListOfDevices(Payload *message = NULL);
    static Array <Device*, MAX_NUMBER_OF_DEVICES> listOfDevices;
    static void setRadio(RFMRadio *aRadio);
    static void setNodeConfig(const NodeConfiguration *aNodeConfig);
    virtual void handle(Payload *message = NULL);
 protected:
    int deviceID;
    static void addDevice(Device *newDevice);
    // pointer to the radio class
    static RFMRadio* radio;
    // pointer to the node configuration struct.
    // Making it const means the values of the config can not modified through this pointer
    static const NodeConfiguration* nodeConfig;
};
