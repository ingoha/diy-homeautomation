#include "node.h"
#include "config.h"
#include "config_board.h"
#include "device.h"
#include <EEPROM.h>
#include <LowPower.h>
#include "radio.h"
#include "utils.h"
#include <SPI.h>
#include "sensors/sensorTempHum.h"
#include "sensors/sensorDistance.h"
#include "sensors/sensorDigital.h"
#include "actorDeko.h"
#include "sensors/SensorImpuls.h"
#include "sensors/sensorD0.h"
#include "sensors/sensorMBus.h"
#include "sensors/sensorI2CMeter.h"


// Constructor for the node class
// return:      void - This function does not return any value.
Node::Node() {
    EEPROM.get(eStartAdressEEPROM, nodeConfig.eepromNotInitialised);
    if (nodeConfig.eepromNotInitialised) {
        nodeConfig.eepromNotInitialised = false;
        this->setConfig();
    }
    this->getConfig();
}

Node::~Node() {
    // Empty destructor
}


// Access the mode in which the node is configured
// return:      byte - mode of the node
byte Node::mode() {
    return nodeConfig.operationMode;
}




// Setup of the node (initialize RFM, get configuration, init all used sensors)
// return:      bool - Always returns true
bool Node::setup() {
    if (debug) {
        Serial.begin(debugBaudRate);
        Serial.println("\n--------------------\n");
    }
    pinMode(LED, OUTPUT);
    pinMode(BATTERY_PIN, INPUT);
    this->printConfig();
    blinkNTimes(1);
    radio.passNodeConfig(&nodeConfig);
    radio.init();
    radio.sleep();
    Device::setRadio(&radio);
    Device::setNodeConfig(&nodeConfig);
    if (nodeConfig.operationMode != eSetupMode) {
        this->setupSensors();
    }
    return true;
}

// Function to be executed when node is in master-slave mode
// return:      void - This function does not return any value.
void Node::masterSlaveMode() {
    if (radio.receiveDone(rfmMessageBuffer)) {
        Payload incomingMsg = *(Payload*) rfmMessageBuffer;
        blinkNTimes(1);
        debugPrinti("deviceID = ", incomingMsg.deviceID);
        debugPrintf("data = ", incomingMsg.sensordata);
        Device::handleListOfDevices(&incomingMsg);
        skip = true;
    }

    if (skip) {
        this->checkBackToSetupMode();
        radio.listenModeStart();
        debugPrintln("Listen Mode Start");
        // go into sleep mode
        LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
        blinkNTimes(1);
        radio.listenModeEnd();
        skip = false;
    }
}

// Function to be executed when node is operated in intervall mode
// return:      void - This function does not return any value.
void Node::intervallMode() {
    if (analogTimeElapsed > nodeConfig.analogDelaySeconds) {
        analogTimeElapsed = 0;
        Device::handleListOfDevices();
        this->checkBackToSetupMode();
    }
    else {
        debugPrinti("Elapsed: ", analogTimeElapsed);
    }
    // Enter power down state for 8 s with ADC and BOD module disabled
    delay(200);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    delay(100);
    // Add the 8 seconds we slept above
    analogTimeElapsed += eSleeptime8s;
}

// Function to be executed when node is operated in setup mode
// return:      void - This function does not return any value.
void Node::setupMode() {
    if (nodeConfig.nodeID == 2) {
        this->requestNodeId();
    }
    int payloadSize = 0;
    debugPrintln("Wait for msg");
    while (!radio.receiveDone(this->rfmMessageBuffer, &payloadSize)) {
        // wait
    }
    if (payloadSize != LED_MSG_SIZE) {
        if ((nodeConfig.nodeID != 2) && (String(nodeConfig.encryptKey) == "")) {
            this->waitForEncryptKey();
            return;
        }
        if ((nodeConfig.nodeID != 2) && (String(nodeConfig.encryptKey) != "")) {
            this->waitForConfig();
            return;
        }
    }
    digitalWrite(LED, *(uint8_t*)rfmMessageBuffer);     // Sets LED to state if msg is only 4 bytes long
}

// Reads the config of the node stored in the EEPROM
// return:      bool - always returns true
bool Node::getConfig() {
    EEPROM.get(eStartAdressEEPROM, nodeConfig);
    return true;
}
// Writes the config of a node in the EEPROM
// return:      bool -always returns true
bool Node::setConfig() {
    EEPROM.put(eStartAdressEEPROM, nodeConfig);
    return true;
}

// Function requests a NodeId from the gateway. If one is returned the internal id attribute is overwritten with the new one
// return:      void - This function does not return any value
void Node::requestNodeId() {
    uint64_t lastSent = 0;
    randomSeed(analogRead(DHTPIN));
    uint32_t randomNumber = random(__LONG_MAX__);
    debugPrintln("Req Id");
    while (!radio.receiveDone(this->rfmMessageBuffer)) {
        if ((int)(millis() - lastSent) > REQ_ID_INTERVALL) {
            radio.requestIdAndEncryptKey(randomNumber);
            lastSent = millis();
        }
    }
    RequestIdPayload answer = *(RequestIdPayload *)this->rfmMessageBuffer;
    if (answer.randomNumber == randomNumber) {
        nodeConfig.nodeID = answer.nodeID;
        this->setConfig();
        while (!radio.reinitialize()) {
            // Wait for radio init with new node id
        }
        debugPrinti("ID: ", nodeConfig.nodeID);
    }
}

// Function waits for the gateway to send the encrypt-key for the network
// return:      void - This function does not return any value
void Node::waitForEncryptKey() {
    for (int i = 0; i < 16; i++) {
        nodeConfig.encryptKey[i] = ((char)rfmMessageBuffer[i]);
    }
    radio.encrypt();
    this->setConfig();
    blinkNTimes(3);
    debugPrintln("Received key");
}

// Function waits for a config from the gateway. Saves it into EEPROM when it arrives
// return:      void - This function does not return any value.
void Node::waitForConfig() {
    DetailedNodeConfig config = *(DetailedNodeConfig *)this->rfmMessageBuffer;
    nodeConfig.operationMode = config.operationMode;
    nodeConfig.sensorDigital = config.sensorDigital;
    nodeConfig.sensorDistance = config.sensorDistance;
    nodeConfig.sensorImpuls = config.sensorImpuls;
    nodeConfig.d0Smartmeter = config.d0Smartmeter;
    nodeConfig.mBus = config.mBus;
    nodeConfig.i2cMeter = config.i2cMeter;
    nodeConfig.i2cMeterDevices = config.i2cMeterDevices;
    nodeConfig.dekoElement = config.dekoElement;
    nodeConfig.dhtType = config.dhtType;
    nodeConfig.analogDelaySeconds = config.analogDelaySeconds;
    nodeConfig.debounceTime = config.debounceTime;
    nodeConfig.startMeterReading = config.startMeterReading;
    nodeConfig.impulsesPerValue = config.impulsesPerValue;
    debugPrintln("Received config");
    this->setConfig();
    resetNode();
}


// Create interface / sensor / actor objects according to the configuration
// return:      void - This function does not return any value.
void Node::setupSensors() {
    // mBus and D0 are exclusive to each other. Should never both be activated if gateway has no bugs
    if (nodeConfig.mBus) {
        new SensorMBus(nodeConfig.mBus);
    }
    else if (nodeConfig.d0Smartmeter) {
        new SensorD0(nodeConfig.d0Smartmeter);
    }
    else {
        // Do nothing
    }

    // Impuls and Distance-Sensor use same interrupt pin therefore they are exclusive. Should never both be activated if gateway has no bugs
    if (nodeConfig.sensorImpuls) {
        new SensorImpuls(nodeConfig.sensorImpuls);
    }
    else if (nodeConfig.sensorDigital) {
        new SensorDigital(nodeConfig.sensorDigital);
    }
    else {
        // Do nothing
    }

    if (nodeConfig.i2cMeter) {
        new SensorI2CMeter(nodeConfig.i2cMeter);
    }
    if (nodeConfig.sensorTempHum) {
        new SensorTempHum(nodeConfig.sensorTempHum);
    }
    if (nodeConfig.sensorDistance) {
        new SensorDistance(nodeConfig.sensorDistance);
    }
    if (nodeConfig.dekoElement) {
        new ActorDeko(nodeConfig.dekoElement);
    }
}

void Node::checkBackToSetupMode() {
    uint64_t timeStartWaitingForAnswer = millis();
    radio.send(DEVICE_ID_NODEMANAGMENT, SENSOR_VAL_RESET_NODE);
    Serial.println("Start Reset Req");
    while ((int)(millis() - timeStartWaitingForAnswer) < TIMEOUT_RESET_REQUEST) {
        if (radio.receiveDone(this->rfmMessageBuffer)) {
            Payload msg = *(Payload *)this->rfmMessageBuffer;
            if ((msg.deviceID == DEVICE_ID_NODEMANAGMENT) && (msg.sensordata == SENSOR_VAL_RESET_NODE)) {
                nodeConfig.operationMode = eSetupMode;
                this->setConfig();
                this->resetNode();
            }
            return;
        }
    }
}

void Node::handleUnknownMode() {
    nodeConfig.operationMode = eSetupMode;
    this->setConfig();
    this->resetNode();
}

// Function waits for 1 s and then performs a reset on the whole chip
// return:      void - This function does not return any value.
void Node::resetNode() {
    int countdownStart = millis();
    while (millis() - countdownStart < WAIT_TIME_BEFORE_RESET) {
        // wait for 3 second for safe reset
    }
    void(* resetFunc) (void) = 0;
    resetFunc();
}

// Prints the config of the node to the serial Interface
// return:      void - This function does not return any value.
void Node::printConfig() {
    debugPrinti("EEPROM:\t", nodeConfig.eepromNotInitialised);
    debugPrinti("Mode:\t", nodeConfig.operationMode);
    debugPrinti("dig:\t", nodeConfig.sensorDigital);
    debugPrinti("dist:\t", nodeConfig.sensorDistance);
    debugPrinti("imp:\t", nodeConfig.sensorImpuls);
    debugPrintf("ImpBeg:\t", nodeConfig.startMeterReading);
    debugPrintf("Pls/Vl:\t", nodeConfig.impulsesPerValue);
    debugPrinti("Bounce:\t", nodeConfig.debounceTime);
    debugPrinti("D0:\t", nodeConfig.d0Smartmeter);
    debugPrinti("MBus:\t", nodeConfig.mBus);
    debugPrinti("LED:\t", nodeConfig.dekoElement);
    debugPrinti("I2C:\t", nodeConfig.i2cMeter);
    debugPrinti("numDev:\t", nodeConfig.i2cMeterDevices);
    debugPrinti("Period:\t", nodeConfig.analogDelaySeconds);
    debugPrinti("ID:\t", nodeConfig.nodeID);
    debugPrinti("DHT:\t", nodeConfig.dhtType);
    debugPrintln("");
}
