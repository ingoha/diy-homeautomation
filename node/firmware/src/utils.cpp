// Arduino universalNode firmware
// This file contains some helper functions

#include "utils.h"
#include "config.h"
#include "config_board.h"
#include "Arduino.h"

// Blinks the LED for a specified number of times.
// n:       An integer specifying the number of times the LED should blink.
// return:  void - This function does not return any value.
void blinkNTimes(int n) {
    for (int i = 0; i < n; i += 1) {
        digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay(500);              // wait for 1/10 second
        digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
        delay(500);             // wait for 1/10 second
    }
}

//
// Print to serial in debug mode only
//

// Prints a string to the Serial output if debug mode is enabled.
// s:       A pointer to a null-terminated char array representing the string to be printed.
// return:  void - This function does not return any value.
void debugPrintln(const char* s) {
    if (debug)
        Serial.println(s);
}

// Prints a formatted string followed by a double value to the Serial output if debug mode is enabled.
// s:       A pointer to a null-terminated char array representing the formatted string to be printed.
// x:       A double value to be printed after the formatted string.
// return:  void - This function does not return any value.
void debugPrintf(const char* s, double x) {
    if (debug) {
        Serial.print(s);
        Serial.println(x);
    }
}

// Prints a formatted string followed by an integer value to the Serial output if debug mode is enabled.
// s:       A pointer to a null-terminated char array representing the formatted string to be printed.
// x:       An integer value to be printed after the formatted string.
// return:  void - This function does not return any value.
void debugPrinti(const char* s, int x) {
    if (debug) {
        Serial.print(s);
        Serial.println(x);
    }
}
