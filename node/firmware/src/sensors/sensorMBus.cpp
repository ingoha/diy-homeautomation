#include "config.h"

#include "sensors/sensorMBus.h"
#include "config_board.h"
#include "Arduino.h"
#include "utils.h"
#include "radio.h"
#include <SPI.h>

#ifndef MBus
#define MBus 0
#define MBUS_ENERGY 0
#define MBUS_VOLUME 0
#define MBUS_FLOW 0
#define MBUS_POWER 0
#endif

// Constructor for the SensorMBus class.
// aDeviceID:   An integer representing the device ID for the SensorMBus object.
// return:      N/A - This is a constructor and does not return any value.
SensorMBus::SensorMBus(int aDeviceID) : Device(aDeviceID) {
    Serial.begin(2400, SERIAL_8E1);
}

SensorMBus::~SensorMBus() {}

// read meter readings over M-Bus protocoll
// radio:   Radio Object for sending messages
// address: address of meter
// return:  void - This function does not return any value.
void SensorMBus::handle(Payload *message) {
    byte serialData[72];                                // array for storeing serial data (length 72 is meter specific (PolluCom E))
    for (int i = 0; i < 3; i++) {                       // try three times to read
        // wakeUp(132, int 100);                        // wack up method for newer versions of PolluCom E
        initialCommunication(nodeConfig->mBusAdress);   // init communication
        if (waitACKSlave()) {                           // check for acknowledgement
            sendRequest(nodeConfig->mBusAdress);        // send request for meter data
            readSerial(serialData);                     // read data from meter (slave)
            extractZaehlerDaten(serialData, *radio);    // extract meter readings from recieved messages and send them via radio
            return;
        }
    }
}

// Function for initializing communication with a SensorMBus device.
// address: A byte representing the slave address of the SensorMBus device.
// return:  void - This function does not return any value.
void SensorMBus::initialCommunication(byte address) {
    Serial.write(0x10);    // short frame
    Serial.write(0x40);    // SND_NKE
    Serial.write(address);  // slave address
    byte checksum = 0x40 + address;
    Serial.write(checksum);  // check sum
    Serial.write(0x16);     // stop
}

// Function for sending a request to a SensorMBus device.
// address:   A byte representing the address of the SensorMBus device.
// return:    void - This function does not return any value.
void SensorMBus::sendRequest(byte address) {
    Serial.write(0x10);    // short fram
    Serial.write(0x5B);    // REQ_NKE
    Serial.write(address);  // address
    byte checksum = 0x5B + address;
    Serial.write(checksum);  // check sum
    Serial.write(0x16);     // stop
}

// Function for extracting data from a MBus message and sending it via RFM radio.
// message:     An array of bytes representing the MBus message.
// radio:       A reference to an RFMRadio object for sending data via radio.
// return:      void - This function does not return any value.
// annotation:  only for Meter type PolluCom E
void SensorMBus::extractZaehlerDaten(byte message[], RFMRadio &radio) {
    // Energy: Bytes 21 - 24
    String energy = String(int(message[24]), HEX) + String(int(message[23]), HEX) + String(int(message[22]), HEX) + String(int(message[21]), HEX);
    float fEnergy = (float)energy.toInt() / 1000;  // MWh
    radio.send(nodeConfig->mBusEnergy, fEnergy);             // send via radio
    delay(200);

    // volume: Bytes 27 - 30
    String volume = String(int(message[30]), HEX) + String(int(message[29]), HEX) + String(int(message[28]), HEX) + String(int(message[27]), HEX);
    float fVolume = (float)volume.toInt() / 1000;  // m^3
    radio.send(nodeConfig->mBusVolume, fVolume);
    delay(200);

    // current Flow: Bytes 33 - 36
    String flow = String(int(message[36]), HEX) + String(int(message[35]), HEX) + String(int(message[34]), HEX) + String(int(message[33]), HEX);
    float fFlow = (float)flow.toInt() / 1000;  //  m^3/h
    radio.send(nodeConfig->mBusFlow, fFlow);
    delay(200);

    // current Power: Bytes 39 - 42
    String power = String(int(message[42]), HEX) + String(int(message[41]), HEX) + String(int(message[40]), HEX) + String(int(message[39]), HEX);
    float fPower = (float)power.toInt() / 1000;  // kW
    radio.send(nodeConfig->mBusPower, fPower);
    delay(200);

    radio.sleep();

    // reset message
    for (int i = 0; i < 72; i++) {   // reset message array
        message[i] = 0x00;
    }
}

// Function for waiting for an acknowledgement (ACK) from the slave device.
// return: bool - Returns true if an ACK is received within 5 seconds, otherwise false.
bool SensorMBus::waitACKSlave() {   // Wait for an acknowledgement from the slave
    byte serial_data;
    uint64_t starttime = (uint64_t) millis();
    uint64_t nowtime = (uint64_t) millis();
    while (nowtime - starttime < 5000) {   // wait for maximal 5 Seconds
        while (Serial.available() > 0) {   // read Serial input
            serial_data = Serial.read();
            if (serial_data == 0xE5) {      // check for single character 0xE5, indicates ACK
                return true;
            }
        }
        nowtime = (uint64_t) millis();
    }
    return false;  // if no ACK arrives return false
}

// Function for reading data from the Serial input and storing it in an array.
// message: An array of bytes to store the data read from Serial input.
// return:  void - This function does not return any value.
// usecase: used after command REQ_U2D for recieving the data
void SensorMBus::readSerial(byte message[]) {
    int index = 0;
    uint64_t starttime = (uint64_t) millis();
    uint64_t nowtime = (uint64_t) millis();
    while (nowtime - starttime < 5000) {  // waiting for maximal 5 Seconds
        while (Serial.available() > 0) {
            message[index] = Serial.read();
            index++;
        }
        nowtime = (uint64_t) millis();
    }
    // Serial.write(message,72);   // write 72 elements from array message // debug message for heterm
}

// Function for waking up a MBus slave device by sending a wake-up sequence through Serial.
// num:     Number of wake-up sequences to send.
// delayMs: Delay in milliseconds after sending wake-up sequences before resuming program.
// return:  void - This function does not return any value.
void SensorMBus::wakeUp(int num, unsigned int delayMs) {
    Serial.end();
    delay(100);
    Serial.begin(2400, SERIAL_8N1);     // change Byte format of serial port to 8N1
    while (!Serial) {
        // wait till serial port is ready
    }
    for (int i = 0; i < num; i++) {     // write wake up sequenz
        Serial.write(0x55);
    }
    Serial.write(0x02);
    Serial.flush();                         // Waits for the transmission of outgoing serial data to complete
    Serial.end();
    uint64_t startDelay = (uint64_t) millis();    // setup for waiting
    uint64_t timeNow = (uint64_t) millis();
    Serial.begin(2400, SERIAL_8E1);         // change Byte format back to 8E1 needed for reading meter data
    while (!Serial) {
        // wait till serial port is ready
    }
    while (abs(timeNow - startDelay) < delayMs) {   // check that delayMs is waited befor resuming program
        timeNow = (uint64_t) millis();
    }
}

