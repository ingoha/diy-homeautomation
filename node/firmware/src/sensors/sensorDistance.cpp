// Arduino universalNode firmware
// This module handles the communication with an HC_SR04 distance sensor
// It also handles powering up and down the sensor to save battery
#include "config.h"
#include "config_board.h"
#include "Arduino.h"
#include "sensors/sensorDistance.h"
#include "utils.h"



// Constructor for the SensorDistance class that takes an integer argument aDeviceID.
// aDeviceID:   An integer specifying the device ID for the SensorDistance.
// return:      void - This function does not return any value.
SensorDistance::SensorDistance(int aDeviceID) : Device(aDeviceID) {
    sensor = new HCSR04(HC_SR04_TRIGGER, HC_SR04_ECHO);

    pinMode(HC_SR04_POWER, OUTPUT);
    digitalWrite(HC_SR04_POWER, HIGH);    // PNP-transistor --> Power inverted!
}

// Destructor. Frees used memory.
SensorDistance::~SensorDistance() {
    delete sensor;
}

// Handles the distance measurement for the SensorDistance object. Sends the value via RFM
// message:     A pointer to a Payload object that may be used for passing additional data, but is not used in this function.
// return:      void - This function does not return any value.
void SensorDistance::handle(Payload *message) {
    // Power on sensor
    digitalWrite(HC_SR04_POWER, LOW);
    // Wait for sensor to boot
    delay(HC_SR04_BOOTTIME_MS);
    // Get one measurement
    double value = sensor->dist();
    // Power down sensor
    digitalWrite(HC_SR04_POWER, HIGH);
    debugPrintf("Dist=", value);
    radio->send(deviceID, value);
    blinkNTimes(1);
    radio->sleep();
}
