#include "sensors/sensorD0.h"
#include "utils.h"
#include "config_board.h"

// Constructor for the SensorD0 class that takes an integer argument aDeviceID.
// aDeviceID:   An integer specifying the device ID for the SensorD0.
// return:      void - This function does not return any value.
SensorD0::SensorD0(int aDeviceID) : Device(aDeviceID) {
    Serial.begin(9600);
    while (!Serial) {
        // wait till serial port is ready
    }
}

SensorD0::~SensorD0() {
}
// Function to handle incoming data from D0 serial interface for the SensorD0 class.
// message:     A pointer to the Payload object that holds the incoming message.
// return:      void - This function does not return any value.

void SensorD0::handle(Payload *message) {
    time_1 = (uint64_t) millis();
    time_2 = (uint64_t) millis();
    D0_meter_Reading = -1;
    D0_current_Power = -1;
    // wait for some seconds:
    debugPrintln("Start waiting");
    while ((time_1 + 8000) > time_2) {  // waiting for 8 Seconds
        time_2 = (uint64_t) millis();
        while (Serial.available() > 0) {
            // read data from serial interface
            serial_data = Serial.read();
            // convert byteformate from 7E1 to 8N1 bye masking parity bit (alternative: use Serial.begin(9600, SERIAL_7E1,
            // when starting serial port, but debug is then not available over Arduino IDE Serial Monitor)
            serial_data = serial_data & 0b01111111;

            // check for end of line
            if (serial_data != '\n') {
                inString += (char)serial_data;
            }
            else {
                // Check for Meter Reading under code 1-0:1.8.0
                if (inString.indexOf("1-0:1.8.0") != -1) {
                    // Get meter reading from string and convert to float
                    D0_meter_Reading = (inString.substring(14, 30)).toFloat();
                    debugPrintf("Digital meter reading: ", D0_meter_Reading);
                }
                if (inString.indexOf("1-0:1.7.255") != -1) {
                    // Get current Power from string and convert to float
                    D0_current_Power = (inString.substring(16, 25)).toFloat();
                    debugPrintf("Power output: ", D0_current_Power);
                }
                inString = "";
            }
        }
    }
    debugPrintln("End waiting");

    // Send meter reading and current Power output with radio
    if (D0_meter_Reading != -1) {
        radio->send(nodeConfig->d0Smartmeter, D0_meter_Reading);
        delay(100);
    }
    radio->sleep();
}
