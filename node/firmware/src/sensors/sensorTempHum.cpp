#include "sensors/sensorTempHum.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

// Constructor for the SensorTempHum class.
// aDeviceID:   An integer representing the device ID for the SensorTempHum object.
// return:      N/A - This is a constructor and does not return any value.
SensorTempHum::SensorTempHum(int aDeviceID) : Device(aDeviceID) {
    blinkNTimes(3);
    dht->begin();
    return;
}

// Sends the Temp and Hum value via RFM to the Gateway if available
// message: A pointer to a Payload object, not needed here, only necessary for actors
// return:  void - This function does not return any value.
void SensorTempHum::handle(Payload *message) {
    radio->listenModeEnd();
    float h = this->dht->readHumidity();
    float t = this->dht->readTemperature();
    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
        debugPrintln("Read DHT failed");
        return;
    }

    debugPrintf("Hum= ", h);
    debugPrintf("Temp=", t);

    radio->send(nodeConfig->sensorTempHum, t);
    blinkNTimes(1);
    radio->send(nodeConfig->sensorHumidity, h);
    blinkNTimes(1);
    radio->sleep();
}
