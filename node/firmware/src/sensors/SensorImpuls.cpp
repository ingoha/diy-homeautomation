#include "sensors/SensorImpuls.h"
#include "config_board.h"
#include "Arduino.h"
#include "radio.h"
#include <SPI.h>
#include "utils.h"

float SensorImpuls::impulsMeterReading = 0;                 // variable for the last meter reading
float SensorImpuls::impulsesPerValue = 0;                   // variable for rotations per kWh/ m^3 o.ae.
volatile float SensorImpuls::impulses = 0;                  // number of impulses since last sending of meter reading
volatile unsigned int SensorImpuls::debounceTime = 0;       // current set debounce time of interrupt
volatile uint64_t SensorImpuls::last_interrupt_time = 0;

// Constructor for SensorImpuls class, which inherits from Device class.
// aDeviceID: Device ID for the SensorImpuls object.
// return: void - This constructor does not return any value.
SensorImpuls::SensorImpuls(int aDeviceID) : Device(aDeviceID) {
    // set starting value for ferraris_sensor
    this->impulsMeterReading = nodeConfig->startMeterReading;
    this->impulsesPerValue = nodeConfig->impulsesPerValue;;
    this->debounceTime = nodeConfig->debounceTime;
    // Ferraris counter interrupt driven
    attachInterrupt(digitalPinToInterrupt(IMPULS_SENSOR_PIN), this->impulsCounterInterrupt, CHANGE);
}
SensorImpuls::~SensorImpuls() {}

// Interrupt service routine: executed when sensor detects impulse
// return: void - This function does not return any value.
void SensorImpuls::impulsCounterInterrupt() {
    uint64_t interrupt_time = (uint64_t) millis();
    if (abs(interrupt_time - last_interrupt_time) > debounceTime) {
        // debounce interrupt with debounceTime
        // increase impulse counter by one:
        impulses++;
        last_interrupt_time = interrupt_time;
    }
}

// Function for sending meter value via radio
// message: A pointer to a Payload object, default value is NULL.
// return: void - This function does not return any value.
void SensorImpuls::handle(Payload *message) {
    // calculate meter reading:
    impulsMeterReading = impulsMeterReading + impulses / (impulsesPerValue * 2);
    // send value of meter:
    radio->send(nodeConfig->sensorImpuls, impulsMeterReading);
    // send number of impulses since last last sending
    delay(100);
    radio->send(nodeConfig->sensorImpulsNumPulses, impulses);
    debugPrintf("Impulses: ", impulses);
    // reset impulse counter:
    impulses = 0;
    radio->sleep();
}
