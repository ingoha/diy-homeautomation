#include "sensors/sensorI2CMeter.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

// Constructor for the SensorI2CMeter class that takes an integer argument aDeviceID.
// aDeviceID:   An integer specifying the device ID for the SensorI2CMeter.
// return:      void - This function does not return any value.
SensorI2CMeter::SensorI2CMeter(int aDeviceID): Device(aDeviceID) {
    blinkNTimes(3);
    Wire.begin();
    this->handle();
    return;
}

// Handles the communication with the I2C devices connected to the SensorI2CMeter object.
// message: A pointer to a Payload object, is not used in this function.
// return:  void - This function does not return any value.
void SensorI2CMeter::handle(Payload *message) {
    for (int i = 1; i <= nodeConfig->i2cMeterDevices; i++) {
        uint64_t I2C_data;

        byte a, b, c, d, e, f, g, h;

        Wire.requestFrom(i, 8);     // request 8 Bytes from slave with I2C adress i

        // read all 8 Bytes
        a = Wire.read();
        b = Wire.read();
        c = Wire.read();
        d = Wire.read();
        e = Wire.read();
        f = Wire.read();
        g = Wire.read();
        h = Wire.read();

        // put all 8 Bytes back together
        I2C_data = a;
        I2C_data = (I2C_data << 8) | b;
        I2C_data = (I2C_data << 8) | c;
        I2C_data = (I2C_data << 8) | d;
        I2C_data = (I2C_data << 8) | e;
        I2C_data = (I2C_data << 8) | f;
        I2C_data = (I2C_data << 8) | g;
        I2C_data = (I2C_data << 8) | h;

        // Calculate actual Meter Value
        float Meter_Reading = (float(I2C_data) / 1000);

        debugPrinti("I2C-Slave ID: ", i);
        debugPrintf("Zähler: ", Meter_Reading);
        // Send the Meter reading to the Gateway
        radio->send(nodeConfig->i2cMeter + i, Meter_Reading);
        blinkNTimes(1);
    }
    radio->sleep();
}
