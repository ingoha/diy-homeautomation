#include "config.h"
#include "sensors/sensorDigital.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

// Constructor for the SensorDigital class that takes an integer argument aDeviceID.
// aDeviceID:   An integer specifying the device ID for the SensorDigital.
// return:      void - This function does not return any value.
SensorDigital::SensorDigital(int aDeviceID) : Device(aDeviceID) {
    // Digital sensor is interrupt driven
    pinMode(DIGITAL_SENSOR_PIN, INPUT_PULLUP);  // Digital sensor
    attachInterrupt(digitalPinToInterrupt(DIGITAL_SENSOR_PIN), handleInterrupt, FALLING);
    return;
}


void SensorDigital::handle(Payload *message) {
}
// Function to handle interrupt event, send data via radio, blink LED, put radio to sleep, and does not return any value
// return:      void - This function does not return any value.
void SensorDigital::handleInterrupt() {
    radio->send(nodeConfig->sensorDigital, 1111);
    blinkNTimes(1);
    radio->sleep();
}

