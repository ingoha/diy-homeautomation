// Arduino universalNode firmware
// This file handles the radio communication

#include "radio.h"
#include "config.h"
#include "config_board.h"
#include "utils.h"

// Constructor and destructor
// return:      void - This function does not return any value.
RFMRadio::RFMRadio() {
  radio = new RFM69;
}
RFMRadio::~RFMRadio() {
  delete radio;
}

// Reads and calculates current battery voltage
// return:      float - This function does not return any value.
float RFMRadio::batteryVoltage() {
    // read the value at analog input
    int value = analogRead(BATTERY_PIN);
    float vout = (value * VOLTAGE) / 1024.0;
    float vin = vout / R1R2;
    if (vin < 0.09)
        vin = 0.0;  // statement to quash undesired reading !
    return vin;
    }

// Initializes the RFM radio
// return:      float - This function does not return any value
void RFMRadio::init() {
    if (!radio->initialize(nodeConfig->frequency, nodeConfig->nodeID, eNetworkID)) {
        while (true)
            blinkNTimes(5);
    }
    if (digitalRead(IS_HCW_PIN)) {
        radio->setHighPower();
    }
    if (String(nodeConfig->encryptKey) == "") {
        radio->encrypt(0);
    }
    else {
        radio->encrypt(nodeConfig->encryptKey);
    }
    radio->setPowerLevel(31);
    radio->sleep();
    // second signal: radio initialized
    blinkNTimes(1);
}

// Sends a value on a certain device ID to the gateway
// aDeviceID:   ID of the device (sensor/actor) sending the message
// aValue:      The sensor value to be sent
// return:      void - This function does not return any value.
void RFMRadio::send(int aDeviceID, float aValue) {
    // send data
    Payload msg;
    msg.nodeID = nodeConfig->nodeID;
    msg.deviceID = aDeviceID;
    msg.uptime_ms = (uint64_t)millis();
    msg.sensordata = aValue;
    msg.battery_volts = batteryVoltage();
    radio->send(nodeConfig->gatewayID, (const void*)(&msg), sizeof(msg));
}

// Function can be called in setup mode registers node at gateway
// randomNumber:    A random number which makes the node unique to the gateway
// return:          void - This function does not return any value
void RFMRadio::requestIdAndEncryptKey(uint32_t randomNumber) {
    RequestIdPayload msg;
    msg.nodeID = nodeConfig->nodeID;
    msg.deviceID = setupDeviceId;
    msg.randomNumber = randomNumber;
    radio->send(nodeConfig->gatewayID, (const void*)(&msg), sizeof(msg));
}

// Puts radio in sleep mode
// return:      void - This function does not return any value.
void RFMRadio::sleep() {
  radio->sleep();
}

// Encrypts radio with the key in the nodeconfiguration
// return:      void - This function does not return any value
void RFMRadio::encrypt() {
    radio->encrypt(nodeConfig->encryptKey);
}

// Reinitializes the radio with the current nodeid
// return:      bool - True if reinit was successful
bool RFMRadio::reinitialize() {
    return radio->initialize(nodeConfig->frequency, nodeConfig->nodeID, eNetworkID);
}

// Puts the radio in Listenmode for. Listens for 150microseconds and sleeps for 1ms
// return:      void - This function does not return any value.
void RFMRadio::listenModeStart() {
      uint32_t rx = 150;
      uint32_t i = 1000;
      radio->listenModeSetDurations(rx, i);
      radio->listenModeStart();
}

// Ends the listenMode of the radio
// return:      void - This function does not return any value.
void RFMRadio::listenModeEnd() {
    radio->listenModeEnd();
    radio->writeReg(0x3D, 0x01);
    return;
}

// Checks if a package can be received and if so makes the payload available in the msg-parameter
// msg:         Payload object in which the received data is written
// return:      bool - Returns true if message was received
bool RFMRadio::receiveDone(byte* msgBuffer, int* payloadSize) {
    if (radio->receiveDone()) {
        int msgLength = radio->PAYLOADLEN;
        memcpy(msgBuffer, radio->DATA, msgLength);
        if (radio->ACKRequested()) {
            radio->sendACK();
        }
        if (payloadSize != nullptr) {
            *payloadSize = msgLength;
        }
        return true;
    }
    else {
        return false;
    }
}

// Passes the config of the node as read-only to the radio class
// nodeConfig:  The node configuration as a const pointer
// return:      void - This function does not reaturn any value
void RFMRadio::passNodeConfig(const NodeConfiguration* aNodeConfig) {
    nodeConfig = aNodeConfig;
}
