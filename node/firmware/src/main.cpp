/*
Original Author: Eric Tsai
Updated by Ingo Haschler
License: CC-BY-SA, https://creativecommons.org/licenses/by-sa/2.0/
Date: 9-1-2014
Original File: UberSensor.ino
This sketch is for a wired Arduino w/ RFM69 wireless transceiver
Sends sensor data (gas/smoke, flame, PIR, noise, temp/humidity) back 
to gateway.  See OpenHAB configuration file.

Required libraries:
    • Adafruit Unified Sensor by Adafruit
    • DHT sensor library by Adafruit
    • RFM69_LowPowerLab by LowPowerLab
    • SPIFlash_LowPowerLab by LowPowerLab
    • Low-Power by Rocket Scream Electronics
    • HCSR04 by gamegine

Blink codes on startup: 1+2+3: everything OK, continous blinking: failed to init RFM module
*/
#include "node.h"
#include "config.h"
#include "utils.h"

Node node;


void setup() {
    node.setup();
}

void loop() {
    if (node.mode() == eSlaveMode) {
        node.masterSlaveMode();
    }
    else if (node.mode() == eIntervallMode) {
        node.intervallMode();
    }
    else if (node.mode() == eSetupMode) {
        node.setupMode();
    }
    else {
        node.handleUnknownMode();
    }
}
