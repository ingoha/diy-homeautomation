#include "device.h"
#include "Array.h"

Array<Device *, MAX_NUMBER_OF_DEVICES> Device::listOfDevices;

// Constructor for the Device class.
// aDeviceID:   An integer representing the device ID for the Device object.
// return:      N/A - This is a constructor and does not return any value.
Device::Device(int aDeviceID) {
    deviceID = aDeviceID;
    listOfDevices.push_back(this);
    return;
}

// Left empty because its a virtual function which is implemented in child-classes
// message: Pointer to a Payload object representing the incoming message.
// return:  void - This function does not return any value.
void Device::handle(Payload *message) {}

// Handles incoming message for all devices, or handles all devices in the list if no message is provided.
// message: Pointer to a Payload object representing the incoming message. If nullptr, all devices (sensors and actors) in the list will be handled.
// return:  void - This function does not return any value.
void Device::handleListOfDevices(Payload *message) {
    if (message) {
        for (unsigned int i = 0; i < listOfDevices.size(); i += 1) {
            if ((int)message->deviceID == listOfDevices[i]->deviceID) {
                listOfDevices[i]->handle(message);
                return;
            }
        }
    }
    else {
        for (unsigned int i = 0; i < listOfDevices.size(); i += 1) {
            listOfDevices[i]->handle();
            blinkNTimes(1);
        }
        return;
    }
}

RFMRadio *Device::radio;
// Sets the RFMRadio object for the Device class.
// aRadio:  Pointer to an RFMRadio object to be set as the radio for the Device class.
// return:  void - This function does not return any value.
void Device::setRadio(RFMRadio *aRadio) {
    radio = aRadio;
    return;
}

const NodeConfiguration *Device::nodeConfig;
// Sets NodeConfig of the Node as read only. All sensors can acces the config with nodeConfig-><element>
// aNodeConfig: Pointer to a NodeConfiguration object which is read only
// return:  void - This function does not return any value.
void Device::setNodeConfig(const NodeConfiguration *aNodeConfig) {
    nodeConfig = aNodeConfig;
    return;
}
