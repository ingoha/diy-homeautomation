#include "config.h"

#include "actorDeko.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

// Constructor for the ActorDeko class.
// aDeviceID:   An integer representing the device ID for the ActorDeko object. Also calls constructor of device class
// return:      N/A - This is a constructor and does not return any value.
ActorDeko::ActorDeko(int aDeviceID) : Device(aDeviceID) {
    pinMode(DEKO, OUTPUT);
    digitalWrite(DEKO, LOW);
    return;
}

// Handles incoming message for the ActorDeko class. Turns LED of or on
// message: Pointer to a Payload object representing the incoming message.
// return:  void - This function does not return any value.
void ActorDeko::handle(Payload *message) {
    if (message) {
        if (message->sensordata == 0) {
            digitalWrite(DEKO, HIGH);
            radio->send(nodeConfig->dekoElement, 1);
        }
        if (message->sensordata == 1) {
            digitalWrite(DEKO, LOW);
            radio->send(nodeConfig->dekoElement, 0);
        }
    }
    return;
}
