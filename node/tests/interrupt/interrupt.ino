/*
Author: Ingo Haschler
 Test program for breadboard arduino
 Components covered:
 * voltage regulator
 * ATmega
 * LED
 Expected result: LED blinks 
 */


#include <SPI.h>
#include <SD.h>
#include "config.h"
#include <RFM69.h>
#include <LowPower.h>

RFM69 radio;


#define interruptPin         3// breadboard arduino (low power) has LED on pin 8



//struct for wireless data transmission
typedef struct {
    int       nodeID;     //node ID (1xx, 2xx, 3xx);  1xx = basement, 2xx = main floor, 3xx = outside
    int       deviceID;   //sensor ID (2, 3, 4, 5)
    unsigned long   uptime_ms;    //uptime in ms
    float     sensordata;     //sensor data?
    float     battery_volts;    //battery condition?
} Payload;


void setup()
{
  pinMode(interruptPin, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  

  // Radio einstellen
  // Wenn Fehler schnelles Blinken
  if(!radio.initialize(FREQUENCY, NODEID, NETWORKID))
  {
    while(true)
    {
      digitalWrite(LED, HIGH);
      delay(100);
      digitalWrite(LED, LOW);
      delay(100);
      digitalWrite(LED, HIGH);
      delay(100);
      digitalWrite(LED, LOW);
      delay(100);
      digitalWrite(LED, HIGH);
      delay(100);
      digitalWrite(LED, LOW);
      delay(100);
      digitalWrite(LED, HIGH);
      delay(100);
      digitalWrite(LED, LOW);
      delay(100);
    }
  }
  radio.setHighPower();
  radio.encrypt(ENCRYPTKEY);
  radio.setPowerLevel(31);
  Serial.begin(SERIAL_BAUD);

  attachInterrupt(digitalPinToInterrupt(interruptPin),wake,FALLING);

}

void wake()
{
  // lässt uC Aufwecken einfügen
  delay(100);
  bool transmit = false;

  // Nachricht
  Payload msg;
  msg.nodeID = NODEID;
  msg.deviceID = GATEWAYID;
  msg.uptime_ms = millis();
  msg.sensordata=1.23;
  msg.battery_volts=1.23;

  // Logik
  while(!transmit)
  {
    if(radio.sendWithRetry(GATEWAYID, (const void*)(&msg), sizeof(msg)))
    {
      
     
      digitalWrite(LED, HIGH);
      transmit = true;
      
    }
    else 
    {
      digitalWrite(LED,HIGH);
      delay(1000);
      digitalWrite(LED, LOW);
      delay(1000); 
      digitalWrite(LED, HIGH);
      delay(1000);
      digitalWrite(LED, LOW);
      break;
    }
  }

 
   
   

}
void loop()
{
   // Standby, indem Interrupts bemerkt werden!!
   // Wenn Interrupt ausgelöst -> uC weckt auf, schickt Nachricht an Gateway und schläft wieder
   // Wenn Nachricht erfolgreich übermittelt, dann LED dauerhaft an
   
   
   
   LowPower.powerStandby(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
  
      
}







