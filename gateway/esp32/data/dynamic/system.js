const gateway = `ws://${window.location.hostname}/ws`;
let websocket;


// Handles the "DOMContentLoaded" event of the document
// This event is triggered when the HTML document has been completely loaded and parsed
// Used for initializing the WebSocket connection and setting the current date
document.addEventListener("DOMContentLoaded", function() { 
    // WebSocket event handler for the "open" event
    // This event is triggered when the WebSocket connection is successfully established
    function onOpen(event) {
        console.log('Connection opened');
    }

    // WebSocket event handler for the "close" event
    // This event is triggered when the WebSocket connection is closed
    // It attempts to reconnect after a delay of 2000 milliseconds
    function onClose(event) {
        console.log('Connection closed');
        setTimeout(initWebSocket, 2000);
    }
      
    // WebSocket event handler for the "message" event
    // This event is triggered when a message is received over the WebSocket connection 
    function onMessage(event) {
        console.log(event);
        const message = JSON.parse(event.data);
        console.log(message);
    }

    
    // Initializes the WebSocket connection
    // It creates a new WebSocket instance and assigns event handlers
    function initWebSocket() {
        websocket = new WebSocket(gateway);
        websocket.onopen    = onOpen;
        websocket.onclose   = onClose;
        websocket.onmessage = onMessage; // <-- add this line
    }
    
    
    
    // Retrieves the current date and formats it as "day.month.year"
    // Returns the formatted date string
    function getDate() {
        const currentDate = new Date();
        const day = currentDate.getDate();
        const month = currentDate.getMonth() + 1;
        const year = currentDate.getFullYear();
        return `${day}.${month}.${year}`;
    }
    
    const date = document.getElementById("date");
    
    
    initWebSocket();
    date.textContent = getDate();


});
