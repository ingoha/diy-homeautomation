class Utills {
  constructor() {

  }
  
  // Get the integer value from the end of an element's ID
  getIntOfIdElementIfItIsTheLastPart(id) {
    //https://regex101.com/r/TiRIo8/1
    const match = id.match(/\d*($|\s*$)/); // Sucht nach der Zahl am Ende des Strings
    const numberString = match ? match[0] : ""; // Extrahiert die gefundene Zahl oder gibt einen leeren String zurück
  
    const number = parseInt(numberString);
    if (isNaN(number)) {
      console.log("Der Wert ist kein Integer");
      return id; // oder einen geeigneten Standardwert zurückgeben
    }
  
    return number;
  }

  // Traverse up the DOM tree to find a specific parent element by tag name
    goToThisParentElement(element, elementName) {
      let searchElement = element.currentTarget;
      while(!(searchElement.tagName === elementName)) {
          searchElement = searchElement.parentNode;
      }

      // console.log(searchElement);
      return searchElement;

  }

  // Traverse up the DOM tree to find a specific parent element by class name
    goToThisParentClass(element, className) {
      let searchElement = element.currentTarget;
      while(!(searchElement.classList.contains(className))) {
          searchElement = searchElement.parentNode;
      }

      // console.log(searchElement);
      return searchElement;
  }

  // Remove the "hide" class from an element and return true if it was present
  removeHideClass(element) {
    if (element.classList.contains("hide")) {
      element.classList.remove("hide");
      return true;
    } else {
        return false;
    }
  }

  // Add the "hide" class to an element
  addHideClass(element) {
    if (element) {
      element.classList.add("hide");
    }
  }

  // Fetch JSON data from a specified URL and handle errors
  async getJson (url) {
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Meine eigene Exeption: ${response.status} ${response.statusText}`);
        }
        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
        throw new Error('Failed to fetch JSON data');
    }
  }

  // Initialize the date element with the current date
  async  initDate() {
    const date = document.getElementById("date");
    date.textContent = getDate();

    function getDate() {
      const currentDate = new Date();
      const day = currentDate.getDate();
      const month = currentDate.getMonth() + 1;
      const year = currentDate.getFullYear();
      return `${day}.${month}.${year}`;
    }
  }

  // Toggle the fade-out class for an element
  async togglefadeOutClass(event){
    const iElement = event.currentTarget.querySelector("i");
    iElement.classList.add("fade-out");
    setTimeout(function() {
        iElement.classList.remove("fade-out");
    }, 1000);
}

}
  
  // Creating an instance of MyClass
  const utills = new Utills();