//objects for link to the possible actions send back to webserver via websocket
const websocketTypes = {
  deleteNode: 1,
  changeName: 2,
  acceptNewNodes: 3,
  sendKey: 4,
  led: 5,
  sendConfiguration: 6,
  resetNode: 7,
};

//objects for link to the operation modes types
const operationModes = {
  setupMode: 0,
  deleteNode: 1,
  changeName: 2,
};

//objects for link to the dht types
const dhtTypes = {
  dht_11: 11,
  dht_12: 12,
  dht_21: 21,
  dht_22: 22,
};
