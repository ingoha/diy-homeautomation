class BuildNewNodesTable  {
    constructor() {
        this.table = document.getElementById("new-nodes-table");
        this.tableBody = this.table.querySelector('tbody');
        this.baseRow = this.tableBody.querySelector('tr.new-node');
        this.noNodesRow = this.tableBody.querySelector('tr.no-new-nodes');
    }

    // Clones a new row in the table
    async cloneNewRow(id, name, statusId) {
        const newRow = this.baseRow.cloneNode(true);
        newRow.classList.remove("hide");
        newRow.id = `node_${id}`; 
        this._changeNewRow(newRow, id, name, statusId);
        this.tableBody.appendChild(newRow);
    }

    // Displays the "No new nodes" row
    async displayNoNewNodeRow() {
        this.noNodesRow.classList.remove("hide");
    }

    // Sends a key to the node
    async _sendKeyToNode(event) {
        //beim drücken, dann socket nachricht mit 
        const selectedRow = utills.goToThisParentElement(event, "TR");
        // console.log(selectedRow);
        websocket.send(JSON.stringify({
            "type": websocketTypes.sendKey,
            "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(selectedRow.id),
        }));
    }

    // Changes the name of the node
    async _changeNodeName(event) {
        const element = event.currentTarget.parentNode;
        const testElement = element.querySelector(".text");
        //SweetAlert2
        let newNodeName = window.prompt("Insert the new name: ", testElement.innerHTML); 
        if (newNodeName === "" || newNodeName === null || newNodeName === undefined) {
            return
        }
        testElement.textContent = newNodeName;
        websocket.send(JSON.stringify({
            "type": websocketTypes.changeName,
            "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(utills.goToThisParentElement(event, "TR").id),
            "nodeName": newNodeName
        }));
    }

    // Deletes the node
    async _deleteThisNode(event) {
        const element = event.currentTarget.parentNode;
        console.log(element);
        const testElement = element.querySelector(".text");
        const nodeContainer = utills.goToThisParentElement(event, "TR");
        if (confirm(`Really delete the Node ${testElement.textContent}?` )){
            websocket.send(JSON.stringify({
                "type": websocketTypes.deleteNode,
                "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(nodeContainer.id),
            }));
            console.log("nodeId", utills.getIntOfIdElementIfItIsTheLastPart(nodeContainer.id));
            //nodeContainer.remove();
            return;
        }else{
            return;
        }
    }

    // Toggles the LED status of the node
    async _toggleLED(event) {
        const parent = utills.goToThisParentClass(event, "name-cell" );

        const onLed = parent.querySelector("img.icon-on");
        const offLed = parent.querySelector("img.icon-off");

        // console.log(onLed);
        // console.log(offLed);

        if(onLed.classList.contains("hide")) {
            // onLed.classList.remove("hide");
            // offLed.classList.add("hide");
            websocket.send(JSON.stringify({
                "type": websocketTypes.led,
                "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(utills.goToThisParentElement(event, "TR").id),
                "value": true
            }));
        } else {
            if(offLed.classList.contains("hide")){
                // offLed.classList.remove("hide");
                // onLed.classList.add("hide");
                websocket.send(JSON.stringify({
                    "type": websocketTypes.led,
                    "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(utills.goToThisParentElement(event, "TR").id),
                    "value": false
                }));
            }
        }
    }

    // async _setNodeConfigurationTable(node, table){
    //     const nodeIdNumber = node.id.match(/\d*($|\s*$)/)[0];
    //     // const tableHead2 = table.querySelector(".node-header p");
    //     const tableHead = table.querySelector("caption .text");
    
    //     table.id = `node_${nodeIdNumber}`;
    //     tableHead.textContent = node.querySelector(" [data-cell='name'] .name-cell .text").textContent;
    // }

    // Shows the configuration table for the node
    async _showConfigNodeTable(event) {
        function _idNumberOfBothElementsIsEqual(element1, element2){
            if (element1 == null || element2 == null) {
                console.log("on element is null: ");
                console.log("element 1:" , element1);
                console.log("element 2:" , element2);
                return null;
            }
        
            if (element1.id == null || element2.id == null) {
                console.log("on element is null: ");
                console.log("element 1:" , element1);
                console.log("element 2:" , element2);
                return null;
            }
            
            const element2IdNumber = utills.getIntOfIdElementIfItIsTheLastPart(element2.id);
            const element1IdNumber = utills.getIntOfIdElementIfItIsTheLastPart(element1.id);
        
            if (element2IdNumber === element1IdNumber ) {
                return true;
            }
            return false;
        }

        async function _setNodeConfigurationTable(node, table){
            const nodeIdNumber = node.id.match(/\d*($|\s*$)/)[0];
            // const tableHead2 = table.querySelector(".node-header p");
            const tableHead = table.querySelector("caption .text");
        
            table.id = `node_${nodeIdNumber}`;
            tableHead.textContent = node.querySelector(" [data-cell='name'] .name-cell .text").textContent;
        }
        
        const selectedRow = utills.goToThisParentElement(event, "TR");
        const nodeTableDiv = document.querySelector(".new-nodes-configuration");
        const table = nodeTableDiv.querySelector(".new-nodes-configuration-table");
        let removed = utills.removeHideClass(nodeTableDiv);
        if (!removed){
            //hide table if clicked at same name
            if(_idNumberOfBothElementsIsEqual(selectedRow, table)) {
                utills.addHideClass(nodeTableDiv);
                return;
            }
        }
        _setNodeConfigurationTable(selectedRow, table);
        table.scrollIntoView({ behavior: "smooth" });
    }

    // Changes the content of the new row
    async _changeNewRow(rowElement, id, jsonName, statusId) {
        const actionCell = rowElement.querySelector('td[data-cell="action"]');
        const nameCell = rowElement.querySelector('td[data-cell="name"]');
        const statusCell = rowElement.querySelector('td[data-cell="status"]');

        let status = "none";

        if (statusId === 1) {
            status = "Wait for Key";
            actionCell.querySelector(".config-text").classList.add("hide");
            actionCell.querySelector(".key-button").classList.remove("hide");
            const sendKeyButton = actionCell.querySelector(".key-button button");
            sendKeyButton.addEventListener("click", this._sendKeyToNode, false); 
            if (sendKeyButton.querySelector("i") !== null) {
                sendKeyButton.addEventListener("click", utills.togglefadeOutClass, false); 
            }

        }else if (statusId === 2){
            status = "Wait for Configuration";
            actionCell.querySelector(".config-text").classList.remove("hide");
            actionCell.querySelector(".key-button").classList.add("hide");
            const openConfigurationButton = actionCell.querySelector(".config-text button");
            openConfigurationButton.addEventListener("click", this._showConfigNodeTable, false); 
        }

        const changeNameButton = nameCell.querySelector("img.change-name-icon");
        changeNameButton.addEventListener("click", this._changeNodeName, false);
        const deleteButton = nameCell.querySelector("img.delete-icon");
        deleteButton.addEventListener("click", this._deleteThisNode, false);
        const ledOn = nameCell.querySelector('img.led.icon-on');
        // console.log("Info über led: ", ledOn);
        ledOn.addEventListener("click", this._toggleLED, false);
        const ledOff = nameCell.querySelector('img.led.icon-off');
        // console.log("Info über led: ", ledOff);
        ledOff.addEventListener("click", this._toggleLED, false);

        let name = jsonName;

        if (name === "" || name === null || name === undefined) {
            name = "Node " + id;
        }
        nameCell.querySelector("span.text").textContent = name;
        statusCell.textContent = status;
        console.log();
    }
}