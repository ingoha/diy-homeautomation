class BuildNodes  {
    constructor() {
        this.urlAllNodesData = "/api/allNodesData";
        this.urlAllActiveNodesNameId = "/api/allActiveNodesIdName";
    }

    // Returns the URL for fetching all nodes data.
    _getUrlAllNodesData () {
        return this.urlAllNodesData;
    }

    // Returns the URL for fetching the names and IDs of all active nodes.
    getUrlAllActiveNodesNameId () {
        return this.urlAllActiveNodesNameId;
    }

    // Converts the given timestamp into a human-readable format and fills it into the lastValUpdatedTextElement.
    async _fillLastUpdatedValueText(timeStamp, lastValUpdatedTextElement) {
        console.log(timeStamp);

        let text = "";

        if (timeStamp > 60) {
            const minutes = Math.floor(timeStamp / 60);
            text = `${minutes} min. ago`;
        } else if (timeStamp > 60 * 60) {
            const hours = Math.floor(timeStamp / (60 * 60));
            text = `${hours} hours ago`;
        } else if (timeStamp > 60 * 60 * 24) {
            const days = Math.floor(timeStamp / (60 * 60 * 24));
            text = `${days} days ago`;
        } else if (timeStamp > 60 * 60 * 24 * 30) {
            const months = Math.floor(timeStamp / (60 * 60 * 24 * 30));
            text = `${months} months ago`;
        } else if (timeStamp > 60 * 60 * 24 * 30 * 12) {
            const years = Math.floor(timeStamp / (60 * 60 * 24 * 30 * 12));
            text = `${years} years ago`;
        } else {
            text = `${timeStamp} sec. ago`;
        }

        lastValUpdatedTextElement.textContent = text;
    }

    // Retrieves the card element and text element based on the given key, removes the hide class from the card element, and fills the value and unit string into the text element.
    async _fillValuesWithUnitInCardElementAndDisplayIt( nodeElement, key, value, unitString) {
        try {
            const cardElement = nodeElement.querySelector(`.${key}`);
            utills.removeHideClass(cardElement);
            const textElement = cardElement.querySelector(".value-text");
            console.log("value + unit: " + value + unitString);
            console.log("card: " + cardElement);
            console.log("textElement: " + textElement);
            textElement.textContent = value + unitString;
        }catch (error) {
            console.error(error);        
        }

    }

    // Includes all nodes' names and IDs to the page by cloning an example node element, setting the name and ID, and appending it to the nodes container.
    async includeAllNodesByNameAndIdToPage(nodes) {
        console.log(nodes);
        
        const nodesContainer = document.querySelector('.all-Nodes');
        const exampleNodesElement = document.querySelector('main .nodes.nodes-example');
        console.log(exampleNodesElement);
        var ids = new Array();

        nodes.forEach((element) => {
            let name = element.name;
            const id = element.id;
            // this.includeOneNodeToContainer(element.name, element.id, exampleNodesElement, nodesContainer);
            const clonedElement = exampleNodesElement.cloneNode(true);
            console.log(clonedElement);
            clonedElement.classList.remove('hide');
            clonedElement.id = `node_${id}`;
            if (name === "" || name === null || name === undefined) {
                name = "Node " + id;
            }
            clonedElement.querySelector("span.text").textContent = name;
            this._handleChangeNameImage(clonedElement);
            this._handleDeleteImage(clonedElement);
            nodesContainer.appendChild(clonedElement);
            ids.push(id);
          });

        return ids;
    }

    // Evaluates the JSON element and builds the corresponding card elements based on the available data.
    async evaluateJsonElementToBuildAllCards( element) {
        if (!("id" in element)) {
            return;
        }

        const nodeElement = document.getElementById(`node_${element.id}`);
        if (nodeElement === null) {
            return;
        }

        if ("lastValUpd" in element) {
            const lastValUpdated = nodeElement.querySelector(".lastValUpd");
            utills.removeHideClass(lastValUpdated);
            const lastValUpdatedTextElement = lastValUpdated.querySelector(".value-text");
            this._fillLastUpdatedValueText(element.lastValUpd, lastValUpdatedTextElement);
            // lastValUpdatedTextElement.textContent = this.fillLastUpdatedValueText(element.lastValUpd);
        }

        if ("temperature" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "temperature" , element.temperature, " °C");
        }

        if ("humidity" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "humidity" , element.humidity, " %");
        }

        if ("rssi" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "rssi" , element.rssi, "");
        }

        if ("battery" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "battery" , element.battery, " V");
        }

        if ("digSens" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "digSens" , element.digSens, "");
        }

        if ("deko" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "deko" , element.deko, "");
        }

        if ("distance" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "distance" , element.distance, "cm");
        }

        if ("I2c" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "I2c" , element.I2c.value, element.I2c.unit);
        }

        if ("MBusOrD0" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "MBusOrD0" , element.MBusOrD0.value, element.MBusOrD0.unit);
        }

        if ("impSens" in element) {
            this._fillValuesWithUnitInCardElementAndDisplayIt(nodeElement, "impSens" , element.impSens.value, element.impSens.unit);
        }

        return;
    }

    // Retrieves all nodes data from the specified URL with parameters and builds the corresponding card elements.
    async _getAndBuildAllNodesData(urlWithParams) {
        const nodesData = await utills.getJson(urlWithParams);
        console.log(nodesData);

        nodesData.forEach(element => {
            this.evaluateJsonElementToBuildAllCards(element);
        });
    }

    // Fills data to the nodes based on their IDs, fetching and building the data in batches.
    async _fillDataToNodes(ids) {
        const nodeCount = ids.length;
        const iterations = Math.trunc(nodeCount/5);
        const restNodes = nodeCount % 5;

        console.log(nodeCount);
        console.log(iterations);
        console.log(restNodes);
        
        for (let i = 1; i <= iterations; i++) {
            //select from ids the first next 5 nodes.
            const firstArrayIndex = (i-1) * 5;
            const lastArrayIndex = firstArrayIndex + 4;
            let urlWithParams = `${this._getUrlAllNodesData()}?`;
            let numberIterations = 1;
            
            for (let index = firstArrayIndex; index <= lastArrayIndex; index++) {
                urlWithParams += `NodeNr_${numberIterations}=${ids[index]}&`;
                numberIterations++;
            }
            
            console.log(urlWithParams);
            this._getAndBuildAllNodesData(urlWithParams);
        }

        if ( restNodes > 0 ) {
            let urlWithParams = `${this._getUrlAllNodesData()}?`;
            const lastNodeIndexEnd = nodeCount - 1;
            const lastNodeIndexStart = nodeCount - restNodes;
            let numberIterations = 1;
            
            for (let index = lastNodeIndexStart; index <= lastNodeIndexEnd; index++) {
                urlWithParams += `NodeNr_${numberIterations}=${ids[index]}&`;
                numberIterations++;
            }
            
            console.log(urlWithParams);
            this._getAndBuildAllNodesData(urlWithParams);
        }

        
    }

    // async includeOneNodeToContainer(name, id ,exampleNodesElement, nodesContainer) {
    //     // if (exampleNodesElement == null) {
    //     //     const exampleNodesElement = document.querySelector('main .nodes');
    //     //     console.log(exampleNodesElement);
    //     // }
    //     // if (nodesContainer == null) {
    //     //     const nodesContainer = document.querySelector('.all-Nodes');
    //     //     console.log(nodesContainer);
    //     // }
    //     // const clonedElement = exampleNodesElement.cloneNode(true);
    //     // console.log(clonedElement);
    //     // clonedElement.classList.remove('hide');
    //     // clonedElement.id = `node_${id}`;
    //     // if (name === "" || name === null || name === undefined) {
    //     //     name = "Node " + id;
    //     // }
    //     // clonedElement.querySelector("span.text").textContent = name;
    //     // this._handleChangeNameImage(clonedElement);
    //     // this._handleDeleteImage(clonedElement);
    //     // nodesContainer.appendChild(clonedElement);
    // }

    // Builds all data nodes by including the nodes' names and IDs to the page and filling the data for each node.
    async buildAllDataNodes(jsonWithIdAndName) {
        try {
            console.log(jsonWithIdAndName);
            var ids = await this.includeAllNodesByNameAndIdToPage(jsonWithIdAndName);
            this._fillDataToNodes(ids);
        } catch (error) {
            console.error(error);
        }
    }

    // Handles the change name functionality by attaching a click event listener to the change name icon.
    async _handleChangeNameImage(elementWithChangeNameImage) {

        function changeNodeName(event) {
            const element = event.currentTarget.parentNode;
            const testElement = element.querySelector(".text");
            //SweetAlert2
            let newNodeName = window.prompt("Insert the new name: ", testElement.textContent); 
            if (newNodeName === null) {
                return;
            }
            //testElement.textContent = newNodeName;
            const nodeContainer = utills.goToThisParentClass(event, "nodes");
            websocket.send(JSON.stringify({
                "type": websocketTypes.changeName,
                "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(nodeContainer.id),
                "nodeName": newNodeName
            }));
        }

        const changeName = elementWithChangeNameImage.querySelector("img.change-name-icon");

        if (changeName !== null) {
            changeName.addEventListener("click", changeNodeName, false); //capture or bubbling phase -> was wird zuerst ausgeführt, also wie propagiert
        }else {
            console.log("Problem: no Image found" );
        }
    }

    // Handles the delete node functionality by attaching a click event listener to the delete icon.
    async _handleDeleteImage(elementWithChangeNameImage) {

        function deleteThisNode(event) {
            const element = event.currentTarget.parentNode;
            console.log(element);
            const testElement = element.querySelector(".text");
            const nodeContainer = utills.goToThisParentClass(event, "nodes");
            if (confirm(`Really delete the Node ${testElement.textContent}?` )){
                websocket.send(JSON.stringify({
                    "type": websocketTypes.deleteNode,
                    "nodeId": utills.getIntOfIdElementIfItIsTheLastPart(nodeContainer.id),
                }));
                console.log("nodeId", utills.getIntOfIdElementIfItIsTheLastPart(nodeContainer.id));
                //nodeContainer.remove();
                return;
            }else{
                return;
            }
        }

        const deleteNode = elementWithChangeNameImage.querySelector("img.delete-icon");
        if (deleteNode !== null) {
            deleteNode.addEventListener("click", deleteThisNode, false); //capture or bubbling phase -> was wird zuerst ausgeführt, also wie propagiert
        }else {
            console.log("Problem: no Image found" );
        }
    }

}