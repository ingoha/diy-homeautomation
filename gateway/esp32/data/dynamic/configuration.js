const gateway = `ws://${window.location.hostname}/ws`;
const websocket = new WebSocket(gateway);

window.addEventListener("load", function () {
  // Function to toggle fade-out class for an element
  function togglefadeOutClass(event) {
    const iElement = event.currentTarget.querySelector("i");
    iElement.classList.add("fade-out");
    setTimeout(function () {
      iElement.classList.remove("fade-out");
    }, 1000);
  }
  // Function to handle name change
  async function handleChangeName() {
    function changeNodeName(event) {
      const element = event.currentTarget.parentNode;
      const testElement = element.querySelector(".text");
      //SweetAlert2
      let newNodeName = window.prompt(
        "Insert the new name: ",
        testElement.innerHTML
      );
      if (newNodeName === null) {
        return;
      }
      // testElement.textContent = newNodeName;
      websocket.send(
        JSON.stringify({
          type: websocketTypes.changeName,
          nodeId: utills.getIntOfIdElementIfItIsTheLastPart(
            utills.goToThisParentElement(event, "TABLE").id
          ),
          nodeName: newNodeName,
        })
      );
    }

    const changeName = document.querySelector(
      "table.new-nodes-configuration-table img.change-name-icon"
    );
    changeName.addEventListener("click", changeNodeName, false); //capture or bubbling phase -> was wird zuerst ausgeführt, also wie propagiert
  }
  
  // Function to handle inter table arrows
  async function handleInterTableArrows() {
    function toggleTable(event) {
      const parent = utills.goToThisParentClass(event, "table-flex-container");
      const tableElement = parent.querySelector(".intern-table");

      if (tableElement.classList.contains("hide")) {
        tableElement.classList.remove("hide");
      } else {
        tableElement.classList.add("hide");
      }
    }

    const internTableArrows = document.querySelectorAll(
      "table .text-expand .toggle-table"
    );

    for (let i = 0; i < internTableArrows.length; i++) {
      internTableArrows[i].addEventListener("click", toggleTable, false); //capture or bubbling phase -> was wird zuerst ausgeführt, also wie propagiert
    }
  }

  // Function to handle all sliders
  async function handleAllSliders() {
    function showSlideValue(event) {
      const slider = event.currentTarget;
      const inputFiled = slider.querySelector('input[type="checkbox"]');

      // const value = inputFiled.checked ? 'on' : 'off';
      const value_bool = inputFiled.checked;
      // console.log('Slider value changed to: ' + value);
      console.log("Slider value changed to: " + value_bool);
    }

    const allSliders = document.querySelectorAll(".switch");
    for (let i = 0; i < allSliders.length; i++) {
      allSliders[i].addEventListener("input", showSlideValue, false);
    }
  }

  // Function to handle Accept New Nodes button
  async function handleAcceptNewNodesButton() {
    function acceptNewNodes() {
      websocket.send(
        JSON.stringify({
          type: websocketTypes.acceptNewNodes,
        })
      );
    }

    const newNodesButton = document.querySelector(
      "table.new-nodes-table caption button"
    );
    newNodesButton.addEventListener("click", acceptNewNodes, false);
  }

  // Function to handle all slider rows
  async function handleAllSliderRows() {
    const allSliderRows = document.querySelectorAll(".switch");
  }

  // Function to handle node reset
  async function handleResetNode() {
    function sendResetWithConfirm(event) {
      let row = event.currentTarget;

      while (!row.classList.contains("right-cell")) {
        row = row.parentNode;
      }

      const selectElement = row.querySelector("select");
      const selectedOption = selectElement.options[selectElement.selectedIndex];
      const selectedText = selectedOption.text;
      if (confirm(`Reset the Node: ${selectedText}?`)) {
        const selectedIdValue = selectedOption.value;
        websocket.send(
          JSON.stringify({
            type: websocketTypes.resetNode,
            nodeId: utills.getIntOfIdElementIfItIsTheLastPart(selectedIdValue),
          })
        );
        togglefadeOutClass(event);
        return;
      } else {
        event.stopPropagation();
      }
    }

    const resetNode = document.querySelector(
      "table .reset-node .right-cell .reset-outerButton button"
    );
    resetNode.removeEventListener("click", togglefadeOutClass, false);
    resetNode.addEventListener("click", sendResetWithConfirm, false);
  }

  // Function to handle sending configuration
  async function handleSendConfiguration() {
    function getOpModeSelectionValue(row) {
      const dataCell = row.cells[1];
      const selectElement = dataCell.querySelector("select");
      const value = parseInt(
        selectElement.options[selectElement.selectedIndex].value
      );
      if (value === 0) {
        return operationModes.setupMode;
      } else if (value === 1) {
        return operationModes.deleteNode;
      } else if (value === 2) {
        return operationModes.changeName;
      } else {
        return null;
      }
    }

    function getDHTSelectionValue(row) {
      const dataCell = row.cells[1];
      const selectElement = dataCell.querySelector("select");
      const value = parseInt(
        selectElement.options[selectElement.selectedIndex].value
      );
      if (value === 11) {
        return dhtTypes.dht_11;
      } else if (value === 12) {
        return dhtTypes.dht_12;
      } else if (value === 21) {
        return dhtTypes.dht_21;
      } else if (value === 22) {
        return dhtTypes.dht_22;
      } else {
        return null;
      }
    }

    function inputElement(row) {
      const dataCell = row.cells[1];
      const inputElement = dataCell.querySelector("input");
      return inputElement.value;
    }

    function multipleInputFileds(row) {
      const internJson = {};
      const dataCell = row.cells[1];
      const inputElements = dataCell.querySelectorAll("input");
      inputElements.forEach((element) => {
        internJson[element.id] = element.value;
      });
      return internJson;
    }

    function sliderElement(row) {
      const dataCell = row.cells[1];
      const inputFiled = dataCell.querySelector('input[type="checkbox"]');
      return inputFiled.checked;
    }

    function ImpulseSensorValue(row) {
      const internJson = {};
      internJson["active"] = sliderElement(row);
      const infoCell = row.cells[0];
      const innerTable = infoCell.querySelector(".intern-table table tbody");
      internJson["startMeterReading"] = multipleInputFileds(innerTable.rows[0]);
      internJson["impulsesPerValue"] = inputElement(innerTable.rows[1]);
      internJson["debounceTime"] = inputElement(innerTable.rows[2]);
      return internJson;
    }

    function InternTableOneInput(row) {
      const internJson = {};
      internJson["active"] = sliderElement(row);
      const infoCell = row.cells[0];
      const innerTable = infoCell.querySelector(".intern-table table tbody");
      internJson["unit"] = inputElement(innerTable.rows[0]);
      return internJson;
    }

    function I2CMeterValue(row) {
      const internJson = {};
      internJson["active"] = sliderElement(row);
      const infoCell = row.cells[0];
      const innerTable = infoCell.querySelector(".intern-table table tbody");
      internJson["I2CDeviceCount"] = inputElement(innerTable.rows[0]);
      internJson["unit"] = inputElement(innerTable.rows[1]);
      return internJson;
    }

    function nodeConfigurationCollection(table) {
      const tableBody = table.querySelector("tbody");
      let myJSON = {};

      const operationModes = tableBody.rows[0];
      const delaySendingData = tableBody.rows[1];
      const digitalSensor = tableBody.rows[2];
      const distanceSensor = tableBody.rows[3];
      const impulseSensor = tableBody.rows[4];
      const smartmeter = tableBody.rows[5];
      const mBus = tableBody.rows[6];
      const i2CMeter = tableBody.rows[7];
      const dekoElement = tableBody.rows[8];
      const dhtType = tableBody.rows[9];

      myJSON[operationModes.id] = getOpModeSelectionValue(operationModes);
      myJSON[delaySendingData.id] = inputElement(delaySendingData);
      myJSON[digitalSensor.id] = sliderElement(digitalSensor);
      myJSON[distanceSensor.id] = sliderElement(distanceSensor);
      myJSON[impulseSensor.id] = ImpulseSensorValue(impulseSensor);
      myJSON[smartmeter.id] = InternTableOneInput(smartmeter);
      myJSON[mBus.id] = InternTableOneInput(mBus);
      myJSON[i2CMeter.id] = I2CMeterValue(i2CMeter);
      myJSON[dekoElement.id] = sliderElement(dekoElement);
      myJSON[dhtType.id] = getDHTSelectionValue(dhtType);
      myJSON["name"] =
        table.parentNode.querySelector("caption .text").textContent;

      console.log(myJSON);
      return myJSON;
    }

    function sendConfigWithConfirm(event) {
      let element = event.currentTarget.parentNode.parentNode;

      console.log(element);
      const table = element.querySelector("table");
      console.log(table);
      const nodeId = table.id;

      const selectedNode =
        table.querySelector(".caption-box .text").textContent;
      if (confirm(`Send the Configuration to Node: ${selectedNode}?`)) {
        if (websocket.readyState !== WebSocket.OPEN) {
          console.log("Websocket is not open. Add Class to handle this");
        }
        console.log("Try to send");
        websocket.send(
          JSON.stringify({
            type: websocketTypes.sendConfiguration,
            nodeId: utills.getIntOfIdElementIfItIsTheLastPart(nodeId),
            configuration: nodeConfigurationCollection(table),
          })
        );
        togglefadeOutClass(event);
        return;
      } else {
        event.stopPropagation();
      }
    }

    const sendConfiguraiton = document.querySelector(
      ".config-send-button button"
    );
    sendConfiguraiton.removeEventListener("click", togglefadeOutClass, false);
    sendConfiguraiton.addEventListener("click", sendConfigWithConfirm, false);
  }

  // Function to handle send button
  function handleSendButton() {
    const sendButton = document.querySelectorAll(".send-button");

    for (let i = 0; i < sendButton.length; i++) {
      if (sendButton[i].querySelector("i") === null) {
        continue;
      }
      sendButton[i].addEventListener("click", togglefadeOutClass, false);
    }
  }

  handleInterTableArrows();
  handleChangeName();
  handleAllSliders();
  handleAllSliderRows();
  handleAcceptNewNodesButton();

  //reihenfolge wichtig!!
  handleSendButton();

  handleResetNode();
  handleSendConfiguration();
});

document.addEventListener("DOMContentLoaded", function () {
  let polipop;

  try {
    polipop = new Polipop("mypolipop", {
      layout: "popups",
      insert: "before",
      pool: 5,
      progressbar: true,
      effect: "slide",
      theme: "compact",
      closer: false,
      position: "top-left",
    });
  }catch (error) {
    console.error(error);
  }

  // container for websocket initialization
  async function initWebsocket() {
    // Evaluate the received JSON message and perform corresponding actions
    async function evaluateJson(jsonMessage) {
      //then we have a successfull action
      if ("Type" in jsonMessage) {
        if (jsonMessage.Type == "ChangeName") {
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          if (nodeElement === null) {
            console.log("Nothing found by: ", nodeId);
            return;
          }
          // console.log(nodeElement);
          nodeElement.querySelector("span.text").textContent =
            jsonMessage.NewInfo;
          const table = document.getElementById("new-nodes-configuration");
          // console.log(table);
          const configTableElement = table.querySelector("#" + nodeId);
          // console.log(configTableElement);
          if (configTableElement) {
            configTableElement.querySelector("caption span.text").textContent =
              jsonMessage.NewInfo;
          }
        } else if (jsonMessage.Type == "NodeDeleted") {
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          console.log(nodeElement);
          nodeElement.remove();
        } else if (jsonMessage.Type == "SendConfig") {
          //Darstellen unten bei resez
          const resetTable = document.querySelector(
            "table.gateway-configuration-table tbody .right-cell .custom-dropdown"
          );
          const selectionResetNodes = resetTable.querySelector("select");
          addOneSingleNodeToResetDropdown(jsonMessage.id, jsonMessage.NewInfo, selectionResetNodes);
          //wieder hide der tabelle machen
          const nodeTableDiv = document.querySelector(
            ".new-nodes-configuration"
          );
          utills.addHideClass(nodeTableDiv);
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          console.log(nodeElement);
          utills.addHideClass(nodeElement); //remove wäre besser, aber unsicher, da id zweimal verwendet wird -> anderes ID-Design für new-nodes-Tabelle verwenden
        } else if (jsonMessage.Type == "checkNewNodes") {
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          console.log(nodeElement);
          nodeElement.remove();
          try {
            const noNodesRow = document.getElementById("no-new-nodes");
            utills.addHideClass(noNodesRow);
          } catch (error) {
            console.error(error);
          }
        } else if (jsonMessage.Type == "SendKey") {
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          console.log(nodeElement);
          nodeElement.remove();
        } else if (jsonMessage.Type == "LED") {
          //dann den wert abfragen -> nur navigation zum bild schauen
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          if (nodeElement === null) {
            console.log("Nothing found by: ", nodeId);
            return;
          }
          console.log(nodeElement);
          const nameCell = nodeElement.querySelector('td[data-cell="name"]');
          const onLed = nameCell.querySelector("img.icon-on");
          const offLed = nameCell.querySelector("img.icon-off");
          if (onLed.classList.contains("hide")) {
            onLed.classList.remove("hide");
            offLed.classList.add("hide");
          } else {
            if (offLed.classList.contains("hide")) {
              offLed.classList.remove("hide");
              onLed.classList.add("hide");
            }
          }
        } else if (jsonMessage.Type == "RESET") {
          let nodeId = `node_${jsonMessage.id}`;
          const nodeElement = document.getElementById(nodeId);
          console.log(nodeElement);
          nodeElement.remove();
        } else {
          console.log("Not relevant Message Type: " + jsonMessage.Type);
        }
        //then we had an error
      } else if ("Error" in jsonMessage) {
        polipop.add({
          content: `Fault occured: ${jsonMessage.ErrorType} at ${jsonMessage.Error}. Check Node ${jsonMessage.Name}`,
          title: "Warnung",
          type: "warning",
        });
      } else if ("event" in jsonMessage) {
        const newNodesTableBuilder = new BuildNewNodesTable();
        if (jsonMessage.event == "NodeWaitingForKey") {
          newNodesTableBuilder.cloneNewRow(jsonMessage.id, jsonMessage.name, 1);
        } else if (jsonMessage.event == "NodeWaitingForConfig") {
          newNodesTableBuilder.cloneNewRow(jsonMessage.id, jsonMessage.name, 2);
        } else {
          console.log("Not relevant Event Type: " + jsonMessage.event);
        }
      }
    }

    // Handle the WebSocket connection open event
    function onOpen(event) {
      console.log("Connection opened");
    }

    // Handle the WebSocket connection close event and attempt to reconnect
    function onClose(event) {
      console.log("Connection closed");
      setTimeout(initWebsocket, 2000);
    }

    // Handle the WebSocket message event and parse the received JSON
    function onMessage(event) {
      try {
        const messageParse = JSON.parse(event.data);
        console.log(messageParse);
        evaluateJson(messageParse);
      } catch (error) {
        console.error("Error reseiving an String, no Json. Error is:");
        console.error(error);
        console.log("Message String: ", event.data);
      }
    }

    websocket.onopen = onOpen;
    websocket.onclose = onClose;
    websocket.onmessage = onMessage;
  }

  // Build the table for displaying new nodes and populate it with keyless nodes and unconfigured nodes
  async function buildNewNodesTable() {
    try {
      const newNodesTableBuilder = new BuildNewNodesTable();

      const keylessNodes = await utills.getJson("/api/allKeylessNodes");
      keylessNodes.forEach((element) => {
        newNodesTableBuilder.cloneNewRow(element.id, element.name, 1);
      });

      const unconfigNodes = await utills.getJson("/api/allUnconfigNodes");
      unconfigNodes.forEach((element) => {
        newNodesTableBuilder.cloneNewRow(element.id, element.name, 2);
      });

      if (
        Object.keys(keylessNodes).length === 0 &&
        Object.keys(unconfigNodes).length === 0
      ) {
        newNodesTableBuilder.displayNoNewNodeRow();
      }
    } catch (error) {
      console.error(error);
    }
  }
  // Add a single node to the reset nodes dropdown selection
  async function addOneSingleNodeToResetDropdown(id, name, selectionResetNodes) {
    // eine neue Option erstellen
    const newOption = document.createElement("option");

    newOption.value = `node_${id}`;
    newOption.id = `node_${id}`;
    let newName = name;
    selectionResetNodes.add(newOption);

    const spanElement = document.createElement("span");
    spanElement.classList.add("text");
    //spanElement.textContent = newOption.value;
    if (newName === "" || newName === null || newName === undefined) {
      newName = "Node " + id;
    }
    spanElement.textContent = newName;

    // Das span-Element der Option hinzufügen
    newOption.appendChild(spanElement);
  }

  // Build the selection options for resetting nodes
  async function buildResetNodesSelection() {
    try {
      const resetTable = document.querySelector(
        "table.gateway-configuration-table tbody .right-cell .custom-dropdown"
      );
      const selectionResetNodes = resetTable.querySelector("select");

      const resetNodes = await utills.getJson("/api/allActiveNodesIdName");

      resetNodes.forEach((element) => {
        addOneSingleNodeToResetDropdown(element.id, element.name, selectionResetNodes);
        // eine neue Option erstellen
        // const newOption = document.createElement("option");

        // newOption.value = `node_${element.id}`;
        // newOption.id = `node_${element.id}`;
        // let name = element.name;
        // selectionResetNodes.add(newOption);

        // const spanElement = document.createElement("span");
        // spanElement.classList.add("text");
        // //spanElement.textContent = newOption.value;
        // if (name === "" || name === null || name === undefined) {
        //     name = "Node " + element.id;
        // }
        // spanElement.textContent = name

        // // Das span-Element der Option hinzufügen
        // newOption.appendChild(spanElement);
      });
    } catch (error) {
      console.error(error);
    }
  }

  buildNewNodesTable();
  buildResetNodesSelection();
  initWebsocket();
  utills.initDate();
});
