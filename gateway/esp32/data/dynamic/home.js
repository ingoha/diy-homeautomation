const gateway = `ws://${window.location.hostname}/ws`;
const websocket = new WebSocket(gateway);


window.addEventListener('load', function() {


});
  

document.addEventListener("DOMContentLoaded", function() { 

    const nodeBuilder = new BuildNodes();
    let polipop;

    try {
        polipop = new Polipop('mypolipop', {
            layout: 'popups',
            insert: 'before',
            pool: 5,
            progressbar: true,
            effect: 'slide',
            theme: 'compact', 
            closer: false,
            position: "top-left",
        });
        } catch (error) {
            console.error(error);
        }

    // Initializes the WebSocket connection
    async function initWebsocket() {
        //check which type is received by a wecksocket message
        async function evaluateJson(jsonMessage) {
            //then we have a successfull action
            if ("Type" in jsonMessage) {
                if (jsonMessage.Type == "ChangeName") {
                    let nodeId = `node_${jsonMessage.id}`;
                    const nodeElement = document.getElementById(nodeId);
                    console.log(nodeElement);
                    nodeElement.querySelector('span.text').textContent = jsonMessage.NewInfo;
                }else if (jsonMessage.Type == "NodeDeleted") {
                    let nodeId = `node_${jsonMessage.id}`;
                    const nodeElement = document.getElementById(nodeId);
                    console.log(nodeElement);
                    nodeElement.remove();
                }else if (jsonMessage.Type == "SendConfig") {
                    //holen mit request des neuen nodes
                    const nodeData = {
                        id: jsonMessage.id,
                        name: jsonMessage.NewInfo
                    };
                    console.log(nodeData);
                    nodeBuilder.buildAllDataNodes(nodeData);
                    // nodeBuilder.fillDataToNodes([jsonMessage.id]);
                }else if (jsonMessage.Type == "RESET") {
                    let nodeId = `node_${jsonMessage.id}`;
                    const nodeElement = document.getElementById(nodeId);
                    console.log(nodeElement);
                    nodeElement.remove();
                } else {
                    console.log("Not relevant Message Type: " + jsonMessage.Type);
                }
            }
            //then we had an error
            else if ("Error" in jsonMessage) {
                let name = jsonMessage.Name;
                if (name === "" || name === null || name === undefined) {
                    name = `Node ${jsonMessage.Name}`;
                }
                polipop.add({
                    content: `Fault occured: ${jsonMessage.ErrorType} at ${jsonMessage.Error}. Check Node: \"${jsonMessage.Name}\"` ,
                    title: 'Warnung',
                    type: 'warning'
                });
            }else if ("Event" in jsonMessage) {
                if (jsonMessage.event == "NewSensorValues") {
                    //zeurst schauen, ob es das id element schon gibt, und dann die Werte einfach reinschreiben -> oder halt neu aufgbauen -> mal schaune die Funtkionen dafür 
                    //hier nicht, wieder rauslöschen, nur für die andere Setiee, bei der anderne die anderen beiden srauschlöschen
                    const nodeData = {
                        id: jsonMessage.id,
                        name: jsonMessage.name
                    };
                    console.log(nodeData);
                    nodeBuilder.includeAllNodesByNameAndIdToPage(nodeData);
                    nodeBuilder.evaluateJsonElementToBuildAllCards(jsonMessage.newData);
                } else {
                    console.log("Not relevant Event Type: " + jsonMessage.event);
                }
            }
        }
        
        function onOpen(event) {
            console.log('Connection opened');
        }

        function onClose(event) {
            console.log('Connection closed');
            setTimeout(initWebsocket, 2000);
        }
        
        function onMessage(event) {
            try {
                const messageParse = JSON.parse(event.data);
                console.log(messageParse);
                evaluateJson(messageParse);
            } catch (error) {
                console.error("Error reseiving an String, no Json. Error is:");
                console.error(error);
                console.log("Message String: ", event.data);
            }
        }
        
        websocket.onopen    = onOpen;
        websocket.onclose   = onClose;
        websocket.onmessage = onMessage;
    }

    // Builds all the node cards with their data 
    async function  buildAllNodesWithData() {
        const nodesNameIdJson = await utills.getJson(nodeBuilder.getUrlAllActiveNodesNameId());
        nodeBuilder.buildAllDataNodes(nodesNameIdJson);
    }

    buildAllNodesWithData();
    initWebsocket();
    utills.initDate();
});