//  include neccessary libraries
#pragma once
#include "RFM69Radio.h"
#include <PubSubClient.h>
#include <WiFi.h>
#include <LEDBlink.h>
#include "NetworkConnection.h"
#include <U8g2lib.h>
#include "OLEDDisplayWrapper.h"
#include "enums/MQTTPublishMsgType.h"
#include "enums/SensorIdValues.h"

// handel the MQTT Communication
class MQTTGateway {
 private:
    WiFiClient _EspWifiClient;              // MQTT-Wifi Client have to be active during MQTT-Session
    PubSubClient *_MqttClient;              // Instance of MQTT library
    String _MqttBrokerIP;                   // IP-Adress of the MQTT Broker
    const String _VERSION = "2.1";          // Version of the Gateway-Software
    NetworkConnection* _NetworkRelevantObj;
    OLEDDisplayWrapper* _OledDisplay;
    String _CallbackTopic;                  // the Topic of a received MQTT Message
    String _CallbackMessage;                // the string of the received MQTT Message
    Payload _CallbackPayloadMessage;        // the RFM-Datatype-Structure for saving in the QUEUE
    bool _CallbackReadCmd;                  // value to check if the reveived MQTT message ist "READ"
    unsigned long _PreviousMilliseconds;

    float _FloatWithDecimal(float value, int decimal_places);                           // converts a float to a float with a specific decimal places number
    bool _MqttPublish(Payload rfm_message, MQTTPublishMsgType msg_type, int rfm_rssi);  // Publish a specific message of a received RFM message
    String _GetClientStateAsString();                                                   // Reconnect to the MQTT Server for multiple times
    void _MqttMessageEditing(String topic, byte* message, unsigned int length);         // fetch Infos of the received MQTT message and topic
    bool _AllowedTopicLenght();                                                         // check if the received MQTT-Topic consists of right place numbers
    int _CheckValueForRead();                                                           // set the value of the bool _CallbackReadCmd
    SensorIdValues _ChooseSensorId(short int sensor_id);                                // select specifier for an action, depends on the choosen sensor id in MQTT Topic
    bool _MQTTSendWhileEvaluatingSensorID(Payload *mqtt_callback_payload_message);      // creates the RFM-Datatype-Structure depending on the Sensorid
    bool _SendRequestedInfoAboutGatewayViaMQTT(short int dummy_sensor_id);              // check MQTT topic to get Infos about gateway; use sensor id as dummy
    bool CatchRFMMsgForLightAcknoledge(Payload rfm_message);

 public:
    MQTTGateway(const String mqtt_server_ip, NetworkConnection* obj, OLEDDisplayWrapper* _OledDisplay);     // construktor needs Broker ip
    ~MQTTGateway();
    Payload* Callback(char* topic, byte* message, unsigned int length, LEDBlink* ledManager);               // MQTT Callback
    void MqttClientInit(std::function<void(char*, byte*, unsigned int)> callback);                          // init the MQTT client; uses extern callback function
    bool MqttPublish(MQTTPublishMsgType single_msg_type, short int node_id = 1, short int error_code = 0);  // publish MQTT MEssage
    bool MqttPublishRfm(Payload rfm_msg, int rfm_rssi);                                                     // publish MQTT message of the received RFM message
    int PrintMQTTClientStatus(bool print_status = true);                                                    // print the status of the MQTT connection
    bool IsConnectedWithBroker();                                                                           // print the status of the MQTT connection
    bool ConnectToBroker();                                                                                 // print the status of the MQTT connection
    bool Publish(String mqtttopic, String mqttmsg);
};
