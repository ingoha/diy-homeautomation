#pragma once

#include <arduino.h>

// struct for wireless data transmission
typedef struct {
    int32_t   NodeId;                 // node ID (1xx, 2xx, 3xx);  1xx = basement, 2xx = main floor, 3xx = outside
    uint32_t  DeviceId;               // sensor ID
    uint64_t  UptimeMilliseconds;     // uptime in ms
    float     SensorData;             // sensor data
    float     BatteryVolts;           // battery condition
} Payload;

typedef struct {
    int32_t nodeID;
    uint32_t deviceID;
    uint32_t randomNumber;
} RequestIdPayload;
