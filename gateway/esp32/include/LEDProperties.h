#pragma once
#include "GatewayUtils.h"
#include "enums/LEDTaskList.h"

// use to safe on LED Instruciton
typedef struct {
  LEDTaskList TaskName;
  int PeriodDurationInMs;   // how Long one Blink-Period
  int PeriodNumebers;       // how often blinking
}LEDProperties;

class LEDPropertiesWrapper{
 public:
    static LEDProperties StructFactory(LEDTaskList taskName, int periodDurationInMs = 500, int periodNumebers = 5);
};