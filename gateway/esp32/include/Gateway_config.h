#pragma once


// #define mosbach
// #define phone
// #define phillipHome
// #define fredaHome



/*
 * RFM69 Radio
 */
#define ADRESS_NEW_NODES   2
#define NODEID          1       // 1 for Gateway
#define NETWORKID       22      // the same on all nodes that talk to each other


#define ENCRYPTKEY      "1234567890123456"  // exactly the same 16 characters/bytes on all nodes!
//  #define ACK_TIME        30  //  max # of ms to wait for an ack
#define REG_IRQFLAGS1   0x27    // Register using to check if RFM69 is connected

#ifdef mosbach
    #define my_ssid "MeinCtutW"
    #define my_password "leg0Handy0weg!"
    #define my_mqtt_server "192.168.178.78"
 #endif

#ifdef phone
     #define my_ssid "iPhone von Philipp"   // haschler fragen wieso nicht allgmeien als const geht? -> also es kommt ein fehler
     #define my_password "123frei4"
     #define my_mqtt_server "172.20.10.2"
#endif

#ifdef phillipHome
    #define my_ssid "Familie Freda"
    #define my_password "1.Kor16,13-14"
    #define my_mqtt_server "192.168.178.51"
#endif

#ifdef fredaHome
    #define my_ssid "WLAN-2D6DT9"
    #define my_password "51494666957768356672"
    #define my_mqtt_server "192.168.178.51"
#endif




/*
 * General
 */

// for Serial.println() debuggings
const bool Debug = true;
