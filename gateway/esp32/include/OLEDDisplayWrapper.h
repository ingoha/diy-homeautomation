#pragma once
#include <Arduino.h>
#include "OLEDTextRow.h"


class OLEDDisplayWrapper{
 private:
    static void TaskFunction(void *pvParameters);
    QueueHandle_t _Queue;
    TaskHandle_t _Task2;

 public:
    void AddTextToQueueAndWriteToOutputs(String text, TextCategorie categorie);
    OLEDDisplayWrapper();
    ~OLEDDisplayWrapper();
};
