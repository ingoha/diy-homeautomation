#pragma once
#include <Arduino.h>
#include <CircularBuffer.h>
#include "Gateway_config.h"
#include "Gateway_config_board.h"
#include "GatewayUtils.h"
#include "MQTTGateway.h"
#include "NodeHandler/NodeHandler.h"
#include "LEDStates.h"
#include "LEDBlink.h"
#include "LEDProperties.h"
#include "OLEDTextRow.h"
#include "OLEDDisplayWrapper.h"
#include "NetworkConnection.h"
#include "RFM69Radio.h"
#include <U8g2lib.h>


typedef enum {
    eRunMode,
    eAcceptNewSensorsMode,
} eGatewayModes;

class RFM69ToMQTTGateway{
 private:
    bool _ToBrokerConnected = false;
    bool _WLANConnected = false;
    eGatewayModes _mode = eRunMode;
    uint64_t millisNewNodesModeStarted = 0;
    const uint64_t maxTimeInNewNodesMode = 200;

    CircularBuffer<Payload, 100>* _RFMMsgQueue;
    LEDBlink* _LEDManager;
    // LEDStates* _LEDTaskArray;
    RFM69Radio* _Radio;
    MQTTGateway* _MQTTClient;
    NetworkConnection* _NetworkConnectionHandling;
    NodeHandler* _NodeHandler;

    OLEDDisplayWrapper* _OLEDDisplayHandler;

    uint64_t previousMillisForWLANConnection = 0;
    uint64_t previousMillisForBrokerConnection = 0;


    // U8G2_SSD1306_128X64_NONAME_1_HW_I2C* _OledDisplay;
    // u8g2_uint_t _DisplayWidth;
    // OLEDTextRow* _Text1;
    // OLEDTextRow* _Text2;
    // OLEDTextRow* _Text3;

    // QueueHandle_t myQueue;

    // static void TaskFunction(void *pvParameters);

    std::function<void(char*, byte*, unsigned int)> _CallbackWrapper;

    // TaskHandle_t Task2;

    LEDBlink* LEDBlinkFactory();




    // LEDStates* _DefineAllLEDs(int& size);
    // void Callback(char* topic, byte* message, unsigned int length);

    // LEDStates* BuildLEDTasks(short unsigned int size);

 public:
    void Run();
    explicit RFM69ToMQTTGateway(NodeHandler* nodeHandler);
    // void Run_test();
    // RFM69ToMQTTGateway(int i);
    ~RFM69ToMQTTGateway();
    bool WaitAndDoNotPass();
    void HandleNetworkConnection();
    void HandleBrokerConnection();
    void HandleReceivedRFMMessage();
    void HandlePreparedMessageFromMQTTCallback();
};
