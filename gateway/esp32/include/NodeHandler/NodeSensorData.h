#pragma once

typedef struct {
    float lastImpulseOrDigitalValue = 0.0;
    float lastD0MeterOrMBusValue = 0.0;
    float lastSensorDistanceValue = 0.0;
    float lastTemperatureValue = 0.0;
    float lastHumidityValue = 0.0;
    float lastI2CValue = 0.0;
    float battery = 0.0;
    int rssi = 0;
    bool dekoElement = false;
} NodeSensorData;


typedef struct {
    String mBusOrD0Unit = "";
    String impSensUnit = "";
    String I2CUnit = "";
} SensorMeasurementUnits;
