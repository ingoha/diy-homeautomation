#pragma once

#include <Array.h>
#include <CircularBuffer.h>


#include "Node.h"
#include "NodeConfig.h"
#include "NodeEvent.h"
#include "RFM69Radio.h"

#define MAX_NUMBER_OF_NODES         ((uint32_t) (500))
#define MAX_NUMBER_OF_EVENTS        ((uint32_t) (50))
#define DEVICE_ID_NODEMANAGMENT     ((uint8_t) 0)
#define SENSOR_VAL_RESET_NODE       ((float) 1.0)
#define SENSOR_VAL_DONT_RESET_NODE  ((float) 0.0)
#define NODE_LED_ON                 ((uint8_t) 1)
#define NODE_LED_OFF                ((uint8_t) 0)

#define NODEHANDLER_DEBUG           // Uncomment if you want to use the debug functions

enum NodeHandlerErrorType {
    eNoError,
    eNoAvailableNodeId,
    eNoAckFromNode,
    eNodeIdNotFound,
    eResetNodeAlreadyRequested,
    eNodeNotInSetupMode,
    eMBusAndD0Selected,
    eImpAndDigSensSelected,
    eInternalError
};

class NodeHandler {
 private:
    int numOfNodes = 0;
    bool newNodesAllowed = false;
    const String nodeDirectory = "/nodes/";
    const String filenamePrefix = "node";
    Array <Node, MAX_NUMBER_OF_NODES> activeNodes;
    Array <int, MAX_NUMBER_OF_NODES> nodesToReset;
    CircularBuffer <NodeEvent, MAX_NUMBER_OF_EVENTS> nodeEvents;
    RFM69Radio* radio;
    bool writeNodeToFile(Node* node);
    bool readNodesFromFile();
    int findFreeNodeId();
    bool handleBackToSetupmodeRequest(int idOfRequestingNode);
    NodeHandlerErrorType validateNodeExistsAndSetupMode(int targetNodeId, Node** nodeToAccess);

 public:
    NodeHandler();
    ~NodeHandler();
    NodeHandlerErrorType handleRfmMessage(const Payload* msg);
    NodeHandlerErrorType handleRfmMessage(const RequestIdPayload* msg);
    NodeHandlerErrorType sendConfigToNode(int targetNodeId, DetailedNodeConfig nodeConfig, SensorMeasurementUnits units, String nodeName = "noName");
    NodeHandlerErrorType sendEncryptKeyToNode(int targetNodeId);
    NodeHandlerErrorType controlNodeLed(int targetNodeId, uint8_t targetLedState);
    NodeHandlerErrorType changeNodeName(int targetNodeId, String newName);
    NodeHandlerErrorType resetNode(int targetNodeId);
    NodeHandlerErrorType deleteNode(int targetNodeId);
    NodeHandlerErrorType pollSlaveNodes();

    Node* getNodeById(int targetNodeId);
    int getNodesWithoutEncryptKey(int* nodesBuffer, int bufferSize);
    int getNodesWaitingForConfig(int* nodesBuffer, int bufferSize);
    int getWorkingNodes(int* nodesBuffer, int bufferSize);
    bool getNodeEvent(NodeEvent* event);

    int getNumOfNodes()                                     {return numOfNodes;}                // inline getter method
    void setNewNodesAllowed(bool newNodesFlag)              {newNodesAllowed = newNodesFlag;}   // inline setter method
    bool getNewNodesAllowed()                               {return newNodesAllowed;}           // inline getter method
    void setRadio(RFM69Radio* aRadio)                       {radio = aRadio;}                   // inline setter method

    // Function below only exists for debug purposses!
    #ifdef NODEHANDLER_DEBUG
    void addNode(int id, bool key, uint8_t mode);
    void addData(Node *node, int id);
    void printAllNodes();
    #endif
};
