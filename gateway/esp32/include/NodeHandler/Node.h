#pragma once

#include <ArduinoJson.h>
#include <time.h>
#include "RFM69messageTypes.h"
#include "NodeConfig.h"
#include "NodeSensorData.h"

class Node {
 private:
    int id = 2;
    String name;
    time_t lastSeen = 0;
    bool hasEncryptKey = false;
    DetailedNodeConfig config;
    NodeSensorData sensorValues;
    SensorMeasurementUnits measurementUnits;

 public:
    Node();
    Node(const Node& node);
    explicit Node(int id);
    explicit Node(String objectAsJson);

    int getId()                                     {return id;}                        // inline getter method
    void setName(String newName)                    {name = newName;}                   // inline setter method
    String getName()                                {return name;}                      // inline getter method
    time_t getLastSeen()                            {return lastSeen;}                  // inline getter method
    bool getHasEncryptKey()                         {return hasEncryptKey;}             // inline getter method
    void setHasEncryptKey(bool value)               {hasEncryptKey = value;}            // inline setter method
    const DetailedNodeConfig* getConfig()           {return &config;}                   // inline getter method
    void setConfig(DetailedNodeConfig newConfig)    {config = newConfig;}               // inline setter method
    void setOperationMode(uint8_t newMode)          {config.operationMode = newMode;}   // inline setter method
    void setUnits(SensorMeasurementUnits newUnits)  {measurementUnits = newUnits;}      // inline setter method
    SensorMeasurementUnits getUnits()               {return measurementUnits;}          // inline getter method
    void messageReceived(const Payload* msg, int rssi);
    String jsonifyForFile();
    String jsonifyNameAndId();
    String jsonifyForWebserver();
    void print();
};
