#pragma once

enum EventTypes {
    eNodeWaitingForKey,
    eNodeWaitingForConfig,
    eNewSensorValues,
};
typedef struct {
    int nodeId;
    EventTypes type;
}NodeEvent;

