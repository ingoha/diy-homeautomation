#pragma once

#include <Arduino.h>

#define DHT11 ((uint8_t) (11))  // DHT TYPE 11
#define DHT12 ((uint8_t) (12))  // DHT TYPE 12
#define DHT21 ((uint8_t) (21))  // DHT TYPE 21
#define DHT22 ((uint8_t) (22))  // DHT Type 22


enum operationModes{
    eSetupMode = 0,
    eIntervallMode = 1,
    eSlaveMode = 2
};

enum sensorIDs{
    eSensorDigitalID = 4,
    eSensorDistanceID = 5,
    eSensorTempHumID = 6,
    eSensorHumidityID = 7,
    eSensorImpulsID = 8,
    eSensorNotUsed = 0
};

enum actorIDs{
    eActorDekoID = 30,
    eActorNotUsed = 0
};

enum interfaceIDs{
    eInterfaceD0Smartmeter = 9,
    eInterfaceI2C = 20,
    eInterfaceMBusID = 20,
    eInterfaceNotUsed = 0
};

// Note DO NOT ALTER the sequence of attributes without checking that the node receives a valid config! Alignment is an issue here!
typedef struct {
    uint8_t operationMode          = eSetupMode;                  // Mode of the node
    uint8_t sensorDigital          = eSensorNotUsed;              // Device ID of the digital sensor, by default not built in
    uint8_t sensorDistance         = eSensorNotUsed;              // Device ID of the distance sensor, by default not built in
    uint8_t sensorImpuls           = eSensorNotUsed;              // Device ID of the impuls sensor, by default not built in
    uint8_t d0Smartmeter           = eInterfaceNotUsed;           // Device ID of the D0 smartmeter, by default not connected
    uint8_t mBus                   = eInterfaceNotUsed;           // Device ID of the MBus, by default not connected
    uint8_t i2cMeter               = eInterfaceNotUsed;           // Device ID of the I2C interface, by default not connected
    uint8_t i2cMeterDevices        = 0;                           // Number of devices on the I2C bus
    uint8_t dekoElement            = eActorNotUsed;               // Device ID of the LED-Element, by default not built in
    uint8_t dhtType                = DHT11;                       // Type of DHT-Sensor which ist used
    int     analogDelaySeconds     = 600;                         // Number of seconds between sending of sensordata
    int     debounceTime           = 0;                           // Debounce time so impulses are only counted once
    float   startMeterReading      = 0;                           // Startvalue of the impuls sensor
    float   impulsesPerValue       = 0;                           // Impulses necesarry for one unit to be counted fully (i.e. m^3, kWh)
} DetailedNodeConfig;
