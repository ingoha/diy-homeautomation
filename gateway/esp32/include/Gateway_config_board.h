#include <U8g2lib.h>

#pragma once



/*
 * ESP32 hardwareconfig
 */
// #define ESP_HSPI      // uncomment to choose HSPI instead of VSPI (= Standard)
#ifdef ESP_HSPI
  #define ESP_SPI_CLK   14
  #define ESP_SPI_MISO  12
  #define ESP_SPI_MOSI  13
  #define ESP_SPI_SS    15
#else
  #define ESP_SPI_CLK   18
  #define ESP_SPI_MISO  19
  #define ESP_SPI_MOSI  23
  #define ESP_SPI_SS    5
#endif

#define SERIAL_BAUD     115200  // ESP32 transmission rate

#define ESP32_INTERRUPT 2       // connect with D0 of RFM69 Board

// choose the Pins for the LED colours
#define LED_GREEN       33
#define LED_RED         32
#define LED_YELLOW      25
#define LED_BLUE        26

/*
 * RFM69 hardwareconfig
 */
#define IS_RFM69HW      true   // uncomment only for RFM69HW! Leave out if you have RFM69W!
// Match frequency to the hardware version of the radio (uncomment one):
// #define FREQUENCY     RF69_433MHZ
#define FREQUENCY       RF69_868MHZ
// #define FREQUENCY     RF69_915MHZ


#define DEKO_ELEMENT 30



#define PUSHBUTTON_ESP32 35
#define PUSHBUTTON_NODEMODE 34

/*
 * OLED Display
 */
// #ifdef U8X8_HAVE_HW_SPI
// #include <SPI.h>
// #endif
// #ifdef U8X8_HAVE_HW_I2C
// #include <Wire.h>
// #endif



