#pragma once
#include <CircularBuffer.h>
#include "LEDProperties.h"
#include "LEDStates.h"
#include "GatewayUtils.h"

class LEDBlink {
 private:
    LEDProperties _ActualLEDTask;               // the actual LED Task which is running (Which LEDs blink)
    CircularBuffer<LEDProperties, 50> _Queue;   // safe the Tasks for LED
    LEDStates* _LedTaskArray;

    int _SizeOfLedTaskArray;

    unsigned long _previousMillis;  //  to check the passed time (the last time the LED was activated)
    bool _TaskIsActive;             //  show if a LED Task is active
    bool _LedsActive = false;

    void _LEDHandler();
    LEDStates _ParseLEDTaskListEnum(LEDTaskList task);


 public:
    LEDBlink(LEDStates* ledTasks, int countOfLedTasks);     // Construktor sets defalut values
    //  LEDBlink();  // Construktor sets defalut values
    ~LEDBlink();
    LEDTaskList WhichLEDTaskIsActive();                     // Construktor sets defalut values
    void Loop();
    int QueueSize();
    void AddTask(LEDProperties led);
    int LEDStatus(short int LEDNumber);                    // gives status info about the given  A LED
};