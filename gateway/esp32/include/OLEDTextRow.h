#pragma once
#include <Arduino.h>
//  #include <clib/u8g2_fonts.c>
//  #include <clib/u8g2.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <CircularBuffer.h>
#include "enums/TextCategorie.h"

struct OLEDText{
    String Text;
    TextCategorie Categorie;
};

class OLEDTextRow {
 private:
    U8G2_SSD1306_128X64_NONAME_1_HW_I2C* U8g2Display;
    String _ActualText;
    String _LastText;
    CircularBuffer<String, 50>* _Queue;     // safe the Tasks for LED

    String _FirstText;
    bool _Iteration = true;
    bool _TwoTimesRun = false;
    uint64_t previousMillis;


    unsigned int _TextWidth;

    int _XPosition = 0;
    unsigned int _YPosition = 0;

    int _Offset;
    int _NextTextOffset;

    const unsigned int _DisplayPixelLenght = 128;
    const uint8_t * _UsedFont;

    void _CheckWriteNextText();
    void _CheckForNextText();

 public:
    void SetNewText(String text, const uint8_t * font = u8g2_font_inb16_mr);
    void HandleTextLine(/*unsigned int x = 0, unsigned int y = 0*/);
    void SetXPosition(unsigned int value);
    void SetYPosition(unsigned int value);
    void SetFont(const uint8_t* value = u8g2_font_inb16_mr);    // do not set cause cons values depend on this
    void SetOffset(unsigned int value);
    void IterateNextTextOffset();
    void CheckForOffsetReset();
    void ScrollText();
    void HandleAllTextChecks();
    OLEDTextRow(U8G2_SSD1306_128X64_NONAME_1_HW_I2C* u8g2Display, String firstText, unsigned int yRowPos = 0);
    ~OLEDTextRow();
    void AddText(String text);
};
