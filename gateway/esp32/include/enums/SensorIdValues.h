#pragma once

// this are the states which are Imported from the Python-Gateway-Code.
// Leave Names like this till their actions are clarified
enum SensorIdValues {
    statMess,
    realMess,
    intMess,
    strMess,
    defaultcase
};
