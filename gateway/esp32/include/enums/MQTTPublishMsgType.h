#pragma once

// The MQTT-Message-Type depends on the message which have to publish
enum MQTTPublishMsgType {
    LastTimeReboot,         // publish the last time the ESP-Gateway was rebooted
    GetSoftwareVersion,     // Publish the Software Version of the gateway
    NodeUnableToReach,      // send if node not reachable
    CallbackSyntaxError,    // Publish ErrorCode for a syntax error in the received MQTT Message
    // The Rest of Types handle a reveiced RFM Message
    SensorData,             // publish sensor data of a node
    NodeUptime,             // publish uptime of a node
    NodeBatteryVoltage,     // publish battery-value of a node
    RssiValue               // publish RSSI of a node
};
