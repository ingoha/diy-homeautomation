#pragma once

enum LEDTaskList {
    NoneSelected,
    RfmMessageReceivedState,
    RfmMessageSend,
    RfmMessageNoAckState,
    MqttMessageReceivedState,
    MqttMessagePublishState,
    MqttProblemState,
    InitDoneState,
    WlanConnectedState,
    WLANNotConnectedState,
    MQTTConnectedToBroker,
};
