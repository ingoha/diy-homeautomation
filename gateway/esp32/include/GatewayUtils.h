#pragma once
#include <time.h>
#include "RFM69Radio.h"
#include <U8g2lib.h>
#include <WiFi.h>

class GatewayUtils {
 private:
    GatewayUtils();
    ~GatewayUtils();
    static GatewayUtils* _Instance;

 public:
    static GatewayUtils* getInstance();
    void operator=(const GatewayUtils &) = delete;  // delete of the assignment operator
    GatewayUtils(GatewayUtils &other) = delete;     // delete of the copy operator
    bool initFileSystem();
    const String ToString(wl_status_t status);
    void WriteToOutput(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String s1, const String s2);
    void WriteToOutput(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String s1);
    void WriteToOutput(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String s1, const int number);
    bool CheckPassedTime(const int waitingtime, uint64_t *previousMillis, bool* firstIteration);
    bool CheckPassedTime(const int waitingtime, uint64_t *previousMillis);
    void WriteOledOnDisplay(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String text);
    void DebugPrintf(const String s);
    void DebugPrintf(const uint64_t s);
    void DebugPrintf(const String s1, const String s2);
    void DebugPrintf(const String s1, const String s2, const String s3);
    void DebugPrintf(const String s1, const String s2, const  String s3, const  String s4);
    void DebugPrintf(const String s, const double x);
    void DebugPrintf(const String s, const int x);
    void DebugPrintf(const String s, const unsigned int x);
    void DebugPrintf(const String s1, const int x, const String s2);
    void DebugPrintf(const String s, const uint64_t x);

    void DebugPrintf(const String s1, const Payload RFMmsg);
    void DebugPrintf(struct tm* timeinfo, const Payload RFMmsg);
    void DebugPrintf(const Payload RFMmsg);
    void DebugPrintf(const Payload *RFMmsg);
    void DebugPrintf(const RequestIdPayload msg);
};
