#pragma once
#include "GatewayUtils.h"
#include "enums/LEDTaskList.h"



class LEDStates {
 private:
    unsigned short int _SizeOfLEDsArray;
    LEDTaskList _TaskName;

    void _ChangeStateOfLED(unsigned short int state);

    unsigned short int _LEDsActive = 0;
    unsigned short int* _LEDs;

 public: 
    LEDStates(int* ledNumbers , unsigned short int sizeOf , LEDTaskList taskName);  // Construktor sets defalut values
    ~LEDStates();
    LEDStates();
    const unsigned short int* GetLEDs();
    const unsigned short int GetLEDArraySize();
    void TurnOnLEDs();
    void TurnOffLEDs();
    void ToogleLEDs(bool ledsActive);
    LEDTaskList GetTaskName();
};