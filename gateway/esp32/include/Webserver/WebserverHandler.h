#ifndef WEBSERVER_WEBSERVERHANDLER_H_
#define WEBSERVER_WEBSERVERHANDLER_H_

#include "NetworkConnection.h"
#include <ESPAsyncWebServer.h>
#include "NodeHandler/NodeHandler.h"
#include "ArduinoJson.h"

enum websockeTypes {
    deleteNode = 1,
    changeName = 2,
    acceptNewNodes = 3,
    sendKey = 4,
    led = 5,
    sendConfiguration = 6,
    resetNode = 7,
};


class WebserverHandler {
 private:
    AsyncWebServer *server;
    AsyncWebSocket *websocket;
    NetworkConnection* networkConnectionHandling;
    NodeHandler* nodeHandler;

    bool wlanConnected = false;
    void callbackWebsocket(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len);
    uint64_t previousMillisForWLANConnection;
    void initWebserverRouting();
    void initWebSocket();
    bool handleFrontendCommands(String msg, AsyncWebSocketClient *client);
    int returnIntInJsonString(JsonVariant var);
    void handleDeleteNode(int id, AsyncWebSocketClient *client);
    void handleChangeName(int id, String name, AsyncWebSocketClient *client);
    void handleAcceptNewNodes(AsyncWebSocketClient *client);
    void handleSendKey(int id, AsyncWebSocketClient *client);
    void handleLed(int id, bool ledState, AsyncWebSocketClient *client);
    void handleSendConfiguration(int id, JsonVariant configJson, AsyncWebSocketClient *client);
    void handleResetNode(int id, AsyncWebSocketClient *client);
    void handleError(NodeHandlerErrorType result, int id, String messageType, String successMessage, AsyncWebSocketClient* client);
    void handleEvents(NodeEvent event);
    template<typename T>
    bool isThisType(const JsonVariant configJson);

 public:
    explicit WebserverHandler(NodeHandler* nodeHandler);
    ~WebserverHandler();
    void handleNetworkConnection();
    void run();
};

#endif 
