#pragma once
#include <U8g2lib.h>
#include "OLEDTextRow.h"

class OLEDDisplay{
 private:
    U8G2_SSD1306_128X64_NONAME_1_HW_I2C* _OledDisplay;
    OLEDTextRow* _RowRFM;
    OLEDTextRow* _RowMQTT;
    OLEDTextRow* _RowInfo;
    QueueHandle_t _Queue;

 public:
    OLEDDisplay();
    ~OLEDDisplay();
    void AddToSpecificRowQueue(OLEDText textObj);
    void WriteToDisplay();
};
