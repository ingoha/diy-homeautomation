#pragma once
#include <RFM69.h>
#include "RFM69messageTypes.h"

// handle the RFM Communication
class RFM69Radio {
 private:
    RFM69* radio;                                   // instance of the used RFM library

 public:
    RFM69Radio(int8_t ESP32_IRQ, int8_t SPI_CLK, int8_t SPI_MISO, int8_t SPI_MOSI, int8_t SPI_SS, bool IS_RFM69HCW);
    ~RFM69Radio();
    int GetMessage(byte* msgBuffer);                                            // fetch Byte Message from RFM-Transceiver
    void CheckRfmConnected();                                                   // gives advice about possibilty that RFM-Transceiver lost connection
    bool SendMessage(const void* msgBuffer, int msgSize, int nodeId);           // Send a Message with RFM-Transceiver
    bool SendMessageWithBurst(const void* msgBuffer, int msgSize, int nodeID);  // Send a Message with RFM-Transceiver with a Burst
    void RfmInit();                                                             // setup the RFM-Transceiver
    bool MessageReceived();                                                     // check if a Message is received
    int Rssi();                                                                 // return the RSSI
    void encryptDefault();                                                      // encrypt the radio with default key
    void encryptNone();                                                         // encrypt the radio with no key
};
