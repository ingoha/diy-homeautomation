#pragma once
#include <Arduino.h>
#include <U8g2lib.h>
#include "OLEDDisplayWrapper.h"
#include <WiFi.h>


// handel the Network (WLAN) Connecton
class NetworkConnection {
 private:
    struct tm* _TimeInfo;                           // Instance of the a Time Struct (https:// cplusplus.com/reference/ctime/tm/,
                                                    // https:// randomnerdtutorials.com/esp32-date-time-ntp-client-server-arduino/)
    String _TimeStamp = "";                         // TimeStamp for the last time ESP32 startet
    const long _GMTSECONDSOFFSET = 3600;            // offset time in seconds for the German GMT: +1 hour
    const int _DAYLIGHTSECONDSOFFSET = 3600;        // daylight time in seconds (2022: daylight (=Zeitumstellung) is used in Germany)
    const String _TIMENTPSERVER = "pool.ntp.org";   // server wherer time to request

 public:
    const String wlanStatusToString(wl_status_t status);
    void ObtainTime();  // Bevor this, we need a network Connection
    NetworkConnection();
    ~NetworkConnection();
    String GetTimeStamp();
    struct tm* GetTimeInfo();
    bool ConnectedWithLocalNetwork();
    bool RestartProcessForLocalNetworkConnection(const String ssid, const String password, int reconnectTrys = 10, int delayTimeForNextReconnectInMs = 500);
};
