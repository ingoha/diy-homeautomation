#include "Webserver/WebserverHandler.h"
#include "Nodehandler/Node.h"
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebserver.h>
#include "FS.h"
#include <LittleFS.h>
#include "GatewayUtils.h"
#include "Gateway_config.h"
#include "Gateway_config_board.h"
#include "AsyncJson.h"
#include "ArduinoJson.h"

// Method for testing the web server
// nodeHandler: The NodeHandler used for testing
void testWebserver(NodeHandler* nodeHandler) {
    int workingNodesOffset = 5;
    int noKeyNodesOffset = 20;
    int noConfigNodesOffset = 35;

    for (size_t i = 0; i < 5; i++) {
        nodeHandler->addNode(i+workingNodesOffset, true, eIntervallMode);
    }

    for (size_t i = 0; i < 2; i++) {
        nodeHandler->addNode(i+noKeyNodesOffset, false, eSetupMode);
    }

    for (size_t i = 0; i < 1; i++) {
        nodeHandler->addNode(i+noConfigNodesOffset, true, eSetupMode);
    }
}

// Method for handling errors in the web server
// result: The error code being handled
// id: The ID of the node related to the error
// messageType: The message type of the error
// successMessage: The success message
// client: The AsyncWebSocketClient to which the response is sent
void WebserverHandler::handleError(NodeHandlerErrorType result, int id, String messageType, String successMessage, AsyncWebSocketClient* client) {
    // GatewayUtils::getInstance()->DebugPrintf("Result (0 is good): \t", result);
    // GatewayUtils::getInstance()->DebugPrintf("ID: \t", id);
    // GatewayUtils::getInstance()->DebugPrintf("messageType: \t", messageType);
    // GatewayUtils::getInstance()->DebugPrintf("successMessage: \t", successMessage);
    StaticJsonDocument <200> doc; 
    String jsonNodeString;
    switch(result) {
        case eNoError:
            doc["Type"] = messageType;
            doc["NewInfo"] = successMessage;
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            websocket->textAll(jsonNodeString);
            doc.clear();
            return;
        case eNoAvailableNodeId:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eNoAvailableNodeId";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eNoAckFromNode:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eNoAckFromNode";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eNodeIdNotFound:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eNodeIdNotFound";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eResetNodeAlreadyRequested:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eResetNodeAlreadyRequested";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eNodeNotInSetupMode:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eNodeNotInSetupMode";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eMBusAndD0Selected:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eMBusAndD0Selected";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eImpAndDigSensSelected:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eImpAndDigSensSelected";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        case eInternalError:
            doc["Error"] = messageType;
            doc["ErrorType"] = "eInternalError";
            doc["Name"] = nodeHandler->getNodeById(id)->getName();
            doc["id"] = id;
            ArduinoJson::serializeJson(doc, jsonNodeString);
            client->text(jsonNodeString);
            doc.clear();
            return;
        default:
            // Handle default case
            break;
    }
}

// Constructor of the WebserverHandler class
// nodeHandler: The NodeHandler used for managing the nodes
WebserverHandler::WebserverHandler(NodeHandler* nodeHandler) {
    this->nodeHandler = nodeHandler;
    networkConnectionHandling = new NetworkConnection();
    handleNetworkConnection();
    server = new AsyncWebServer(80);
    websocket = new AsyncWebSocket("/ws");

    initWebSocket();

    initWebserverRouting();

    // UNCOMMENT only if you do not have access to a real node
    // testWebserver(nodeHandler);
}

// Method for returning an integer from a JSON string
// var: The JsonVariant from which the integer is extracted
// return: The extracted integer
int WebserverHandler::returnIntInJsonString(JsonVariant var) {
    int ret = 0;
    if (var.is<uint8_t>()) {
        ret = var.as<uint8_t>();
    } else if (var.is<String>()) {
        String stringValue = var.as<String>();
        ret = atoi(stringValue.c_str());
    }
    return ret;
}

// Method for handling the deletion of a node
// id: The ID of the node to be deleted
// client: The AsyncWebSocketClient to which the response is sent
void WebserverHandler::handleDeleteNode(int id, AsyncWebSocketClient *client) {
    NodeHandlerErrorType result = nodeHandler->deleteNode(id);
    handleError(result, id, "NodeDeleted", "", client);
}


// Method for changing the name of a node
// id: The ID of the node whose name is being changed
// name: The new name of the node
// client: The AsyncWebSocketClient to which the response is sent
void WebserverHandler::handleChangeName(int id, String name, AsyncWebSocketClient *client) {
    NodeHandlerErrorType result = nodeHandler->changeNodeName(id, name);
    handleError(result, id, "ChangeName", name,client);
}

// Method for accepting new nodes
// client: The AsyncWebSocketClient to which the response is sent
void WebserverHandler::handleAcceptNewNodes(AsyncWebSocketClient *client) {
    nodeHandler->setNewNodesAllowed(true);

    StaticJsonDocument <40> doc; 
    String jsonNodeString;

    doc["type"] = "checkNewNodes";

    ArduinoJson::serializeJson(doc, jsonNodeString);
    websocket->textAll(jsonNodeString);
    doc.clear();
}

// Method for sending an encryption key to a node
// id: The ID of the node to which the key is sent
// client: The AsyncWebSocketClient to which the response is sent
void WebserverHandler::handleSendKey(int id, AsyncWebSocketClient *client) {
    NodeHandlerErrorType result = nodeHandler->sendEncryptKeyToNode(id);
    handleError(result, id, "SendKey", "" ,client);
}


// Method for controlling the LED of a node
// id: The ID of the node whose LED is being controlled
// ledState: The state of the LED (on or off)
// client: The AsyncWebSocketClient to which the response is sent
void WebserverHandler::handleLed(int id, bool ledState, AsyncWebSocketClient *client) {
    if(ledState) {
        NodeHandlerErrorType result = nodeHandler->controlNodeLed(id, NODE_LED_ON);
        handleError(result, id, "LED", "ON" ,client);
    }else {
        NodeHandlerErrorType result = nodeHandler->controlNodeLed(id, NODE_LED_OFF);
        handleError(result, id, "LED", "OFF" ,client);
    }
}

// Checks if the given JsonVariant is a specific type
// configJson: The JsonVariant to check
// return: True if the type matches the template type, False otherwise
template<typename T>
bool WebserverHandler::isThisType(const JsonVariant configJson) {
    if (configJson.is<T>()) {
        GatewayUtils::getInstance()->DebugPrintf("See good Json: \t", configJson.as<String>());
        return true;
    }
    GatewayUtils::getInstance()->DebugPrintf("Problem Json: \t", configJson.as<String>());
    return false;
}

// Handles the process of sending a configuration to a node
// id: The ID of the node to send the configuration to
// configJson: The JsonVariant containing the configuration
// client: The AsyncWebSocketClient
void WebserverHandler::handleSendConfiguration(int id, JsonVariant configJson, AsyncWebSocketClient *client) {

    if (!(isThisType<uint8_t>(configJson["operation_mode"]) ||
          isThisType<uint8_t>(configJson["digital_sensor"]) ||
          isThisType<uint8_t>(configJson["distance_sensor"]) ||

          isThisType<uint8_t>(configJson["impule_sensor"]["active"]) ||
          isThisType<float>(configJson["impule_sensor"]["impulsesPerValue"]) ||
          isThisType<int>(configJson["impule_sensor"]["debounceTime"]) ||
          isThisType<float>(configJson["impule_sensor"]["startMeterReading"]) ||
          isThisType<String>(configJson["impule_sensor"]["unit"]) ||

          isThisType<uint8_t>(configJson["smartmeter"]["active"]) ||
          isThisType<uint8_t>(configJson["M_bus"]["active"]) ||

          isThisType<String>(configJson["smartmeter"]["unit"]) ||
          isThisType<String>(configJson["M_bus"]["unit"]) ||

          isThisType<uint8_t>(configJson["I2C_meter"]["active"]) ||
          isThisType<uint8_t>(configJson["I2C_meter"]["I2CDeviceCount"]) ||
          isThisType<String>(configJson["I2C_meter"]["unit"]) ||

          isThisType<uint8_t>(configJson["deko_element"]) ||
          isThisType<uint8_t>(configJson["dht_type"]) ||
          isThisType<uint8_t>(configJson["delay_sending_data"]) ||
          isThisType<String>(configJson["name"]))) {
        //websocket alerm, dass was nicht stimm.
        return;
    }

    DetailedNodeConfig nodeConfig;
    SensorMeasurementUnits units;

    nodeConfig.operationMode = configJson["operation_mode"].as<uint8_t>();
    nodeConfig.sensorDigital = configJson["digital_sensor"].as<uint8_t>();
    nodeConfig.sensorDistance = configJson["distance_sensor"].as<uint8_t>();
    
    nodeConfig.sensorImpuls = configJson["impule_sensor"]["active"].as<uint8_t>();
    nodeConfig.impulsesPerValue = configJson["impule_sensor"]["impulsesPerValue"].as<float>();
    nodeConfig.debounceTime = configJson["impule_sensor"]["debounceTime"].as<int>();
    nodeConfig.startMeterReading = configJson["impule_sensor"]["startMeterReading"].as<float>();
    units.impSensUnit = configJson["impule_sensor"]["unit"].as<String>();

    nodeConfig.d0Smartmeter = configJson["smartmeter"]["active"].as<uint8_t>();
    nodeConfig.mBus = configJson["M_bus"]["active"].as<uint8_t>();

    if (nodeConfig.mBus == 1) {
        units.mBusOrD0Unit = configJson["M_bus"]["unit"].as<String>();    
    }
    else if (nodeConfig.d0Smartmeter == 1) {
        units.mBusOrD0Unit = configJson["smartmeter"]["unit"].as<String>();    
    }

    nodeConfig.i2cMeter = configJson["I2C_meter"]["active"].as<uint8_t>();
    nodeConfig.i2cMeterDevices = configJson["I2C_meter"]["I2CDeviceCount"].as<uint8_t>();
    units.I2CUnit = configJson["I2C_meter"]["unit"].as<String>();

    nodeConfig.dekoElement = configJson["deko_element"].as<uint8_t>();
    nodeConfig.dhtType = configJson["dht_type"].as<uint8_t>();
    nodeConfig.analogDelaySeconds = configJson["delay_sending_data"].as<int>();

    String nodeName = configJson["name"].as<String>();

    NodeHandlerErrorType result = nodeHandler->sendConfigToNode(id, nodeConfig, units, nodeName); 
    handleError(result, id, "SendConfig", nodeName ,client);
}

// Method handles the reset of a node
// id: Id of the node to be reset
// client: Pointer to the WebSocket client
void WebserverHandler::handleResetNode(int id, AsyncWebSocketClient *client) {
    if (id == null) {
        return;
    }
    NodeHandlerErrorType result = nodeHandler->resetNode(id);
    handleError(result, id, "RESET", "" ,client);
}

// Method handles frontend commands received via WebSocket
// msg: Message received from the frontend
// client: Pointer to the WebSocket client
// return: Returns true if the command was successfully handled, false otherwise
bool WebserverHandler::handleFrontendCommands(String msg, AsyncWebSocketClient *client) {
    StaticJsonDocument <800> doc;
    DeserializationError error = ArduinoJson::deserializeJson(doc, msg);
    if (error) {
        GatewayUtils::getInstance()->DebugPrintf("Fehler beim Deserialisieren: ", error.c_str());
    }

    GatewayUtils::getInstance()->DebugPrintf("Größe (Bytes) der Nachricht: ", msg.length());

    if (doc.containsKey("type")) {
        uint8_t type = returnIntInJsonString(doc["type"]);

        switch (type) {
            case deleteNode:
                handleDeleteNode(returnIntInJsonString(doc["nodeId"]), client);
                break;
            case changeName:
                handleChangeName(returnIntInJsonString(doc["nodeId"]), doc["nodeName"].as<String>(), client);
                break;
            case acceptNewNodes:
                handleAcceptNewNodes(client);
                break;
            case sendKey:
                handleSendKey(returnIntInJsonString(doc["nodeId"]), client);
                break;
            case led:
                handleLed(returnIntInJsonString(doc["nodeId"]), doc["value"].as<bool>(), client);
                break;
            case sendConfiguration:
                handleSendConfiguration(returnIntInJsonString(doc["nodeId"]), doc["configuration"], client);
                break;
            case resetNode:
                handleResetNode(returnIntInJsonString(doc["nodeId"]), client);
                break;
            default:
                break;
        }

        return true;
    } else {
        return false;
    }
}

// Callback method for WebSocket events
// server: Pointer to the WebSocket server
// client: Pointer to the WebSocket client
// type: Type of the WebSocket event
// arg: Additional argument for the event
// data: Pointer to the received data
// len: Length of the received data
void WebserverHandler::callbackWebsocket(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
    AwsFrameInfo *info = (AwsFrameInfo*)arg;
    String message = "";
    switch (type) {
        case WS_EVT_CONNECT:
            message = "[Info]: WebSocket client #" + String(client->id()) +  "connected from " + client->remoteIP().toString() + ".";
            GatewayUtils::getInstance()->DebugPrintf(message);
            break;
        case WS_EVT_DISCONNECT:
            message = "[Info]: WebSocket client #" + String(client->id()) +  "disconnected.";
            GatewayUtils::getInstance()->DebugPrintf(message);
            break;
        case WS_EVT_DATA:
            if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
                String jsonMsg = "";
                for (size_t i = 0; i < len; i++) {
                    jsonMsg += (char) data[i];
                }
                message = "[Info]: Message of  client #" + String(client->id()) + ": " + jsonMsg;
                GatewayUtils::getInstance()->DebugPrintf(message);
                handleFrontendCommands(jsonMsg, client);
            }
            break;
        case WS_EVT_PONG:
            break;
        case WS_EVT_ERROR:
            break;
        }
}

// Handles network connection and attempts to connect to the local network
void WebserverHandler::initWebSocket() {
    websocket->onEvent([this](AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
        this->callbackWebsocket(server, client, type, arg, data, len);
    });
    server->addHandler(websocket);
}

// Handles events received from nodes and broadcasts them via WebSocket
void WebserverHandler::initWebserverRouting() {
    server->serveStatic("/images/", LittleFS, "/images/");
    server->serveStatic("/html/", LittleFS, "/html/");
    server->serveStatic("/css/", LittleFS, "/css/");
    server->serveStatic("/dynamic/", LittleFS, "/dynamic/");

    server->on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->redirect("/home");  // umleiten zur URL "/home.html"
    });

    server->on("/home", HTTP_GET, [](AsyncWebServerRequest *request){
       request->send(LittleFS, "/html/home.html", String(), false);
    });

    server->on("/configuration", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/html/configuration.html", String(), false);
    });

    server->on("/system", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/html/system.html", String(), false);
    });

    server->on("/test", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/html/test.html", String(), false);
    });

    server->on("/nav", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/html/navigation.html", String(), false);
    });

    // need at config page -> display all New Nodes
    // need at reset option
    // only id and name to display -> json key is id
    server->on("/api/allUnconfigNodes", HTTP_GET, [this](AsyncWebServerRequest *request){
        int unconfigNodes[20] = {0};
        int unconfigNodesCount =  nodeHandler->getNodesWaitingForConfig(unconfigNodes, 20);
        GatewayUtils::getInstance()->DebugPrintf("unconfigNdoesCount: ", unconfigNodesCount);
        String jsonNodeString = "[";

        for (size_t i = 0; i < unconfigNodesCount; i++) {
            jsonNodeString += nodeHandler->getNodeById(unconfigNodes[i])->jsonifyNameAndId();
            jsonNodeString += ",";
        }

        if (jsonNodeString[jsonNodeString.length()-1] == ',') {
            jsonNodeString[jsonNodeString.length()-1] = ']';
        }else{
            jsonNodeString += "]";
        }
        request->send(200, "application/json", jsonNodeString);
        return;
    });

    // only id and name to display -> json key is id
    server->on("/api/allKeylessNodes", HTTP_GET, [this](AsyncWebServerRequest *request){
        int keylessNodes[20] = {0};
        int keylessNodesCount =  nodeHandler->getNodesWithoutEncryptKey(keylessNodes, 20);
        String jsonNodeString = "[";

        for (size_t i = 0; i < keylessNodesCount; i++) {
            jsonNodeString += nodeHandler->getNodeById(keylessNodes[i])->jsonifyNameAndId();
            jsonNodeString += ",";
        }

        if (jsonNodeString[jsonNodeString.length()-1] == ',') {
            jsonNodeString[jsonNodeString.length()-1] = ']';
        }else{
            jsonNodeString += "]";
        }
        request->send(200, "application/json", jsonNodeString);
        return;
    });

    // need at home page -> display aallKeylessNodesll Nodes
    // need at reset option
    // only id and name to display -> json key is id
    server->on("/api/allActiveNodesIdName", HTTP_GET, [this](AsyncWebServerRequest *request){
        int workingNodes[MAX_NUMBER_OF_NODES] = {0};
        int workingNodesCount = nodeHandler->getWorkingNodes(workingNodes, MAX_NUMBER_OF_NODES);
        String jsonNodeString = "[";

        for (size_t i = 0; i < workingNodesCount; i++) {
            jsonNodeString += nodeHandler->getNodeById(workingNodes[i])->jsonifyNameAndId();
            jsonNodeString += ",";
        }

        if (jsonNodeString[jsonNodeString.length()-1] == ',') {
            jsonNodeString[jsonNodeString.length()-1] = ']';
        }else{
            jsonNodeString += "]";
        }
        request->send(200, "application/json", jsonNodeString);
        return;
    });

    server->on("/api/allNodesData", HTTP_GET, [this](AsyncWebServerRequest *request){
        try {
            String jsonNodeString = "[";
            for (size_t i = 1; i <= request->args(); i++)
            {
                if (!request->hasParam("NodeNr_" + String(i)))
                    continue;
                
                if (isnan(request->hasParam("NodeNr_" + String(i))))
                    continue;
                
                int nodeId = request->getParam("NodeNr_" + String(i))->value().toInt();
                Node* node = nodeHandler->getNodeById(nodeId);
                if (node == nullptr){
                    GatewayUtils::getInstance()->DebugPrintf("Node is null: ", nodeId);
                    continue;
                }
                jsonNodeString += nodeHandler->getNodeById(nodeId)->jsonifyForWebserver();
                jsonNodeString += ",";
            }
            if (jsonNodeString[jsonNodeString.length()-1] == ',') {
                jsonNodeString[jsonNodeString.length()-1] = ']';
            }else{
                jsonNodeString += "]";
            }
            request->send(200, "application/json", jsonNodeString);
            return;
        }catch (const std::exception& ex) {
            GatewayUtils::getInstance()->DebugPrintf(String(ex.what()));
        }
    });

    // Start server
    server->begin();
}

// Runs the web server and handles network connections and events
void WebserverHandler::run() {
    handleNetworkConnection();
    //websocket->cleanupClients();
    //abfragen der Queue:
    NodeEvent eventChecker[MAX_NUMBER_OF_EVENTS]; 
    if (nodeHandler->getNodeEvent(eventChecker)){
        handleEvents(eventChecker[0]);
    }
    //nodeHandler->
}

// Destructor for WebserverHandler
WebserverHandler::~WebserverHandler() {
    delete networkConnectionHandling;
    delete server;
    delete websocket;
}

// This method handles different events related to nodes and sends the event information to connected clients via a websocket.
// event: received event from NodeHandler
void WebserverHandler::handleEvents(NodeEvent event) {
    StaticJsonDocument <200> doc; 
    String jsonNodeString;

    switch (event.type) {
        case eNodeWaitingForKey:
            doc["event"] = "NodeWaitingForKey";
            doc["id"] = event.nodeId;
            doc["name"] = nodeHandler->getNodeById(event.nodeId)->getName();
            ArduinoJson::serializeJson(doc, jsonNodeString);
            websocket->textAll(jsonNodeString);
            doc.clear();
            return;
        
        case eNodeWaitingForConfig:
            doc["event"] = "NodeWaitingForConfig";
            doc["id"] = event.nodeId;
            doc["name"] = nodeHandler->getNodeById(event.nodeId)->getName();
            ArduinoJson::serializeJson(doc, jsonNodeString);
            websocket->textAll(jsonNodeString);
            doc.clear();
            return;

        case eNewSensorValues:
            doc["event"] = "NewSensorValues";
            doc["id"] = event.nodeId;
            doc["name"] = nodeHandler->getNodeById(event.nodeId)->getName();
            doc["newData"] = nodeHandler->getNodeById(event.nodeId)->jsonifyForWebserver();
            ArduinoJson::serializeJson(doc, jsonNodeString);
            websocket->textAll(jsonNodeString);
            doc.clear();
            return;
        
        default:
            break;
    }
    
}

// This method handles the network connection of the device.
void WebserverHandler::handleNetworkConnection() {
    wlanConnected = networkConnectionHandling->ConnectedWithLocalNetwork();
    if (wlanConnected) {
        return;
    }

    if (!GatewayUtils::getInstance()->CheckPassedTime(20000, &previousMillisForWLANConnection))
        return;

    GatewayUtils::getInstance()->DebugPrintf("Connect ESP to Wifi:  " + String(my_ssid) + ". ");

    if (networkConnectionHandling->RestartProcessForLocalNetworkConnection(my_ssid, my_password)) {
        GatewayUtils::getInstance()->DebugPrintf("Connected to IP " +   WiFi.localIP().toString() + ". ");
        wlanConnected = true;
    }
    else {
        GatewayUtils::getInstance()->DebugPrintf("Not connected, WiFi Status: " + networkConnectionHandling->wlanStatusToString(WiFi.status()) + ".");
        wlanConnected = false;
    }
}