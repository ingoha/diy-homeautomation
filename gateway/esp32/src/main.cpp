#include "RFM69ToMQTTGateway.h"
#include "NodeHandler/NodeHandler.h"
#include "Webserver/WebserverHandler.h"
RFM69ToMQTTGateway* RFMToMQTTObject;
NodeHandler* nodeHandler;
WebserverHandler* webserver;

void setup() {
    GatewayUtils::getInstance()->initFileSystem();
    nodeHandler = new NodeHandler();
    webserver = new WebserverHandler(nodeHandler);
    RFMToMQTTObject = new RFM69ToMQTTGateway(nodeHandler);
}

void loop() {
    webserver->run();
    RFMToMQTTObject->Run();
}
