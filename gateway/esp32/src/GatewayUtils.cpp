#include <Arduino.h>
#include <FS.h>
#include <LittleFS.h>
#include <WiFi.h>

#include "GatewayUtils.h"
#include "Gateway_config.h"
#include "Gateway_config_board.h"

// Constructor for GatewayUtils class
GatewayUtils::GatewayUtils() {
    Serial.begin(115200);
}

// Destructor for GatewayUtils class
GatewayUtils::~GatewayUtils() {
    delete _Instance;
}

GatewayUtils* GatewayUtils::_Instance = nullptr;

// Get the instance of GatewayUtils class (Singleton pattern)
GatewayUtils* GatewayUtils::getInstance() {
    if (_Instance == nullptr) {
        _Instance  = new GatewayUtils();
    }
    return _Instance;
}

// Write two strings to the output (Serial and OLED display)
void GatewayUtils::WriteToOutput(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String s1, const String s2) {
    if (Debug) {
        Serial.print(s1);
        Serial.println(s2);
    }
    String writeToDisplay = s1 + s2;
    WriteOledOnDisplay(u8g2, writeToDisplay);
}

// Write a string and a number to the output (Serial and OLED display)
void GatewayUtils::WriteToOutput(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String s1, const int number) {
    if (Debug) {
        Serial.print(s1);
        Serial.println(number);
    }
    String numberStr(number);
    String writeToDisplay = s1 + numberStr;
    WriteOledOnDisplay(u8g2, writeToDisplay);
}

// Write a string to the output (Serial and OLED display)
void GatewayUtils::WriteToOutput(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String s1) {
    if (Debug) {
        Serial.println(s1);
    }
    String writeToDisplay = s1;
    WriteOledOnDisplay(u8g2, writeToDisplay);
}

// Initialize and format the LittleFS file system
bool GatewayUtils::initFileSystem() {
    if (!LittleFS.begin(true)) {
        Serial.println("LITTLEFS initialization failed! Try to format LITTLEFS");
        // Format the file system
        if (!LittleFS.format()) {
            Serial.println("Failed to format LITTLEFS");
            return false;
        }
        // Initialize LITTLEFS after format
        if (!LittleFS.begin(true)) {
            Serial.println("LITTLEFS initialization failed!");
            return false;
        }
    }
    return true;
}


// function: count a waiting time without using Delay Funktion
// waitingtime: the time we want to wait
// previousMillis: the passed time we have waited
// return if waiting time is passed return true, else return false
bool GatewayUtils::CheckPassedTime(const int waitingtime, uint64_t *previousMillis, bool* firstIteration) {
    uint64_t currentMillis = millis();

    if (*firstIteration == true) {
        *firstIteration = false;
        *previousMillis = currentMillis;
        return false;
    }

    if ((currentMillis - *previousMillis) >= waitingtime) {
        *previousMillis = currentMillis;
        return true;
    }
    return false;
}

// function: count a waiting time without using Delay Funktion
// waitingtime: the time we want to wait
// previousMillis: the passed time we have waited
// return if waiting time is passed return true, else return false
bool GatewayUtils::CheckPassedTime(const int waitingtime, uint64_t *previousMillis) {
    uint64_t currentMillis = millis();

    if ((currentMillis - *previousMillis) >= waitingtime) {
        *previousMillis = currentMillis;
        return true;
    }
    return false;
}

// function: connect to the WIFI several times
// u8g2: Instance of the OLED Display
// text: text which i have to display
// return none
void GatewayUtils::WriteOledOnDisplay(U8G2_SSD1306_128X64_NONAME_1_HW_I2C *u8g2, const String text) {
    u8g2->clearBuffer();                   // clear the internal memory
    u8g2->setFont(u8g2_font_ncenB08_tr);   // choose a suitable font
    u8g2->drawStr(0, 10, text.c_str());    // write something to the internal memory
    u8g2->sendBuffer();
}

// Print two strings to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s) {
    if (Debug)
        Serial.println(s);
}


//
// Print to serial in debug mode only
//
// Print the Value depending on the given Format

// Print a string and a double value to Serial as a debug message
void GatewayUtils::DebugPrintf(const uint64_t s) {
  if (Debug)
    Serial.println(s);
}

// Print a string and an integer value to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s1, const String s2)  {
  if (Debug) {
    Serial.print(s1);
    Serial.println(s2);
  }
}

// Print a string and an unsigned integer value to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s, double x) {
  if (Debug) {
    // char buff[32];
    // int size = 30 - strlen(s);
    // dtostrf(x, size, 2, buff);
    Serial.print(s);
    Serial.println(x);
  }
}

// Print a string and an integer value to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s, int x) {
  if (Debug) {
    char buff[34];
    itoa(x, buff, 10);
    Serial.print(s);
    Serial.println(buff);
  }
}

// Print a string and an unsigned integer value to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s, const unsigned int x) {
  if (Debug) {
    char buff[34];
    itoa(x, buff, 10);
    Serial.print(s);
    Serial.println(buff);
  }
}

// Print a string, an integer value, and another string to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s1, int x, const String s2) {
  if (Debug) {
    char buff[34];
    itoa(x, buff, 10);
    Serial.print(s1);
    Serial.print(buff);
    Serial.println(s2);
  }
}

// Print a string and an unsigned 64-bit integer value to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s, uint64_t x) {
  if (Debug) {
    char buff[34];
    ltoa(x, buff, 10);
    Serial.print(s);
    Serial.println(buff);
  }
}

// Print four strings to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s1, const String s2, const String s3, const String s4) {
  if (Debug) {
    Serial.print(s1);
    Serial.print(s2);
    Serial.print(s3);
    Serial.println(s4);
  }
}

// Print time information and a Payload object to Serial as a debug message
void GatewayUtils::DebugPrintf(struct tm* timeinfo, Payload RFMmsg) {
  if (Debug) {
    Serial.print(timeinfo, "%A, %B %d %Y %H:%M:%S");
    Serial.print(" Message from node ");
    Serial.print(RFMmsg.NodeId);
    Serial.print(", sensorID ");
    Serial.print(RFMmsg.DeviceId);
    Serial.print(", uptime ");
    Serial.print(RFMmsg.UptimeMilliseconds);
    Serial.print(", data ");
    Serial.print(RFMmsg.SensorData);
    Serial.print(", battery ");
    Serial.println(RFMmsg.BatteryVolts);
  }
}

// Print a string and a Payload object to Serial as a debug message
void GatewayUtils::DebugPrintf(const String s1, Payload RFMmsg) {
  if (Debug) {
      Serial.print(s1);
      DebugPrintf(&RFMmsg);
  }
}

// Print a Payload object to Serial as a debug message
void GatewayUtils::DebugPrintf(const Payload *RFMmsg) {
  if (Debug) {
    Serial.print("Message to Sender-node ");
    Serial.print(RFMmsg->NodeId);
    Serial.print(", Device ");
    Serial.print(RFMmsg->DeviceId);
    Serial.print(", Uptime ");
    Serial.print(RFMmsg->UptimeMilliseconds);
    Serial.print(", Data ");
    Serial.print(RFMmsg->SensorData);
    Serial.print(", Battery ");
    Serial.println(RFMmsg->BatteryVolts);
  }
}

// Print a string and a Payload object to Serial as a debug message
void GatewayUtils::DebugPrintf(RequestIdPayload msg) {
    Serial.print("Message:\t\t");
    Serial.print("NodeId: ");
    Serial.print(msg.nodeID);
    Serial.print("\tDeviceId: ");
    Serial.print(msg.deviceID);
    Serial.print("\tRand Numb: ");
    Serial.print(msg.randomNumber);
    Serial.println("");
}
