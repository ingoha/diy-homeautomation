#include "OLEDDisplayWrapper.h"
#include "OLEDDisplay.h"
#include "Gateway_config.h"



void OLEDDisplayWrapper::TaskFunction(void *pvParameters) {
    QueueHandle_t queue = *((QueueHandle_t*) pvParameters);
    struct OLEDText* receivedMessage;
    OLEDDisplay oledHandler;
    while (true) {
        if (xQueueReceive(queue, &(receivedMessage), 0) == pdTRUE) {
            if (receivedMessage != nullptr) {
                oledHandler.AddToSpecificRowQueue(*receivedMessage);
                delete receivedMessage;
            }
        }
        vTaskDelay(20);
        oledHandler.WriteToDisplay();
    }
}


OLEDDisplayWrapper::OLEDDisplayWrapper() {
    OLEDText queueMessage;
    _Queue = xQueueCreate(50, sizeof(&queueMessage));   // https://www.freertos.org/a00118.html -> explain this
    xTaskCreatePinnedToCore(
    TaskFunction,                                       /* Task function. */
    "OLEDDisplayTask",                                  /* Name of task. */
    10000,                                              /* Stack size of task (in words). */
    (void*) &_Queue,                                    /* Parameter passed as input of the task */
    1,                                                  /* Priority of the task. */
    &_Task2,                                            /* Task handle. */
    0);                                                 /* Core where the task should run (0 or 1). */
}

void OLEDDisplayWrapper::AddTextToQueueAndWriteToOutputs(String text, TextCategorie categorie) {
    if (Debug)
        Serial.println("[Info]: "+ text);
    OLEDText* textObj = new OLEDText();
    textObj->Text = text;
    textObj->Categorie = categorie;
    xQueueSend(_Queue, (void*) &textObj, 0);
}

