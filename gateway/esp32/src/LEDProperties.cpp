#include "LEDProperties.h"

// Method: Create a LEDProperties object using the provided parameters
// Parameters:
// - taskName: The LED task name
// - periodDurationInMs: The duration of each period in milliseconds
// - periodNumebers: The number of periods
// Return: The created LEDProperties object
LEDProperties LEDPropertiesWrapper::StructFactory(LEDTaskList taskName, int periodDurationInMs, int periodNumebers) {
    LEDProperties obj;
    obj.TaskName = taskName;
    obj.PeriodDurationInMs = periodDurationInMs;
    obj.PeriodNumebers = periodNumebers;
    return obj;
}


