#include "LEDStates.h"
#include "enums/LEDTaskList.h"


// Method: Constructor to create a LEDStates object with the specified LED numbers, size, and task name
// Parameters:
// - ledNumbers: An array of LED numbers
// - sizeOf: The size of the LED array
// - taskName: The LED task name
LEDStates::LEDStates(int* ledNumbers , unsigned short int sizeOf, LEDTaskList taskName ) {
    this->_LEDs = new unsigned short int[sizeOf];

    for (size_t i = 0; i < sizeOf; i++) {
      _LEDs[i] = ledNumbers[i];
    }

    this->_SizeOfLEDsArray = sizeOf;
    this->_TaskName = taskName;
}

// only for use as array
// Method: Default constructor for creating a LEDStates object as an array
LEDStates::LEDStates() {
}

// Method: Destructor for cleaning up resources
LEDStates::~LEDStates() {
    //  if (_LEDs != nullptr)
    //      delete[] _LEDs;
}

// Method: Get the array of LED numbers
// Return: A constant pointer to the LED numbers array
const unsigned short int* LEDStates::GetLEDs() {
    return _LEDs;
}

// Method: Get the size of the LED array
// Return: The size of the LED array
const unsigned short int LEDStates::GetLEDArraySize() {
    return _SizeOfLEDsArray;
}

// Method: Turn off all LEDs in the LED array
void LEDStates::TurnOffLEDs() {
    _ChangeStateOfLED(0);
}

// Method: Toggle the state of all LEDs in the LED array based on the provided flag
// Parameters:
// - ledsActive: Flag indicating whether the LEDs should be active or inactive
void LEDStates::ToogleLEDs(bool ledsActive) {
    _LEDsActive = ledsActive;
    _LEDsActive = 1;

    if (ledsActive)
        TurnOffLEDs();
    else
        TurnOnLEDs();
}

// Method: Turn on all LEDs in the LED array
void LEDStates::TurnOnLEDs() {
    _ChangeStateOfLED(1);
}

// Method: Get the task name associated with the LEDStates object
// Return: The LED task name
LEDTaskList LEDStates::GetTaskName() {
    return _TaskName;
}

// Method: Change the state of all LEDs in the LED array to the specified state
// Parameters:
// - state: The state to set the LEDs to (0 for off, 1 for on)
void LEDStates::_ChangeStateOfLED(unsigned short int state) {
    for (size_t i = 0; i < _SizeOfLEDsArray; i++) {
        digitalWrite(_LEDs[i], state);
    }
}
