#include "MQTTGateway.h"
#include "GatewayUtils.h"
#include <WiFi.h>
#include <Arduino.h>
#include <time.h>
#include "LEDProperties.h"
#include <CircularBuffer.h>
#include "LEDBlink.h"
#include "OLEDDisplayWrapper.h"
#include "Gateway_config_board.h"



// Function: Construktor creates the MQTT-Client
// mqtt_server_ip: the used Broker IP
// Return: none
MQTTGateway::MQTTGateway(const String mqtt_server_ip, NetworkConnection* obj, OLEDDisplayWrapper* oledDisplay) {
    _OledDisplay = oledDisplay;
    _MqttBrokerIP = mqtt_server_ip;
    _MqttClient = new PubSubClient(_EspWifiClient);
    _NetworkRelevantObj = obj;
}

// Function: destruktor
// Return: none
MQTTGateway::~MQTTGateway() {
    delete _MqttClient;
    delete _NetworkRelevantObj;
    delete _OledDisplay;
}

// Function: shows the Status of the MQTT-Client
// print_status: true if status has to print
// Return: the State of the Client
bool MQTTGateway::IsConnectedWithBroker() {
    if (_MqttClient->connected()) {
        _MqttClient->loop();
        return true;
    }
    else {
        return false;
    }
}


// Function: shows the Status of the MQTT-Client
// print_status: true if status has to print
// Return: the State of the Client
int MQTTGateway::PrintMQTTClientStatus(bool print_status) {
    if (print_status) {
        GatewayUtils::getInstance()->DebugPrintf("Status des MQTT Clients: ", _GetClientStateAsString());
    }
    return _MqttClient->state();    // hint for status: https:// pubsubclient.knolleary.net/api#state
}

// Function: converts a float to a float with a specific decimal places number
// value: the float value with an undefined value of decimal places
// decimal_places: the number of decimal places we want to use
// Return: the float with the specifc number of decimal places
float MQTTGateway::_FloatWithDecimal(float value, int decimal_places) {
    float result_value;
    int decimal = 0;
    switch (decimal_places) {
    case 1:
        decimal = (int)(value *10);
        result_value = ((float) decimal/10.0);
    case 2:
        decimal = (int)(value *100);
        result_value = ((float) decimal/100.0);
    }

    return result_value;
}

// Function: init the MQTT client; uses extern callback function
// Param callback: callback function with the parameter:
// MQTT-topic: from which MQTT Topic
// MQTT-message: the  MQTT wich is send
// MQTT-message-length: char-length of the MqTT message
// retzurn: none
//  void MQTTGateway::MqttClientInit(std::function<void(String, uint8_t*, unsigned int)> callback) // use in setup -> evtl
void MQTTGateway::MqttClientInit(std::function<void(char*, byte*, unsigned int)> callback) {    // use in setup -> evtl
    _MqttClient->setServer(_MqttBrokerIP.c_str(), 1883);
    _MqttClient->setCallback(callback);
}

// Function: plan to use this if possible
// MQTT-topic: from which MQTT Topic
// MQTT-message: the  MQTT wich is send
// MQTT-message-length: char-length of the MqTT message
// Return:
Payload* MQTTGateway::Callback(char* topic, byte* message, unsigned int length, LEDBlink* ledManager) {
    String topic_str(topic);
    ledManager->AddTask(LEDPropertiesWrapper::StructFactory(MqttMessageReceivedState));
    _MqttMessageEditing(topic_str, message, length);
    if (!_AllowedTopicLenght()) {
        return nullptr;
    }
    Payload* MQTT_Payload_Message = new Payload;
    // get info out of the topic about the target nodeID and DeviceId
    MQTT_Payload_Message->NodeId = topic_str.substring(19, 2).toInt();
    MQTT_Payload_Message->DeviceId = topic_str.substring(25, 2).toInt();

    // reaction if MQTT msg is for the Gateway
    if (MQTT_Payload_Message->NodeId == 1) {
        if (_SendRequestedInfoAboutGatewayViaMQTT(MQTT_Payload_Message->DeviceId)) {
            ledManager->AddTask(LEDPropertiesWrapper::StructFactory(MqttMessagePublishState));
        }
        return nullptr;
    }
    _CallbackReadCmd = _CheckValueForRead();
    // get the DeviceId of the MQTT Message
    if (_MQTTSendWhileEvaluatingSensorID(MQTT_Payload_Message)) {
        ledManager->AddTask(LEDPropertiesWrapper::StructFactory(MqttMessagePublishState));
    }
    return MQTT_Payload_Message;
}

// Function: try to reconnect to mqtt broker for a set intervall and attemps
// Param 1: how often try to reconnect
// Param 2: how many milliseconsds to wait for a reconnect try
// Return: True if connecting is working; false if no connectig can occure
bool MQTTGateway::ConnectToBroker() {  // z_ToDo schauen ob bool, die nur eine Zeit geht und des dann macht
    if (_MqttClient->connect("ESP32Client")) {
        _OledDisplay->AddTextToQueueAndWriteToOutputs("Connected To Broker. ", MQTT);
        return true;
    }
    else {
        _OledDisplay->AddTextToQueueAndWriteToOutputs("No Connection with Broker, reason: " + _GetClientStateAsString() + ". ", MQTT);
        return false;
    }
}

String MQTTGateway::_GetClientStateAsString() {
  switch (_MqttClient->state()) {
    case MQTT_CONNECTION_TIMEOUT:
      return "MQTT_CONNECTION_TIMEOUT";
    case MQTT_CONNECTION_LOST:
      return "MQTT_CONNECTION_LOST";
    case MQTT_CONNECT_FAILED:
      return "MQTT_CONNECT_FAILED";
    case MQTT_DISCONNECTED:
      return "MQTT_DISCONNECTED";
    case MQTT_CONNECTED:
      return "MQTT_CONNECTED";
    case MQTT_CONNECT_BAD_PROTOCOL:
      return "MQTT_CONNECT_BAD_PROTOCOL";
    case MQTT_CONNECT_BAD_CLIENT_ID:
      return "MQTT_CONNECT_BAD_CLIENT_ID";
    case MQTT_CONNECT_UNAVAILABLE:
      return "MQTT_CONNECT_UNAVAILABLE";
    case MQTT_CONNECT_BAD_CREDENTIALS:
      return "MQTT_CONNECT_BAD_CREDENTIALS";
    case MQTT_CONNECT_UNAUTHORIZED:
      return "MQTT_CONNECT_UNAUTHORIZED";
    default:
      return "UNKNOWN_STATE";
  }
}


bool MQTTGateway::Publish(String mqtttopic, String mqttmsg){
    GatewayUtils::getInstance()->DebugPrintf("MQTT published at channel: " +  mqtttopic + " with message: " + mqttmsg);
    return _MqttClient->publish(mqtttopic.c_str(), mqttmsg.c_str());
}


// Function: publish MQTT message of the received RFM message
// rfm_message: the Struct Payload RFM Message
// rfm_rssi: the RSSI Value of the connection between node and Gateway
// return: return true if publish worked
bool MQTTGateway::MqttPublishRfm(Payload rfm_message, int rfm_rssi) {
    if (!getLocalTime(_NetworkRelevantObj->GetTimeInfo(), 50)) {
        GatewayUtils::getInstance()->DebugPrintf("Failed to obtain time");
        return false;
    }
    // mit Format printen  // https:// cplusplus.com/reference/ctime/strftime/
    GatewayUtils::getInstance()->DebugPrintf(_NetworkRelevantObj->GetTimeInfo(), rfm_message);  


    if (CatchRFMMsgForLightAcknoledge(rfm_message))
        return true;

    if (rfm_message.DeviceId == 6 || rfm_message.DeviceId == 7) {
        rfm_message.SensorData = _FloatWithDecimal(rfm_message.SensorData, 1);
    }

    bool all_good = true;

    if (!Publish("home/rfm_gw/nb/node" + String(rfm_message.NodeId) + "/dev" + String(rfm_message.DeviceId) + "/data", String(rfm_message.SensorData)))
        all_good = false;

    if (!Publish("home/rfm_gw/nb/node" + String(rfm_message.NodeId) + "/dev" + String(rfm_message.DeviceId) + "/uptime", String(rfm_message.UptimeMilliseconds)))
        all_good = false;

    char char_buff[8];  // 8 Indize Stellen: 1x VZ, 3x Vor, 1x komma, 2x Nach + null = 8 Stellen
    dtostrf(rfm_message.BatteryVolts, 3, 2, char_buff);
    if (!Publish("home/rfm_gw/nb/node" + String(rfm_message.NodeId) + "/battery", String(char_buff)))
        all_good = false;

    if (!Publish("home/rfm_gw/nb/node" + String(rfm_message.NodeId) + "/rssi", String(rfm_rssi)))
        all_good = false;

    return all_good;
}

bool MQTTGateway::CatchRFMMsgForLightAcknoledge(Payload rfm_message) {
    if (rfm_message.DeviceId != DEKO_ELEMENT)
        return false;

    switch ((int)rfm_message.SensorData) {
        case 0:
            Publish("home/rfm_gw/nb/node03/dev20", "ON");
            Publish("home/rfm_gw/nb/node03/errorOFF", "no Error");
            return true;

        case 1:
            Publish("home/rfm_gw/nb/node03/dev20", "OFF");
            Publish("home/rfm_gw/nb/node03/errorON", "no Error");
            return true;

        default:
            return false;
    }
}

// __________________________________________________Callback-Handling_____________________________________

// Function: fetch Infos of the received MQTT message and topic
// topic: from which MQTT Topic
// message: the  MQTT wich is send
// length: char-length of the MqTT message
// Return none
void MQTTGateway::_MqttMessageEditing(String topic, byte* message, unsigned int length) {
    _CallbackTopic = topic;
    _CallbackMessage = "";
    for (int i = 0; i < length; i++) {
        _CallbackMessage += (char)message[i];
    }
    Serial.print("From this MQTT-topic: ");
    Serial.print(_CallbackTopic);
    //  DebugPrintf ("From this MQTT-topic: " , _CallbackTopic);
    GatewayUtils::getInstance()->DebugPrintf("With Message With message: ", _CallbackMessage);
    return;
}

// Function: check if the received MQTT-Topic consists of right place numbers
// Return: true if the number of the topic is 27
bool MQTTGateway::_AllowedTopicLenght() {
    if (_CallbackTopic.length() != 27) {
        return false;
    }
    return true;
}

// Funktion: schaut ob die MQTT-MEssage Read ist und setzt das byte dafür
// Function: set the value of the bool _CallbackReadCmd
// Return: none
int MQTTGateway::_CheckValueForRead() {
    if (_CallbackMessage == "READ") {
        //  _CallbackReadCmd = 1;
        return 1;
    }
    //  _CallbackReadCmd = 0;
    return 0;
}


// Function: select specifier for an action, depends on the choosen sensor id in MQTT Topic -> get these values from the Python-Gateway-Code
// sensor_id: the Sensor id which is in the MQTT Topic
// Return: a specifier which helps to detect, what kind of message we want to send -> this Enum should help to add or handle the reactions
SensorIdValues MQTTGateway::_ChooseSensorId(short int sensor_id) {
    switch (sensor_id) {
    case 5:
    return statMess;
    case 6:
    return statMess;
    case 8:
    return statMess;
    case 16 ... 31:
    return statMess;

    case 0:
    if (_CallbackReadCmd != 1)  {
        return defaultcase;
    }
    return realMess;
    case 2:
    if (_CallbackReadCmd != 1)  {
        return defaultcase;
    }
    return realMess;
    case 3:
    if (_CallbackReadCmd != 1)  {
        return defaultcase;
    }
    return realMess;
    case 4:
    if (_CallbackReadCmd != 1)  {
        return defaultcase;
    }
    return realMess;
    case 40 ... 71:
    if (_CallbackReadCmd != 1)  {
        return defaultcase;
    }
    return realMess;

    case 1:
    return intMess;
    case 7:
    return intMess;
    case 32 ... 39:
    return intMess;

    case 72:
    return strMess;

    default:
    return defaultcase;
    }
}

// Function: creates the RFM-Datatype-Structure depending on the Sensorid
// mqtt_callback_payload_message: the Struct Payload message we create to send it to the Node
// Return: true if a message has to be send to the node
bool MQTTGateway::_MQTTSendWhileEvaluatingSensorID(Payload *mqtt_callback_payload_message) {
    SensorIdValues device_id_val = _ChooseSensorId(mqtt_callback_payload_message->DeviceId);
    switch (device_id_val) {
    case statMess:
        if (_CallbackMessage == "ON") {
            mqtt_callback_payload_message->UptimeMilliseconds = 1;
            mqtt_callback_payload_message->SensorData = 0;
            mqtt_callback_payload_message->BatteryVolts = 0;
            GatewayUtils::getInstance()->DebugPrintf("ON");
            break;  
        }
        if (_CallbackMessage == "OFF") {
            mqtt_callback_payload_message->UptimeMilliseconds = 0;
            mqtt_callback_payload_message->SensorData = 1;
            mqtt_callback_payload_message->BatteryVolts = 0;
            GatewayUtils::getInstance()->DebugPrintf("OFF");
            break;
        }else {
            Publish("home/rfm_gw/nb/node" + String(mqtt_callback_payload_message->NodeId) + "/dev91",
            "syntax error code 3 for node " + String(mqtt_callback_payload_message->NodeId)); //Callback Syntax Error Code 4
        return true;
        }

    case realMess:
        GatewayUtils::getInstance()->DebugPrintf("realMess");
        // check use
        // message.fltVal = float(message.payload) -> wait, not working?
        // zeile 107 in Python gateway Codes
        break;

    case intMess:
        GatewayUtils::getInstance()->DebugPrintf("intMess");
        // check use
        // message.intVal = int(message.payload)
        // einfach einen int Wert speichern in die Payload und den frage ich iwo ab -> wie soll der Int aussehen? oder sende ich den mit MQTT mit?
        // zeile 112 in Python gateway Codes
        break;

    case strMess:
        GatewayUtils::getInstance()->DebugPrintf("strMess");
        break;

    default:
        Publish("home/rfm_gw/nb/node" + String(mqtt_callback_payload_message->NodeId) + "/dev91",
                "syntax error code 4 for node " + String(mqtt_callback_payload_message->NodeId)); //Callback Syntax Error Code 4
        return true;
    }
    GatewayUtils::getInstance()->DebugPrintf("MQTT-Message at Callback: ");
    GatewayUtils::getInstance()->DebugPrintf(mqtt_callback_payload_message);
    return false;
}

// funktion: check MQTT topic to get Infos about gateway; use sensor id as dummy
// dummy_sensor_id: the written Sensor id Value in the topic -> Sensor = Device
// return true if a MQTT Message is published
bool MQTTGateway::_SendRequestedInfoAboutGatewayViaMQTT(short int dummy_sensor_id) {
    
  if (dummy_sensor_id == 0) {
    Publish("home/rfm_gw/nb/node01/dev00",
            _NetworkRelevantObj->GetTimeStamp());
    return true;
  }
  if (dummy_sensor_id == 3){
    Publish("home/rfm_gw/nb/node01/dev03",
            _VERSION);
    return true;
    }
    return false;
}

