// #include <Arduino.h>
// #include <Wire.h>
#include "OLEDDisplay.h"


// Method: Write the text lines to the OLED display
// Renders the text lines for RFM, MQTT, and Info categories on the display
void OLEDDisplay::WriteToDisplay() {
    _OledDisplay->firstPage();
    do {
        _RowRFM->HandleTextLine();
        _RowMQTT->HandleTextLine();
        _RowInfo->HandleTextLine();
    } while ( _OledDisplay->nextPage() );

    _RowRFM->HandleAllTextChecks();
    _RowMQTT->HandleAllTextChecks();
    _RowInfo->HandleAllTextChecks();
}

// Method: Add text to the queue of a specific row based on the category
// Parameters:
// - textObj: The OLEDText object containing the text and category
// Adds the text to the corresponding row's queue for rendering on the display
void OLEDDisplay::AddToSpecificRowQueue(OLEDText textObj) {
    switch (textObj.Categorie) {
    case RFM:
        _RowRFM->AddText(textObj.Text);
        return;
    case MQTT:
        _RowMQTT->AddText(textObj.Text);
        return;
    case Info:
        // Serial.println("in Switch: " + String(textObj.Text));
        _RowInfo->AddText(textObj.Text);
        return;
    default:
        return;
    }
}

// Constructor: Initializes the OLEDDisplay object
// Creates an instance of U8G2_SSD1306_128X64_NONAME_1_HW_I2C for the OLED display
// Initializes the text rows for RFM, MQTT, and Info categories
OLEDDisplay::OLEDDisplay() {
    _OledDisplay = new U8G2_SSD1306_128X64_NONAME_1_HW_I2C (U8G2_R0, /* reset=*/ U8X8_PIN_NONE, /* clock=*/ SCL, /* data=*/ SDA);   // ESP32 Thing, HW I2C with pin remappingt
    _OledDisplay->begin();
    _OledDisplay->setFontMode(0);       // calculate the pixel width of the text
    _RowRFM = new OLEDTextRow(_OledDisplay, "RFM:", 16);
    _RowMQTT = new OLEDTextRow(_OledDisplay, "MQTT:", 37);
    _RowInfo = new OLEDTextRow(_OledDisplay, "Info:" , 58);
}

// Destructor: Cleans up resources
// Deletes the OLED display and text row objects
OLEDDisplay::~OLEDDisplay() {
    delete _OledDisplay;
    delete _RowRFM;
    delete _RowMQTT;
    delete _RowInfo;
}
