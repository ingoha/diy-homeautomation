#include "RFM69Radio.h"
#include "Gateway_config.h"
#include "Gateway_config_board.h"
#include "GatewayUtils.h"




// Function: link to the Radio Instance the SPI Class, the Interrupt Pin and if an RFM69HCW is used
// ESP32IRQ: PinNumber of the Interrupt
// SPI_CLK: PinNumber of the SPI Clock
// SPI_MISO: PinNumber of the SPI MISO
// SPI_MOSI: PinNumber of the SPI MOSI
// SPI_SS: PinNumber of the SPI Slave Select
// RFM69HCW: true if this module is in use
// Return: none
RFM69Radio::RFM69Radio(int8_t ESP32IRQ, int8_t SPI_CLK, int8_t SPI_MISO, int8_t SPI_MOSI, int8_t SPI_SS, bool RFM69HCW = false) {
    SPIClass *SPI_User;
    SPI_User = new SPIClass;
    SPI_User->begin(SPI_CLK, SPI_MISO, SPI_MOSI, SPI_SS);
    radio = new RFM69(SPI_SS, ESP32IRQ, RFM69HCW, SPI_User);
    RfmInit();
    //  DebugPrintf("RFM_Radio created");
}

// Function: destructor
// Return: none
RFM69Radio::~RFM69Radio() {
  delete radio;
  //  DebugPrintf("RFM_Radio deleted");
}

// Function: Read the Message that is send by Node
// msgBuffer: Pointer to a byte buffer in which the msg is stored. Gateway class handles interpretation
// Return: Size of received message
int RFM69Radio::GetMessage(byte* msgBuffer) {
    int msgLength = (int) radio->PAYLOADLEN;
    memcpy(msgBuffer, radio->DATA, msgLength);
    CheckRfmConnected();
    return msgLength;;
}

// Function: return the RSSI Value
// Return: the RSSI Value
int RFM69Radio::Rssi() {
  return radio->RSSI;
}


// Function: handle Messages send from Node and send ACK back if wanted
// Return: true if an acknowledge is requeseted
bool RFM69Radio::MessageReceived() {
    if (radio->receiveDone()) {
        GatewayUtils::getInstance()->DebugPrintf("Packet received");
        if (radio->ACKRequested()) {
            radio->sendACK();
            GatewayUtils::getInstance()->DebugPrintf(" - ACK sent");
        }
        return true;
    }
    CheckRfmConnected();
    return false;
}

// Function: gives debug advice if RFM69 is not connected
// Return: none
void RFM69Radio::CheckRfmConnected() {
    uint8_t checkvalue = radio->readReg(REG_IRQFLAGS1);
    if (checkvalue == 255) {
        // GatewayUtils::getInstance()->DebugPrintf("check if RFM69 lost connection ");
    }
}

// Function: send msg to Node with Burst at first, wait for a ack
// msg: Struct-Message, which has to be send
// Return: true if node send an ack; false if node not send ack
bool RFM69Radio::SendMessageWithBurst(const void* msgBuffer, int msgSize, int nodeId) {
    uint64_t burst_time_start = (uint64_t) millis();
    radio->listenModeSendBurst(nodeId, msgBuffer, msgSize);
    bool send_worked;
    if (SendMessage(msgBuffer, msgSize, nodeId))
        send_worked = true;
    else
        send_worked = false;
    uint64_t burst_time_stop = (uint64_t) millis();
    GatewayUtils::getInstance()->DebugPrintf("Second for Burst duration: ", (burst_time_stop-burst_time_start));
    return send_worked;
}

// Function: send msg to Node, wait for a ack
// Param 1: RFM Message, which has to be send
// Return: true if node send ack; false if node not send ack
bool RFM69Radio::SendMessage(const void* msgBuffer, int msgSize, int nodeId) {
    //  without ack: radio->send(toWhichNode, (const void*)(&msg), sizeof(msg)); // z_ToDO die andere Send methode nehmn
    if (radio->sendWithRetry(nodeId, msgBuffer, msgSize)) {
        GatewayUtils::getInstance()->DebugPrintf("Node ", nodeId , " received message");
        return true;
    }
    GatewayUtils::getInstance()->DebugPrintf("Node ", nodeId , " DIDNT receive message");
    if (Debug)
        CheckRfmConnected();
    return false;
}


// Function: Init the RFM Communication
// Return: void function returns nothing
void RFM69Radio::RfmInit() {
    radio->initialize(FREQUENCY, NODEID, NETWORKID);    // Network-Parameters
    radio->encrypt(ENCRYPTKEY);
    radio->setPowerLevel(31);
    radio->setHighPower();
    radio->spyMode();
  // proposal: check which Node are in the Network an display this Information
}

// Function: Encrypt radio with default key
// Return: void function returns nothing
void RFM69Radio::encryptDefault() {
    radio->encrypt(ENCRYPTKEY);
}

// Function: Encrypt radio with no key so that unencrypted messages can be received
// Return: void function returns nothing
void RFM69Radio::encryptNone() {
    radio->encrypt(0);
}
