#include "NodeHandler/NodeHandler.h"

#ifdef NODEHANDLER_DEBUG
void NodeHandler::addNode(int id, bool key, uint8_t mode) {
    numOfNodes++;
    Node node(id);

    addData(&node, id);
    node.setHasEncryptKey(key);
    DetailedNodeConfig conf;
    conf.operationMode = mode;
    node.setConfig(conf);
    activeNodes.push_back(node);
}

void NodeHandler::addData(Node *node, int id) {
    Payload msg = {.NodeId = id, .DeviceId = eSensorDigitalID, .UptimeMilliseconds = millis(), .SensorData = 1.0, .BatteryVolts = 5.2};
    node->messageReceived(&msg, -10);
    msg.DeviceId = eSensorImpulsID; msg.SensorData = 3.4; msg.BatteryVolts = 2.0;
    node->messageReceived(&msg, 20);
    // node->print();
    msg.DeviceId = eSensorTempHumID; msg.SensorData = 2.0; msg.BatteryVolts = 3.4;
    node->messageReceived(&msg, 30);
    // node->print();
    msg.DeviceId = eInterfaceD0Smartmeter; msg.SensorData = 35232.3; msg.BatteryVolts = 4.3;
    node->messageReceived(&msg, -23);
    // node->print();
    msg.DeviceId = eActorDekoID; msg.SensorData = 1.0; msg.BatteryVolts = 3.3;
    node->messageReceived(&msg, 38);
    node->print();
}
void NodeHandler::printAllNodes() {
    for (int i = 0; i < activeNodes.size(); i++) {
        activeNodes[i].print();
        Serial.println("");
    }
}
#endif
