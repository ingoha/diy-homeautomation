#include "math.h"
#include "Nodehandler/Node.h"
#include "GatewayUtils.h"

const int SERIALIZE_JSON_SIZE = 500;
const int SERIALIZE_JSON_SMALL_SIZE = 200;
const int DESERIALIZE_JSON_SIZE = 1000;

// Constructor of the node class
// id:      The id the node should get
// return:  Constructor does not return anything
Node::Node(int id) {
    this->id = id;
}

Node::Node(const Node& node) {
    // empty
}

Node::Node() {
    // empty
}

// Second Constructor of the node class
// objectAsJson: Gets a JSON represantation of the node and copies the values from the string to the attributes
// return:       Constructor does not return anything
Node::Node(String objectAsJson) {
    StaticJsonDocument <DESERIALIZE_JSON_SIZE> doc;
    deserializeJson(doc, objectAsJson);

    id = doc["id"].as<int>();
    name = doc["name"].as<String>();
    hasEncryptKey = doc["hasKey"].as<bool>();

    config.operationMode = doc["conf"]["mode"].as<uint8_t>();
    config.sensorDigital = doc["conf"]["sDig"].as<uint8_t>();
    config.sensorDistance = doc["conf"]["sDis"].as<uint8_t>();
    config.sensorImpuls = doc["conf"]["sImp"].as<uint8_t>();
    config.startMeterReading = doc["conf"]["sImpStrt"].as<float>();
    config.impulsesPerValue = doc["conf"]["sImpPerVal"].as<float>();
    config.debounceTime = doc["conf"]["sImpBounce"].as<int>();
    config.d0Smartmeter = doc["conf"]["iD0"].as<uint8_t>();
    config.mBus = doc["conf"]["iMbus"].as<uint8_t>();
    config.i2cMeter = doc["conf"]["iI2C"].as<uint8_t>();
    config.i2cMeterDevices = doc["conf"]["iI2CDev"].as<uint8_t>();
    config.dekoElement = doc["conf"]["aLED"].as<uint8_t>();
    config.analogDelaySeconds = doc["conf"]["period"].as<int>();
    config.dhtType = doc["conf"]["dht"].as<byte>();

    measurementUnits.mBusOrD0Unit = doc["unit"]["uMbusD0"].as<String>();
    measurementUnits.impSensUnit = doc["unit"]["uImp"].as<String>();
    measurementUnits.I2CUnit = doc["unit"]["uI2C"].as<String>();
}

// Creates a JSON-representation of a node object including id, and the config. Is to be safed in the filesystem
// Returns: String - JSON representation of a node
String Node::jsonifyForFile() {
    StaticJsonDocument <SERIALIZE_JSON_SIZE> doc;
    JsonObject obj = doc.to<JsonObject>();
    obj["id"] = id;
    obj["name"] = name;
    obj["hasKey"] = hasEncryptKey;

    JsonObject jsonConfig = obj.createNestedObject("conf");
    jsonConfig["mode"] = config.operationMode;
    jsonConfig["sDig"] = config.sensorDigital;
    jsonConfig["sDis"] = config.sensorDistance;
    jsonConfig["sImp"] = config.sensorImpuls;
    jsonConfig["sImpStrt"] = config.startMeterReading;
    jsonConfig["sImpPerVal"] = config.impulsesPerValue;
    jsonConfig["sImpBounce"] = config.debounceTime;
    jsonConfig["iD0"] = config.d0Smartmeter;
    jsonConfig["iMbus"] = config.mBus;
    jsonConfig["iI2C"] = config.i2cMeter;;
    jsonConfig["iI2CDev"] = config.i2cMeterDevices;
    jsonConfig["aLED"] = config.dekoElement;
    jsonConfig["period"] = config.analogDelaySeconds;
    jsonConfig["dht"] = config.dhtType;

    JsonObject jsonUnits = obj.createNestedObject("unit");
    jsonUnits["uMbusD0"] = measurementUnits.mBusOrD0Unit;
    jsonUnits["uImp"] = measurementUnits.impSensUnit;
    jsonUnits["uI2C"] = measurementUnits.I2CUnit;

    String objectAsJson;
    serializeJson(doc, objectAsJson);
    doc.clear();
    return objectAsJson;
}

// Creates a JSON-represantation of the name and the ID for the node. Called by webserver
// return:  String - JSON represantation of name and id
String Node::jsonifyNameAndId() {
    StaticJsonDocument <SERIALIZE_JSON_SMALL_SIZE> doc;
    JsonObject obj = doc.to<JsonObject>();
    obj["id"] = id;
    obj["name"] = name;
    String objectAsJson;
    serializeJson(obj, objectAsJson);
    obj.clear();
    return objectAsJson;
}

// Creates a JSON-represantation of a node object includig only the data which is needed by the webserver to show recent values
// Returns: String - JSON represantation of node for the webserver
String Node::jsonifyForWebserver() {
    StaticJsonDocument <SERIALIZE_JSON_SIZE> doc;
    JsonObject obj = doc.to<JsonObject>();
    obj["id"] = id;
    time_t currentTime;
    time(&currentTime);
    double diffInSeconds = difftime(currentTime, lastSeen);
    // GatewayUtils::getInstance()->DebugPrintf("Seconds: ",diffInSeconds);
    obj["lastValUpd"] = diffInSeconds;
    obj["temperature"] = round(sensorValues.lastTemperatureValue * 100.0) / 100.0;
    obj["humidity"] = round(sensorValues.lastHumidityValue * 100.0) / 100.0;
    obj["rssi"] = sensorValues.rssi;
    obj["battery"] = round(sensorValues.battery * 100.0) / 100.0;
    if (this->config.sensorDigital) {
        obj["digSens"] = round(sensorValues.lastImpulseOrDigitalValue * 100.0) / 100.0;
    }
    if (this->config.sensorImpuls) {
        obj["impSens"]["value"] = round(sensorValues.lastImpulseOrDigitalValue * 100.0) / 100.0;
        obj["impSens"]["unit"] = getUnits().impSensUnit;
    }
    if (this->config.mBus) {
        obj["MBusOrD0"]["value"] = round(sensorValues.lastD0MeterOrMBusValue * 100.0) / 100.0;
        obj["MBusOrD0"]["unit"] = getUnits().mBusOrD0Unit;
    }
    if (this->config.d0Smartmeter) {
        obj["MBusOrD0"]["value"] = round(sensorValues.lastD0MeterOrMBusValue * 100.0) / 100.0;
        obj["MBusOrD0"]["unit"]= getUnits().mBusOrD0Unit;
    }
    if (this->config.sensorDistance) {
        obj["distance"] = round(sensorValues.lastSensorDistanceValue * 100.0) / 100.0;
    }
    if (this->config.dekoElement) {
        obj["deko"] = sensorValues.dekoElement;
    }
    if (this->config.i2cMeter) {
        obj["I2c"]["value"] = round(sensorValues.lastI2CValue * 100.0) / 100.0;
        obj["I2c"]["unit"] = getUnits().I2CUnit;

    }
    String objectAsJson;
    serializeJson(doc, objectAsJson);
    doc.clear();
    return objectAsJson;
}


void Node::print() {
    GatewayUtils::getInstance()->DebugPrintf("ID:\t\t\t", id);
    GatewayUtils::getInstance()->DebugPrintf("Last Seen:\t\t", (int)lastSeen);
    GatewayUtils::getInstance()->DebugPrintf("Battery:\t\t", sensorValues.battery);
    GatewayUtils::getInstance()->DebugPrintf("RSSI:\t\t\t", sensorValues.rssi);
    GatewayUtils::getInstance()->DebugPrintf("Has key:\t\t", hasEncryptKey);
    GatewayUtils::getInstance()->DebugPrintf("");
    GatewayUtils::getInstance()->DebugPrintf("Operation Mode:\t\t", config.operationMode);
    GatewayUtils::getInstance()->DebugPrintf("Sensor Digital:\t\t", config.sensorDigital);
    GatewayUtils::getInstance()->DebugPrintf("Sensor Distance:\t", config.sensorDistance);
    GatewayUtils::getInstance()->DebugPrintf("Sensor Impuls:\t\t", config.sensorImpuls);
    GatewayUtils::getInstance()->DebugPrintf("Unit:\t\t\t", measurementUnits.impSensUnit);
    GatewayUtils::getInstance()->DebugPrintf("Start Meter Reading:\t", config.startMeterReading);
    GatewayUtils::getInstance()->DebugPrintf("Impulses per value:\t", config.impulsesPerValue);
    GatewayUtils::getInstance()->DebugPrintf("Debounce Time:\t\t", config.debounceTime);
    GatewayUtils::getInstance()->DebugPrintf("D0 Smartmeter:\t\t", config.d0Smartmeter);
    GatewayUtils::getInstance()->DebugPrintf("MBus:\t\t\t", config.mBus);
    GatewayUtils::getInstance()->DebugPrintf("Unit:\t\t\t", measurementUnits.mBusOrD0Unit);
    GatewayUtils::getInstance()->DebugPrintf("I2C Meter:\t\t", config.i2cMeter);
    GatewayUtils::getInstance()->DebugPrintf("Unit:\t\t\t", measurementUnits.I2CUnit);
    GatewayUtils::getInstance()->DebugPrintf("I2C Devices:\t\t", config.i2cMeterDevices);
    GatewayUtils::getInstance()->DebugPrintf("LED Element:\t\t", config.dekoElement);
    GatewayUtils::getInstance()->DebugPrintf("AnalogDelaySeconds:\t", config.analogDelaySeconds);
    GatewayUtils::getInstance()->DebugPrintf("DHT type:\t\t", config.dhtType);
    GatewayUtils::getInstance()->DebugPrintf("");
    GatewayUtils::getInstance()->DebugPrintf("Imp/DigSens Data:\t", sensorValues.lastImpulseOrDigitalValue);
    GatewayUtils::getInstance()->DebugPrintf("Distance Data:\t\t", sensorValues.lastSensorDistanceValue);
    GatewayUtils::getInstance()->DebugPrintf("D0/Mbus Data:\t\t", sensorValues.lastD0MeterOrMBusValue);
    GatewayUtils::getInstance()->DebugPrintf("Temperature Data:\t", sensorValues.lastTemperatureValue);
    GatewayUtils::getInstance()->DebugPrintf("Humidity Data:\t\t", sensorValues.lastHumidityValue);
    GatewayUtils::getInstance()->DebugPrintf("LED Status:\t\t", sensorValues.dekoElement, "\n");
}


// Function should be called when message from this node was received to update the last seen time and a sensor value
// msg:         read only reference to a payload object containing new information for one of the sensors
// return:      void - Function returns nothing
void Node::messageReceived(const Payload* msg, int rssi) {
    time(&lastSeen);
    sensorValues.battery = msg->BatteryVolts;
    sensorValues.rssi = rssi;
    int devId = msg->DeviceId;
    if ((devId == eSensorDigitalID) || (devId == eSensorImpulsID)) {
        sensorValues.lastImpulseOrDigitalValue = msg->SensorData;
        return;
    }
    if ((devId == eInterfaceD0Smartmeter) || (devId == eInterfaceMBusID)) {
        sensorValues.lastD0MeterOrMBusValue = msg->SensorData;
        return;
    }
    if (devId == eSensorDistanceID) {
        sensorValues.lastSensorDistanceValue = msg->SensorData;
        return;
    }
    if (devId == eSensorTempHumID) {
        sensorValues.lastTemperatureValue = msg->SensorData;
        return;
    }
    if (devId == eSensorHumidityID) {
        sensorValues.lastHumidityValue = msg->SensorData;
        return;
    }
    if (devId == eActorDekoID) {
        sensorValues.dekoElement = (bool)msg->SensorData;
        return;
    }
    return;
}
