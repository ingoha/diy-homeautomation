#include <FS.h>
#include <LittleFS.h>

#include "NodeHandler/NodeHandler.h"
#include "GatewayUtils.h"
#include "Gateway_config.h"

NodeHandler::NodeHandler() {
    this->readNodesFromFile();
    GatewayUtils::getInstance()->DebugPrintf("Number of nodes at startup: ", this->numOfNodes);
}

NodeHandler::~NodeHandler() {
    // Destructor implementation
}

// Method handles a normal Payload message from a node. Enqueues newSensorData event and allows the node to update its values
// msg:     A read only reference to the incoming rfm message
// return:  Returns the module specific error code
NodeHandlerErrorType NodeHandler::handleRfmMessage(const Payload* msg) {
    Node* node = this->getNodeById(msg->NodeId);
    if (node == nullptr) {
        return eNodeIdNotFound;
    }
    node->messageReceived(msg, radio->Rssi());
    if (msg->DeviceId == DEVICE_ID_NODEMANAGMENT) {
        // Every node sends a msg on DEVICE_ID_NODEMANAGMENT once in a period so new value is added only once per period
        NodeEvent event = {.nodeId = msg->NodeId, .type = eNewSensorValues};
        nodeEvents.unshift(event);
        if (!this->handleBackToSetupmodeRequest(msg->NodeId)) {
            return eNoAckFromNode;
        }
    }
    return eNoError;
}

// Method is called when gateway accepts new nodes and a node wants an ID
// msg:     The incoming message of the node
// return:  Returns a module specific error code
NodeHandlerErrorType NodeHandler::handleRfmMessage(const RequestIdPayload* msg) {
    if (msg->nodeID == 2) {
        int freeId = this->findFreeNodeId();
        if (freeId == 0) {
            return eNoAvailableNodeId;
        }
        RequestIdPayload answer = {.nodeID = freeId, .deviceID = msg->deviceID, .randomNumber = msg->randomNumber};
        GatewayUtils::getInstance()->DebugPrintf(answer);
        if (!radio->SendMessage((const void*)&answer, sizeof(answer), ADRESS_NEW_NODES)) {
            return eNoAckFromNode;
        }
        numOfNodes++;
        Node newNode(freeId);
        this->writeNodeToFile(&newNode);
        Serial.println("NodeHandler : " + String(newNode.jsonifyForFile()));
        activeNodes.push_back(newNode);
        NodeEvent event = {.nodeId = freeId, .type = eNodeWaitingForKey};
        nodeEvents.unshift(event);
        GatewayUtils::getInstance()->DebugPrintf("Accepted new Node with id: ", freeId);
    }
    return eNoError;
}

// Method sends a configuration to a node
// targetNodeId:    Id of the node to which the configuration is sent
// nodeConfig:      The configuration to be sent
// nodeName:        Name of the node to which the configuration is sent to. Has a default value
// return:          Returns a module specific error code
NodeHandlerErrorType NodeHandler::sendConfigToNode(int targetNodeId, DetailedNodeConfig nodeConfig, SensorMeasurementUnits units, String nodeName) {
    Node* node = nullptr;
    NodeHandlerErrorType error = this->validateNodeExistsAndSetupMode(targetNodeId, &node);
    if (error != eNoError) {
        return error;
    }
    if ((nodeConfig.d0Smartmeter) && (nodeConfig.mBus)) {
        return eMBusAndD0Selected;
    }
    if ((nodeConfig.sensorImpuls) && (nodeConfig.sensorDigital)) {
        return eImpAndDigSensSelected;
    }
    if (!radio->SendMessage((const void*)&nodeConfig, sizeof(nodeConfig), targetNodeId)) {
        return eNoAckFromNode;
    }
    node->setConfig(nodeConfig);
    node->setUnits(units);
    node->setName(nodeName);
    this->writeNodeToFile(node);
    return eNoError;
}

// Method sends the encryptkey to the node and updates the config for the node object
// targetNodeId:    Id of the node to which the key is sent
// return:          Returns a module specific error code
NodeHandlerErrorType NodeHandler::sendEncryptKeyToNode(int targetNodeId) {
    Node* node = nullptr;
    NodeHandlerErrorType error = this->validateNodeExistsAndSetupMode(targetNodeId, &node);
    if (error != eNoError) {
        return error;
    }
    radio->encryptNone();
    if (!radio->SendMessage((const void*)ENCRYPTKEY, sizeof(ENCRYPTKEY), targetNodeId)) {
        radio->encryptDefault();
        return eNoAckFromNode;
    }
    radio->encryptDefault();
    node->setHasEncryptKey(true);
    this->writeNodeToFile(node);
    NodeEvent event = {.nodeId = targetNodeId, .type = eNodeWaitingForConfig};
    nodeEvents.unshift(event);
    return eNoError;
}

// Method sets the state of a node´s LED to identify nodes when they are in setup mode
// targetNodeid:    Id of the node whose LED should be controlled
// targetLedState:  The state which the LED should adapt (on / off)
// return:          Returns a module specific error code
NodeHandlerErrorType NodeHandler::controlNodeLed(int targetNodeId, uint8_t targetLedState) {
    Node* node = nullptr;
    NodeHandlerErrorType error = this->validateNodeExistsAndSetupMode(targetNodeId, &node);
    if (error != eNoError) {
        return error;
    }
    if (!node->getHasEncryptKey()) {
        radio->encryptNone();
    }
    if (!radio->SendMessage((const void*)&targetLedState, sizeof(targetLedState), targetNodeId)) {
        error = eNoAckFromNode;
    }
    if (!node->getHasEncryptKey()) {
        radio->encryptDefault();
    }
    return error;
}

// Method to change the name of a node when this is requested by the webserver
// targetNodeid:    ID of the node whose name is changed
// newName:         The new name of the node
// return:          Returns the module specific error code
NodeHandlerErrorType NodeHandler::changeNodeName(int targetNodeId, String newName) {
    Node* node = this->getNodeById(targetNodeId);
    if (node == nullptr) {
        return eNodeIdNotFound;
    }
    node->setName(newName);
    if (this->writeNodeToFile(node)) {
        return eNoError;
    }
    return eInternalError;
}

// Adds a nodeid in the list of nodes which should be resetted. Doesnt add the id, if its not a known id or if it is already in nodesToReset
// targetNodeId:    Id of the target to be resetted
// return:          Module specific error code
NodeHandlerErrorType NodeHandler::resetNode(int targetNodeId) {
    for (int i = 0; i < nodesToReset.size(); i++) {
        if (nodesToReset[i] == targetNodeId) {
            return eResetNodeAlreadyRequested;
        }
    }
    Node* nodeToReset = this->getNodeById(targetNodeId);
    if (nodeToReset == nullptr) {
        return eNodeIdNotFound;
    }
    nodesToReset.push_back(targetNodeId);
    return eNoError;
}

// Function deletes a node from the list of nodes and from the file system
// targetNodeId: Id of the target to be deleted from the list of nodes
// return: Module specific error code
NodeHandlerErrorType NodeHandler::deleteNode(int targetNodeId) {
    for (int i = 0; i < activeNodes.size(); i++) {
        if (activeNodes[i].getId() == targetNodeId) {
            this->activeNodes.remove(i);
            this->numOfNodes--;
            LittleFS.remove(nodeDirectory + filenamePrefix + String(targetNodeId) + ".json");
            return eNoError;
        }
    }
    return eNodeIdNotFound;
}

NodeHandlerErrorType NodeHandler::pollSlaveNodes() {
    time_t now = 0;
    time(&now);
    Payload msg = {.NodeId = 0, .DeviceId = 1};
    NodeHandlerErrorType error = eNoError;
    for (int i = 0; i < activeNodes.size(); i++) {
        Node* node = &activeNodes[i];
        if ((node->getConfig()->operationMode == eSlaveMode) &&
           (now - node->getLastSeen() > node->getConfig()->analogDelaySeconds)) {
            msg.NodeId = node->getId();
            if (!radio->SendMessageWithBurst((const void*) &msg, sizeof(msg), msg.NodeId)) {
                error = eNoAckFromNode;
            }
        }
    }
    return error;
}

// Method returns number of nodes without encryption key and stores their ids in nodesbuffer
// Note:            If bufferSize < workingNodes then bufferSize is returned
// nodesBuffer:     Pointer to an array of size bufferSize of integers. Here the node ids matching the filter are added
// bufferSize:      Size of the provided array
// return:          Number of nodes waiting for an encryptkey
int NodeHandler::getNodesWithoutEncryptKey(int* nodesBuffer, int bufferSize) {
    int nodesAdded = 0;
    for (int i = 0; i < activeNodes.size(); i++) {
        // Condition for nodes without key is that they are in setupmode and hasKey-Flag in Node object has not been setted yet
        if ((activeNodes[i].getConfig()->operationMode == eSetupMode) && (!activeNodes[i].getHasEncryptKey())) {
            nodesAdded++;
            if (nodesAdded <= bufferSize) {
                *nodesBuffer = activeNodes[i].getId();
                nodesBuffer++;
            }
        }
    }
    return nodesAdded;
}

// Method returns number of nodes waiting for config. The ids of the nodes are stored in nodesBuffer
// Note:            If bufferSize < workingNodes then bufferSize is returned
// nodesBuffer:     Pointer to an array of size bufferSize of integers. Here the node ids matching the filter are added
// bufferSize:      Size of the provided array
// return:          Returns number of nodes waiting for a config
int NodeHandler::getNodesWaitingForConfig(int* nodesBuffer, int bufferSize) {
    int nodesAdded = 0;
    for (int i = 0; i < activeNodes.size(); i++) {
        // Condition for nodes waiting for config is that they are in setupmode and hasKey-Flag in Node  has been setted
        if ((activeNodes[i].getConfig()->operationMode == eSetupMode) && (activeNodes[i].getHasEncryptKey())) {
            nodesAdded++;
            if (nodesAdded <= bufferSize) {
                *nodesBuffer = activeNodes[i].getId();
                nodesBuffer++;
            }
        }
    }
    return nodesAdded;
}

// Method returns number of working nodes. Stores their ids in nodesBuffer
// Note:            If bufferSize < workingNodes then bufferSize is returned
// nodesBuffer:     Pointer to an array of size bufferSize of integers. Here the node ids matching the filter are added
// bufferSize:      Size of the provided array
// return:          Returns the number of nodes which are not in setup mode
int NodeHandler::getWorkingNodes(int* nodesBuffer, int bufferSize) {
    int nodesAdded = 0;
     for (int i = 0; i < activeNodes.size(); i++) {
        // Condition for nodes without key is that they are in not in setup mode
        if ((activeNodes[i].getConfig()->operationMode != eSetupMode) && (activeNodes[i].getHasEncryptKey())) {
            nodesAdded++;
            if (nodesAdded <= bufferSize) {
                *nodesBuffer = activeNodes[i].getId();
                nodesBuffer++;
            }
        }
    }
    return nodesAdded;
}


bool NodeHandler::getNodeEvent(NodeEvent* event) {
    if (nodeEvents.isEmpty()) {
        return false;
    }
    *event = nodeEvents.pop();
    return true;
}

// Methhod gets a reference to the node object and writes it to the filesystem
// note:    The object is not altered in the method so make sure all changes are applied to the node object before calling the method
// node:    Pointer to a node object
// return:  true if write was successful
bool NodeHandler::writeNodeToFile(Node* node) {
    File file = LittleFS.open(nodeDirectory + filenamePrefix + String(node->getId()) + ".json", "w", true);
    if (!file) {
        GatewayUtils::getInstance()->DebugPrintf("Failed to open file");
        return false;
    }
    file.print(node->jsonifyForFile());
    file.close();
    return true;
}

bool NodeHandler::readNodesFromFile() {
    File dir = LittleFS.open(nodeDirectory);
    if (!dir.isDirectory()) {
        GatewayUtils::getInstance()->DebugPrintf("Opened file is not a directory");
        return false;
    }
    File accessedFile = dir.openNextFile();
    String jsonNodeRepresentation = "";
    while (accessedFile) {
        while (accessedFile.available()) {
            jsonNodeRepresentation += (char)accessedFile.read();
        }
        numOfNodes++;
        activeNodes.push_back(Node(jsonNodeRepresentation));
        jsonNodeRepresentation = "";
        accessedFile.close();
        accessedFile = dir.openNextFile();
    }
    dir.close();
    return true;
}


// Method finds the first available node id in the current network.
// Note:    Runtime is quadratic could be optimized with a set. However right now I focus on memory usage
// Return:  int - Returns the first available node id
int NodeHandler::findFreeNodeId() {
    int nextId = 3;     // 0 for broadcast, 1 for gateway, 2 for new nodes. 3 could be first free node
    bool idAvailable = false;
    while (!idAvailable) {
        idAvailable = true;
        for (int i = 0; i < activeNodes.size(); i++) {
            if (activeNodes[i].getId() == nextId) {
                nextId++;
                idAvailable = false;
                break;
            }
        }
        if (idAvailable) {
            return nextId;
        }
        if (nextId > MAX_NUMBER_OF_NODES) {
            return 0;
        }
    }
    return 0;
}

// Method is executed when node asks if it should go back to setup mode
// idOfRequestingNode:  Id of the node which sent the request
// return:              Method returns nothing
bool NodeHandler::handleBackToSetupmodeRequest(int idOfRequestingNode) {
    Payload answer = {.NodeId = idOfRequestingNode, .DeviceId = DEVICE_ID_NODEMANAGMENT, .UptimeMilliseconds = 0, .SensorData = SENSOR_VAL_DONT_RESET_NODE};
    int indexResettedNode = 0;
    for (int i = 0; i < nodesToReset.size(); i++) {
        if (nodesToReset[i] == idOfRequestingNode) {
            answer.SensorData = SENSOR_VAL_RESET_NODE;
            indexResettedNode = i;
            break;
        }
    }
    if (!radio->SendMessage((const void*)&answer, sizeof(answer), idOfRequestingNode)) {
        return false;
    }
    if (answer.SensorData == SENSOR_VAL_RESET_NODE) {
        this->nodesToReset.remove(indexResettedNode);
        Node* resettedNode = this->getNodeById(idOfRequestingNode);
        if (resettedNode != nullptr) {
            resettedNode->setOperationMode(eSetupMode);
        }
        this->writeNodeToFile(resettedNode);
        NodeEvent event = {.nodeId = idOfRequestingNode, .type = eNodeWaitingForConfig};
        nodeEvents.unshift(event);
    }
    return true;
}

// Method iterates over active nodes and returns reference to one if the id was found
// targetNodeId:    The id whose reference should be returned
// return:          Address of node if it exists, nullptr if it was not found
Node* NodeHandler::getNodeById(int targetNodeId) {
    for (int i = 0; i < activeNodes.size(); i++) {
        if (activeNodes[i].getId() == targetNodeId) {
            return &activeNodes[i];
        }
    }
    return nullptr;
}

// Method makes sure node exists and is in setup mode. The node can be retrieved via pass by reference
// targetNodeId:    The id of the node to check
// nodeToAccess:    Pointer of pointer to node, which is returned if check was successfull, otherwise nullptr
// note:            Pointer to pointer is unfortunately necessary to avoid creating a node outside of the activeNodes array
// return:          True if node exists and is in setupmode, false otherwise
NodeHandlerErrorType NodeHandler::validateNodeExistsAndSetupMode(int targetNodeId, Node** nodeToAccess) {
    *nodeToAccess = this->getNodeById(targetNodeId);
    if (nodeToAccess == nullptr) {
        return eNodeIdNotFound;
    }
    if ((*nodeToAccess)->getConfig()->operationMode != eSetupMode) {
        return eNodeNotInSetupMode;
    }
    return eNoError;
}
