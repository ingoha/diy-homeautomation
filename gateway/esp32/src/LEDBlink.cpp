#include "Gateway_config.h"
#include "Gateway_config_board.h"
#include "GatewayUtils.h"
#include "LEDBlink.h"
#include <Arduino.h>
#include "LEDProperties.h"


// Constructor: Initializes the LEDBlink object
// Parameters:
// - ledTasks: An array of LEDStates objects representing the LED tasks
// - countOfLedTasks: The count of LED tasks in the array
// Initializes member variables and copies the LED tasks from the array
LEDBlink::LEDBlink(LEDStates* ledTasks, int countOfLedTasks) {
    //  this->_ActualLEDTask = nullptr;

    this->_previousMillis = 0;
    this->_TaskIsActive = false;

    this->_SizeOfLedTaskArray = countOfLedTasks;

    _LedTaskArray = new LEDStates[countOfLedTasks];

    for (size_t i = 0; i < countOfLedTasks; i++) {
      _LedTaskArray[i] = ledTasks[i];
    }
}

// Function: Add an LED task to the queue
// Parameters:
// - led: The LEDProperties object representing the LED task to add
// Adds the LED task to the queue for processing
void LEDBlink::AddTask(LEDProperties led) {
    _Queue.push(led);
}

// Destructor: Cleans up resources
// Deletes the LED task array and clears the LED task queue
LEDBlink::~LEDBlink() {
    delete[] _LedTaskArray;
    _Queue.clear();
}

// Function: Main loop to handle LED tasks
// Checks if an LED task is active and handles it accordingly
// If no LED task is active, retrieves the next task from the queue and activates it
// Return: no
void LEDBlink::Loop() {
    if (_TaskIsActive) {
        // handle the actual LED
        _LEDHandler();
    }
    else {
        if (_Queue.isEmpty()) {
            return;
        }

        LEDProperties NextLEDTask = _Queue.shift();
        //  delete[] _ActualLEDTask;
        //  _ActualLEDTask = new LEDProperties();

        _ActualLEDTask.TaskName = NextLEDTask.TaskName;
        _ActualLEDTask.PeriodDurationInMs = NextLEDTask.PeriodDurationInMs;
        _ActualLEDTask.PeriodNumebers = NextLEDTask.PeriodNumebers *2;
        _TaskIsActive = true;
    }
}

// Function: Get the active LED task
// Returns the LEDTaskList representing the currently active LED task
LEDTaskList LEDBlink::WhichLEDTaskIsActive() {
    return _ActualLEDTask.TaskName;
}

// Function: gives status info about the given  A LED
// LEDNumber: LEDNumber the state i want to know
// return status of the LED
int LEDBlink::LEDStatus(short int LEDNumber) {
    return digitalRead(LEDNumber);
}

// Funktion: deceides wichk LED how long will shine, depends on the LEDs we want to shine
// LEDNumbers: Which LED Number (depends on the PIN connected to) will shine
// LEDsizeArray: Number of different LED we use for this LED task
void LEDBlink::_LEDHandler() {
    unsigned long currentMillis = millis();

    // check if time is passed bigger than the Period time of on/off Time
    if ((currentMillis - _previousMillis) >= (_ActualLEDTask.PeriodDurationInMs)/2) {
        //  DebugPrintf("LEDBlink.cpp 88");
        //  DebugPrintf(_LedsActive);

        _previousMillis = currentMillis;
        _ParseLEDTaskListEnum(_ActualLEDTask.TaskName).ToogleLEDs(_LedsActive);

        _LedsActive = !_LedsActive;
        _ActualLEDTask.PeriodNumebers -= 1;
        // if  the actual LED Task is not over reset all values of _ActualLEDTask, as the beginning
        if (_ActualLEDTask.PeriodNumebers == 0) {
            _ActualLEDTask.PeriodNumebers = 0;
            _ActualLEDTask.PeriodDurationInMs = 0;
            _ActualLEDTask.TaskName = NoneSelected;
            _TaskIsActive = false;
        }
    }
}

// Method: Parses the LED task list enumeration
// Parameters:
// - task: The LEDTaskList value representing the LED task
// Returns the corresponding LEDStates object based on the task
LEDStates LEDBlink::_ParseLEDTaskListEnum(LEDTaskList task) {
    if (task == NoneSelected) {
        GatewayUtils::getInstance()->DebugPrintf("This Selection is not possible to handle ");
        return LEDStates(0, 0, NoneSelected);
    }

    for (size_t i = 0; i < _SizeOfLedTaskArray; i++) {
        if (_LedTaskArray[i].GetTaskName() == task) {
            return _LedTaskArray[i];
        }
    }

    GatewayUtils::getInstance()->DebugPrintf("This Selection Mode is not added as Task to LEDBlink ");
    return LEDStates(0, 0, NoneSelected);
}


// Function: return the Size of the queue
// Return: the Size of the queue
int LEDBlink::QueueSize() {
    return _Queue.size();
}
