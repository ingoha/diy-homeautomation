#include "NetworkConnection.h"
#include "GatewayUtils.h"
#include "GatewayUtils.h"
#include "Gateway_config.h"
#include "Gateway_config_board.h"
#include <time.h>
#include "OLEDDisplayWrapper.h"

// Method: Constructor for creating a NetworkConnection object
// Initializes the _TimeInfo struct pointer
NetworkConnection::NetworkConnection() {
    _TimeInfo = new struct tm;
}

// Method: Destructor for cleaning up resources
// Deletes the _TimeInfo struct pointer
NetworkConnection::~NetworkConnection() {
    delete _TimeInfo;   // Instance of the a Time Struct (https://cplusplus.com/reference/ctime/tm/, , https://randomnerdtutorials.com/esp32-date-time-ntp-client-server-arduino/)
}

// Method: Check if the device is connected to a local network
// Return: True if connected, false otherwise
bool NetworkConnection::ConnectedWithLocalNetwork() {
    if (WiFi.status() == WL_CONNECTED)
        return true;
    else
        return false;
}

// if here than we need to try to connectet to WIFI
// Method: Restart the process of connecting to a local network with the provided credentials
// Parameters:
// - ssid: The SSID of the local network
// - password: The password for the local network
// - reconnectTrys: The number of reconnect attempts to make
// - delayTimeForNextReconnectInMs: The delay time between reconnect attempts in milliseconds
// Return: True if connected, false otherwise
bool NetworkConnection::RestartProcessForLocalNetworkConnection(const String ssid, const String password, int reconnectTrys, int delayTimeForNextReconnectInMs) {
    bool connected = false;
    WiFi.begin(ssid.c_str(), password.c_str());
    for (size_t i = 0; i < reconnectTrys; i++) {
        if (WiFi.status() == WL_CONNECTED) {
            connected = true;
            break;
        }
        delay(delayTimeForNextReconnectInMs);
    }

    return connected;
}

// Method: Convert the WLAN status to a string representation
// Parameters:
// - status: The WLAN status
// Return: The string representation of the WLAN status
const String NetworkConnection::wlanStatusToString(wl_status_t status) {
    switch (status) {
        case WL_NO_SHIELD:
            return "WL_NO_SHIELD";
        case WL_IDLE_STATUS:
            return "WL_IDLE_STATUS";
        case WL_NO_SSID_AVAIL:
            return "WL_NO_SSID_AVAIL";
        case WL_SCAN_COMPLETED:
            return "WL_SCAN_COMPLETED";
        case WL_CONNECTED:
            return "WL_CONNECTED";
        case WL_CONNECT_FAILED:
            return "WL_CONNECT_FAILED";
        case WL_CONNECTION_LOST:
            return "WL_CONNECTION_LOST";
        case WL_DISCONNECTED:
            return "WL_DISCONNECTED";
        default:
            return "UNKNOWN_STATUS";
    }
}


// Method: Get the timestamp
// Return: The timestamp as a string
String NetworkConnection::GetTimeStamp() {
    return this->_TimeStamp;
}

// Method: Get the time information
// Return: A pointer to the time information struct
struct tm* NetworkConnection::GetTimeInfo() {
    return this->_TimeInfo;
}

// Method: Obtain the current time from the NTP server
void NetworkConnection::ObtainTime() {
    if (_TimeStamp != "") {
        return;
    }
    configTime(_GMTSECONDSOFFSET, _DAYLIGHTSECONDSOFFSET, _TIMENTPSERVER.c_str());

    if (!getLocalTime(_TimeInfo)) {
        GatewayUtils::getInstance()->DebugPrintf("Failed to obtain time");
        return;
    }
    char timebuf[10] = "";
    strftime(timebuf, 10 , "%A", _TimeInfo);
    _TimeStamp += timebuf;
    _TimeStamp += " ";
    strftime(timebuf, 10 , "%B", _TimeInfo);
    _TimeStamp += timebuf;
    _TimeStamp += " ";
    strftime(timebuf, 10 , "%d", _TimeInfo);
    _TimeStamp += timebuf;
    _TimeStamp += " ";
    strftime(timebuf, 10 , "%Y", _TimeInfo);
    _TimeStamp += timebuf;
    _TimeStamp += " ";
    strftime(timebuf, 10 , "%H", _TimeInfo);
    _TimeStamp += timebuf;
    _TimeStamp += " ";
    strftime(timebuf, 10 , "%M", _TimeInfo);
    _TimeStamp += timebuf;
    _TimeStamp += " ";
    strftime(timebuf, 10 , "%S", _TimeInfo);
    _TimeStamp += timebuf;
    GatewayUtils::getInstance()->DebugPrintf(_TimeStamp);
}


