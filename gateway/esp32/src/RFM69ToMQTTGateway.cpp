#include "RFM69ToMQTTGateway.h"

#include <Arduino.h>
#include "Gateway_config.h"
#include "Gateway_config_board.h"
#include "LEDProperties.h"
#include <WiFi.h>

// Constructor for RFM69ToMQTTGateway class
// Initializes the serial communication, pins, and various objects required for operation
// nodeHandler: Pointer to the NodeHandler object
RFM69ToMQTTGateway::RFM69ToMQTTGateway(NodeHandler* nodeHandler) {
    Serial.begin(SERIAL_BAUD);
    Serial.flush();
    while (!Serial)
      delay(100);

    pinMode(PUSHBUTTON_ESP32, INPUT);
    pinMode(PUSHBUTTON_NODEMODE, INPUT);

    _LEDManager = LEDBlinkFactory();
    _OLEDDisplayHandler = new OLEDDisplayWrapper();
    _RFMMsgQueue = new CircularBuffer<Payload, 100>;
    _Radio = new RFM69Radio(ESP32_INTERRUPT, ESP_SPI_CLK, ESP_SPI_MISO, ESP_SPI_MOSI, ESP_SPI_SS, IS_RFM69HW);
    _NetworkConnectionHandling = new NetworkConnection();
    _MQTTClient = new MQTTGateway(my_mqtt_server, _NetworkConnectionHandling, _OLEDDisplayHandler);
    _CallbackWrapper  = [this](char* topic, byte* message, unsigned int length)     {
        Payload* valueToAdd =  _MQTTClient->Callback(topic, message, length, _LEDManager);
        if (valueToAdd == nullptr) {
            delete valueToAdd;
            return;
        }
        _RFMMsgQueue->push(*valueToAdd);
        delete valueToAdd;
    };
    _MQTTClient->MqttClientInit(_CallbackWrapper);

    _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(InitDoneState));
    _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("Init done. ", Info);

    _NodeHandler = nodeHandler;
    _NodeHandler->setRadio(_Radio);
}

// Method that runs the main loop of the RFM69ToMQTTGateway
// Handles network and broker connections, mode and encryption handling, RFM and MQTT message handling
void RFM69ToMQTTGateway::Run() {
    _LEDManager->Loop();

    // if one of these LEDs are active wait till over -> make sure that it works
    if (this->WaitAndDoNotPass())
        return;

    HandleNetworkConnection();
    HandleBrokerConnection();
    // Handling mode and radio encryption
    if ((_NodeHandler->getNewNodesAllowed()) && (_mode == eRunMode)) {
        GatewayUtils::getInstance()->DebugPrintf("Changed in NewSensMode");
        _mode = eAcceptNewSensorsMode;
        millisNewNodesModeStarted = (uint64_t)millis();
        _Radio->encryptNone();
    }
    if ((_mode == eAcceptNewSensorsMode) && (millis() - millisNewNodesModeStarted > maxTimeInNewNodesMode)) {
        GatewayUtils::getInstance()->DebugPrintf("Changed in RunMode");
        _mode = eRunMode;
        _Radio->encryptDefault();
        _NodeHandler->setNewNodesAllowed(false);
    }

    // Handle RFM / MQTT
    if (_Radio->MessageReceived())
        HandleReceivedRFMMessage();
    if ((!_RFMMsgQueue->isEmpty()))
        HandlePreparedMessageFromMQTTCallback();
    if (_mode == eRunMode) {
        _NodeHandler->pollSlaveNodes();
    }
    return;
}

// Destructor for RFM69ToMQTTGateway class
// Clears the RFM message queue and deletes allocated objects
RFM69ToMQTTGateway::~RFM69ToMQTTGateway() {
  _RFMMsgQueue->clear();
  delete _RFMMsgQueue;
  delete _LEDManager;
  delete _Radio;
  delete _MQTTClient;
  delete _OLEDDisplayHandler;
  delete _NetworkConnectionHandling;
}


// Factory method for creating an instance of the LEDBlink class
// Initializes the pin modes and LED states for different tasks
// Returns: Pointer to the created LEDBlink object
LEDBlink* RFM69ToMQTTGateway::LEDBlinkFactory() {
    pinMode(LED_GREEN, OUTPUT);
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_YELLOW, OUTPUT);
    pinMode(LED_BLUE, OUTPUT);

    int mqttProbelmLEDS[1]   = {LED_YELLOW                              };
    int rfmReceivedLEDs[1]   = {            LED_BLUE                    };
    int rfmNoAckLEDs[1]      = {                      LED_RED           };
    int rfmSendLEDs[1]       = {                               LED_GREEN};
    int mqttPublishedLEDS[2] = {LED_YELLOW,                    LED_GREEN};
    int mqttReceivecLEDS[2]  = {            LED_BLUE,          LED_GREEN};
    int mqttConnected[2]     = {            LED_BLUE,          LED_GREEN};
    int wlanLEDs[3]          = {LED_YELLOW, LED_BLUE,          LED_GREEN};
    int noWlanLEDs[3]        = {LED_YELLOW, LED_BLUE, LED_RED           };
    int initLEDs[4]          = {LED_YELLOW, LED_BLUE, LED_RED, LED_GREEN};

    const int sizeOf = 10;

    LEDStates _LEDTaskArray[sizeOf] = {
      LEDStates(rfmReceivedLEDs, sizeof(rfmReceivedLEDs)/sizeof(rfmReceivedLEDs[0]), RfmMessageReceivedState),
      LEDStates(rfmSendLEDs, sizeof(rfmSendLEDs)/sizeof(rfmSendLEDs[0]), RfmMessageSend),
      LEDStates(rfmNoAckLEDs, sizeof(rfmNoAckLEDs)/sizeof(rfmNoAckLEDs[0]), RfmMessageNoAckState),
      LEDStates(mqttReceivecLEDS, sizeof(mqttReceivecLEDS)/sizeof(mqttReceivecLEDS[0]), MqttMessageReceivedState),
      LEDStates(mqttPublishedLEDS, sizeof(mqttPublishedLEDS)/sizeof(mqttPublishedLEDS[0]), MqttMessagePublishState),
      LEDStates(mqttProbelmLEDS, sizeof(mqttProbelmLEDS)/sizeof(mqttProbelmLEDS[0]), MqttProblemState),
      LEDStates(mqttConnected, sizeof(mqttConnected)/sizeof(mqttConnected[0]), MQTTConnectedToBroker),
      LEDStates(initLEDs, sizeof(initLEDs)/sizeof(initLEDs[0]), InitDoneState),
      LEDStates(wlanLEDs, sizeof(wlanLEDs)/sizeof(wlanLEDs[0]), WlanConnectedState),
      LEDStates(wlanLEDs, sizeof(wlanLEDs)/sizeof(wlanLEDs[0]), WLANNotConnectedState),
    };

  return new LEDBlink(_LEDTaskArray, sizeOf);
}

// Checks if the gateway is in a state where it should wait and not proceed with the main loop
// Returns: True if the gateway should wait, False otherwise
bool RFM69ToMQTTGateway::WaitAndDoNotPass() {
    if (_LEDManager->WhichLEDTaskIsActive() == WlanConnectedState) {
        return true;
    }
    if (_LEDManager->WhichLEDTaskIsActive() == InitDoneState) {
        return true;
    }
    return false;
}

// Handles the network connection
// Checks if the gateway is connected to the local network and takes necessary actions
void RFM69ToMQTTGateway::HandleNetworkConnection() {
    _WLANConnected = _NetworkConnectionHandling->ConnectedWithLocalNetwork();
    if (_WLANConnected) {
        return;
    }

    if (!GatewayUtils::getInstance()->CheckPassedTime(30000, &previousMillisForWLANConnection))
        return;


    // check if it is connected or if waittime for reconnect passed
    // restart reconnet and show result
    _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("Connect ESP to Wifi:  " + String(my_ssid) + ". ", Info);

    if (_NetworkConnectionHandling->RestartProcessForLocalNetworkConnection(my_ssid, my_password)) {
        _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("Connected to IP " +   WiFi.localIP().toString() + ". ", Info);
        _WLANConnected = true;
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(WlanConnectedState));
        _NetworkConnectionHandling->ObtainTime();
    }
    else {
        GatewayUtils::getInstance()->DebugPrintf("[Info]: Not connected, WiFi Status: " + _NetworkConnectionHandling->wlanStatusToString(WiFi.status()) + ".");
        _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("No Wifi connection. ", Info);
        _WLANConnected = false;
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(WLANNotConnectedState));
    }
}

// Handles the broker connection
// Checks if the gateway is connected to the MQTT broker and takes necessary actions
void RFM69ToMQTTGateway::HandleBrokerConnection() {
    if (!_WLANConnected)
        return;

    if (!GatewayUtils::getInstance()->CheckPassedTime(100000, &previousMillisForBrokerConnection)) {
        return;
    }

    if (_MQTTClient->IsConnectedWithBroker())
        return;

    // check if MQTT is conncted one time
    if (_MQTTClient->ConnectToBroker()) {
        _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("Broker Connected. ", MQTT);
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(MQTTConnectedToBroker));
        _ToBrokerConnected = true;
    }
    else {
        _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("Broker not Connected. ", MQTT);
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(MqttProblemState));
        _ToBrokerConnected = false;
    }
}

// Handles the received RFM message
// Retrieves the RFM message, processes it based on the gateway mode, and publishes it to the MQTT broker if necessary
void RFM69ToMQTTGateway::HandleReceivedRFMMessage() {
    _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(RfmMessageReceivedState));
    _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("Received RFM Msg. ", RFM);
    // Retrieve msg from radio
    byte rfmMessageBuffer[RF69_MAX_DATA_LEN];         // Define is from RFM-Lib
    _Radio->GetMessage(rfmMessageBuffer);
    if (_mode == eRunMode) {
        Payload msg = *(Payload*) rfmMessageBuffer;
        GatewayUtils::getInstance()->DebugPrintf(&msg);
        _NodeHandler->handleRfmMessage(&msg);
        if ((_ToBrokerConnected) && (msg.DeviceId != DEVICE_ID_NODEMANAGMENT)) {
            _MQTTClient->MqttPublishRfm(msg, _Radio->Rssi());
            _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(MqttMessagePublishState));
            _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("MQTT Msg published. ", MQTT);
        }
    }
    if (_mode == eAcceptNewSensorsMode) {
        RequestIdPayload msg = *(RequestIdPayload*) rfmMessageBuffer;
        _NodeHandler->handleRfmMessage(&msg);
    }
}

// Handles the prepared message from the MQTT callback
// Sends the prepared RFM message to the corresponding node and publishes a message to the MQTT broker if an error occurs
void RFM69ToMQTTGateway::HandlePreparedMessageFromMQTTCallback() {
    Payload Message_To_RFM = _RFMMsgQueue->shift();
    if (!_Radio->SendMessageWithBurst((const void*) &Message_To_RFM, sizeof(Payload), Message_To_RFM.NodeId)) {
        if (_ToBrokerConnected) {
        _MQTTClient->Publish("home/rfm_gw/nb/node " + String(Message_To_RFM.NodeId) + "/dev90" ,
                             "could not reach node " + String(Message_To_RFM.NodeId));
      // _MQTTClient->MqttPublish(NodeUnableToReach, Message_To_RFM.NodeId);
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(MqttMessagePublishState));
        }
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(RfmMessageNoAckState));
        _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("RFM Msg send, no Ack. ", RFM);
    }
    else {
        _LEDManager->AddTask(LEDPropertiesWrapper::StructFactory(RfmMessageSend));
        _OLEDDisplayHandler->AddTextToQueueAndWriteToOutputs("RFM Msg send. ", RFM);
    }
}
