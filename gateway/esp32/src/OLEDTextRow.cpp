#include "OLEDTextRow.h"
#include "GatewayUtils.h"

// Destructor for the OLEDTextRow class
// Deletes the used font and the U8g2Display object
// Clears the queue of tasks
// Deletes the queue object
OLEDTextRow::~OLEDTextRow() {
    delete _UsedFont;
    delete U8g2Display;
    _Queue->clear();
    delete _Queue; //safe the Tasks for LED 
}

// Constructor for the OLEDTextRow class
// Initializes the U8g2Display and sets the initial text and position
// Parameters:
// - u8g2Display: Pointer to the U8G2_SSD1306_128X64_NONAME_1_HW_I2C object for OLED display
// - firstText: Initial text to be displayed
// - yRowPos: Y position of the text row on the display
OLEDTextRow::OLEDTextRow(U8G2_SSD1306_128X64_NONAME_1_HW_I2C* u8g2Display, String firstText, unsigned int yRowPos) {
    U8g2Display = u8g2Display;
    _YPosition = yRowPos;
    _Queue = new CircularBuffer<String, 50>;
    _FirstText = firstText;
    _LastText = _FirstText;
    SetNewText(firstText);
}

// Sets a new text for the OLEDTextRow
// Parameters:
// - text: New text to be displayed
// - font: Pointer to the font for the text (optional)
void OLEDTextRow::SetNewText(String text, const uint8_t * font) {
    // _TextWidth = 0;
    // _XPosition = 0;
    // _Offset = 0;
    // _NextTextOffset = 0;
    _ActualText = text;
    _UsedFont = font;
    U8g2Display->setFont(_UsedFont);
    _TextWidth = U8g2Display->getUTF8Width(_ActualText.c_str());
    // DebugPrintf("Next Entry 28 - : " + _ActualText + " , "+_TextWidth);
    if (_TextWidth == 0) {
        _ActualText = _LastText;
        _TwoTimesRun = true;
        _CheckForNextText();
    }
}

// Sets the X position of the text on the display
// Parameters:
// - value: X position value
void OLEDTextRow::SetXPosition(unsigned int value) {
    _XPosition = value;
}

// Sets the Y position of the text on the display
// Parameters:
// - value: Y position value
void OLEDTextRow::SetYPosition(unsigned int value) {
    _YPosition = value;
}

// Sets the offset value for scrolling the text
// Parameters:
// - value: Offset value
void OLEDTextRow::SetOffset(unsigned int value) {
    _Offset = value;
}

// Scrolls the text by adjusting the offset value
void OLEDTextRow::ScrollText() {
    // _XPosition -= 3;
    _Offset -= 3;
}

// Handles all text checks and updates, including scrolling and checking for offset reset
void OLEDTextRow::HandleAllTextChecks() {
    ScrollText();
    // IterateNextTextOffset();
    CheckForOffsetReset();
}

// Updates the next text offset value based on the current X position and text width
void OLEDTextRow::IterateNextTextOffset() {
    if (_XPosition +  _TextWidth <= _DisplayPixelLenght)
        _NextTextOffset += 3;
}

// Adds a new text to the queue
// If the queue is full, the oldest text is shifted out
// Parameters:
// - text: Text to be added to the queue
void OLEDTextRow::AddText(String text) {
    if(_Queue->isFull()) {
        GatewayUtils::getInstance()->DebugPrintf("QUEUE is voll. ");
        _Queue->shift();
    }
    _Queue->push(text);
}

// Checks for the next text in the queue and sets it as the current text
// If the queue is empty, the last text is restored
void OLEDTextRow::_CheckForNextText() {
    if (!_TwoTimesRun) {
        _TwoTimesRun = true;
        return;
    }
    _TwoTimesRun = false;


    if (_Queue->isEmpty()) {
        // if (_LastText != _ActualText) {
        //     SetNewText(_LastText);
        // }
        return;
    }

    _LastText = _ActualText;
    String nextShift = _Queue->shift();
    // Serial.println("TextRow: " + info);
    SetNewText(nextShift);
}

// Checks if the offset value has reached the reset point and resets it if necessary
// Also checks for the next text in the queue
void OLEDTextRow::CheckForOffsetReset() {
    // Serial.println("89 " + _XPosition +  _TextWidth);
    // if (_XPosition +  _TextWidth <= 0)
    if ( (u8g2_uint_t)_Offset < (u8g2_uint_t)-_TextWidth) {
        // DebugPrintf("hier drin in Zeile 92 in TextRow");
        _XPosition = 0;
        _Offset = 0;
        _NextTextOffset = 0;
        _CheckForNextText();
    }
}

// Checks if the current text can be written to the display based on the X position and text width
void OLEDTextRow::_CheckWriteNextText() {
    if (_XPosition +  _TextWidth <= _DisplayPixelLenght) {
        // U8g2Display->setFont(_UsedFont);
        U8g2Display->drawUTF8(_XPosition + 128/*128-_NextTextOffset*/, _YPosition, _ActualText.c_str());						
    }
}

// Handles displaying the text line on the OLED display
void OLEDTextRow::HandleTextLine(/*unsigned int x, unsigned int y*/) {

    if(_TextWidth < _DisplayPixelLenght) {
        if (GatewayUtils::getInstance()->CheckPassedTime(5000, &previousMillis, &_Iteration)) {
            _Iteration = true;
            _TwoTimesRun = true;
            _CheckForNextText();
        }
        U8g2Display->drawUTF8(0, _YPosition, _ActualText.c_str());
        return;
    }


    // // _XPosition = 0;
    // x = offset;
    _XPosition = _Offset;
    do {	
        U8g2Display->drawUTF8(_XPosition, _YPosition, _ActualText.c_str());
        _XPosition += _TextWidth;	

        // U8g2Display->setCursor(20, 16);
        // U8g2Display->print(_XPosition);					// this value must be lesser than 128 unless U8G2_16BIT is set
        // U8g2Display->setCursor(80, 16);
        // U8g2Display->print(_TextWidth);		

    } while( _XPosition < U8g2Display->getDisplayWidth() );		

    // U8g2Display->setCursor(20, 32);
    // U8g2Display->print(_Offset);					// this value must be lesser than 128 unless U8G2_16BIT is set
    // U8g2Display->setCursor(80, 32);
    // U8g2Display->print(_TextWidth);					

    // if(_TextWidth < _DisplayPixelLenght) {
    //     if (GatewayUtils::getInstance()->CheckPassedTime(5000, &previousMillis, &_Iteration)) {
    //         _Iteration = true;
    //         _CheckForNextText();
    //     }
    //     U8g2Display->drawUTF8(0, _YPosition, _ActualText.c_str());
    //     return;
    // }
    // // _XPosition = _Offset;
    // // U8g2Display->setFont(u8g2_font_inb21_mr);
    // U8g2Display->drawUTF8(_XPosition, _YPosition, _ActualText.c_str());
    // _CheckWriteNextText();
}